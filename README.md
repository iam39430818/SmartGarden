SmartGarden is a garden's management app for android systems, the app implements data statistics from a garden such as humidity, temperature or electric consumption, an automated management such as watering or light and a many more features. 
The app has been developed as the final work for the cycle of programming in Android. 
The app needs to work with the server running, unfortunately  the server is not online right now, 
you can see a video demostration with the app working with the server running.

The server uses Apache server for the android app SmartGarden with PHP technology and MySQL. Arduino's controlles uses Python technology. 

Technologies:

	Android
	SQLite 3
	Json
	PHP
	Python
	MySQL
	Apache
	Raspberry Pi
	Arduino
	
You can view the project documentation and the video demonstration in the Documentacion folder.