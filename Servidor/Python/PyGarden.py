#!/usr/bin/env python
# -*- coding: utf-8 -*-

import time
import serial
import threading
import socket
import sys
import MySQLdb
import datetime
from gcm import *


class RS485Com ():
	
	def __init__ (self,milisTimeOut,port):
		self.hora = datetime.datetime.now().hour

		# Inicializamos las arrays
		self.tEncender= []
		self.tApagar = []
		self.tEnableado = []
		# Rellenamos los datos de las arrays vacios
		for i in range(7):
			self.tEncender.append(0)
			self.tApagar.append(0)
			self.tEnableado.append(False)
		# Indicamos el tiempo de comunicacion con el canal
		self.timeout = float(milisTimeOut) / 1000.0
		self.canal = serial.Serial('/dev/ttyAMA0', baudrate=115200, timeout=self.timeout)
		
		# Ponemos en marcha el proceso que realiza las tareas de escaneo
		# Crea el semáforo para la comunicación para socket y thread.
		self.semaforo = threading.Lock()
		self.procScan = threading.Thread (target = self.procesoScaneo)
		self.procScan.start()

		# Esta clase es un servidor tcp, en el puerto indicado
		self.server = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
		self.server.setsockopt (socket.SOL_SOCKET,socket.SO_REUSEADDR,1)
		self.server.bind (('',port))
		self.server.listen (5)
		
		while True:
			# Se bloquea esperando una nueva peticion
			(clientSocket, address) = self.server.accept ()
			print "Aceptada conexion cliente"
				
			try:
				# Lee los datos de la petición del cliente.
				datos = clientSocket.recv (2048)
				# En caso que no recibamos datos del cliente retornamos cadena vacia
				if datos == '': continue
				
				print 'Comando:',datos
				# Miramos que tipo de comando ejecuta el cliente
				comando = datos[0]
				# Comando para encender y apagar
				if comando == 'E' or comando == 'A':
					self.canal.write (datos + '\n')
					retorno = "OK"
				# Comando para obtener los disponibles
				# O ver la temperatura, humedad y consumo actual
				elif comando == 'X' or comando == 'T' or comando == 'C' or comando == 'H':
					self.canal.write (datos + '\n')
					retorno = self.canal.readline() + '\n'
				# Programar una funcion automatica
				elif comando == 'P':
					# Miramos el canal que ejecuta la accion
					canal = int(datos[1])
					# Comprobamos si lo quiere encender o apagar
					enablear = int(datos[2])
					# Recuperamos el tiempo de inicio y fin
					h1 = int(datos[3:5])
					m1 = int(datos[5:7])
					h2 = int(datos[7:9])
					m2 = int(datos[9:11])
					print comando,canal,enablear,h1,m1,h2,m2
					print self.tEncender
					print self.tApagar
					print self.tEnableado
					# Pasamos el tiempo de inicio a segundos
					self.tEncender[canal] = h1*3600+m1*60
					print self.tEncender
					# Pasamos el tiempo de fin a segundos
					self.tApagar[canal] = h2*3600+m2*60
					print self.tApagar
					# Guardamos si quiere encender o apagar la funcion
					self.tEnableado[canal] = (enablear == 1)
					print self.tEnableado
					retorno = "OK"
				# En caso de fallar algo retornamos un mensaje de error	
				else:
					retorno = "ERROR\n"

				# En caso de ir todo correctamente se le devuelve al cliente
				# la información pedida, con un \n al final
				print "Devuelvo: " + retorno
				clientSocket.send (retorno + '\n')
			# Si salta alguna exepcion en la comunicacion retornamos un 
			# mensaje de error y cerramos la comunicación
			except:
				print "ERROR GORDO"
				clientSocket.close ()		
	

	# Funcion que usaremos para enviar notificaciones.	
	def enviarNotificacion(self, regId, mensaje, tipo, idMensaje):
		# Creamos el GCM con nuestra API KEY
		gcm = GCM("AIzaSyDoazn_gVapZgYhFNRcIhAx_Tzrq-mtJ1Y")
		# Creamos los datos que vamos a enviar ESTABA ENTRE COMILLAS SIMPLES
		data = {'mensaje': mensaje,'tipo': tipo,'idMensaje':idMensaje}
		# Obtenemos el registration id al que se va enviar la notificacion
		reg_id = regId
		# Enviamos la notificacion
		gcm.plaintext_request(registration_id=reg_id, data=data)
		print "Notificacion enviada: ",mensaje,tipo,idMensaje
		return;
	
	'''
		Proceso que hace el escaneo continuo de las placas.
		Es un thread que se activa y se ejecuta continuamente, preguntando una por una a todas
		las posibles placas enganchadas en el bus.
	'''
	def procesoScaneo (self):
		print 'Se inicia el proceso de escaneo'
		# Inicializamos el tiempo de espera que se necesita para guardar un registro
		TGuardar = 0
		# Inicializamos el envio de un solo tipo de notificacion
		tempAviso = False
                tempPeligro = False
                humAviso = False
                humPeligro = False
		# Bucle infinito que ejecuta el hilo
		while True:
			time.sleep (1)
			# Obtenemos la fecha actual y la pasamos a segundos
			now = datetime.datetime.now()
			midnight = now.replace(hour=0, minute=0, second=0, microsecond=0)
			seconds = (now - midnight).seconds
			# Inicializamos el primer canal a escuchar
			canal = 4
			# Indicamos un bucle que recorra todos los canales disponibles
			while canal <= 6:
				# Comprobamos si quiere encender o apagar la funcion automatica
				if self.tEnableado[canal]:
					# En caso de querer encender el tiempo de inicio tiene que ser mas pequeño
					# que el tiempo de apagado
					if self.tEncender[canal] < self.tApagar[canal]:
						# Se encendera en caso de que el tiempo actual sea mayor o igual que
						# que el tiempo de encendido y tambien que el tiempo actual sea
						# menor o igual que el tiempo de apagado
						if seconds >= self.tEncender[canal] and seconds <= self.tApagar[canal]:
							self.canal.write ("E" + str(canal) + "\n")
						# Cuando acabe se apaga	
						else:
							self.canal.write ("A" + str(canal) + "\n")
					# En caso de que el tiempo no se cumpla que el tiempo de encendido es mas
					# pequeño que el tiempo de apagado		
					else:
						# Comprobamos si el tiempo actual es mayor o igual que el tiempo de 
						# encendido o tambien que el tiempo actual sea menor o igual
						# que el tiempo de apagado
						if seconds >= self.tEncender[canal] or seconds <= self.tApagar[canal]:
							self.canal.write ("E" + str(canal) + "\n")
						# Cuando acabe se apaga	
						else:
							self.canal.write ("A" + str(canal) + "\n")
								
				# Obtenemos la temperatura y humedad actual
				self.canal.write("T\n")
				temperaturaActual = int(self.canal.readline().rstrip("\n"))
				self.canal.write("H\n")
				humedadActual = int(self.canal.readline().rstrip("\n"))
                                # Comprobamos que los campos no esten vacios para poder enviar la notificacion
                                if temperaturaActual != '' and humedadActual != '':
					# Creamos la consulta para ver si quiere notificaciones
					consultaNotificacion = "SELECT notificar, temperatura_max, temperatura_min, humedad_max, humedad_min, (SELECT reg_id FROM notificacion WHERE notificacion.id_usuario=cultivo.id_usuario) FROM cultivo WHERE id_cultivo=1"
					# Ejecutamos la consulta en la base de datos
					try:
                                        	db = MySQLdb.connect ("localhost","root","raspberry","cultivo")
	                                        cursor = db.cursor()
        	                                cursor.execute(consultaNotificacion)
						resultadosConsulta = cursor.fetchall()
						for filas in resultadosConsulta:
							quiereNotificacion = filas[0]
							tempMaxUsuario = int(filas[1])
							tempMinUsuario = int(filas[2])
							humMaxUsuario = int(filas[3])
							humMinUsuario = int(filas[4])
							reg_id = filas[5]
					except:
						print "Error consulta notificacion"
					finally:
						if db:
                        	                	db.close()
					print "Temperatura actual: ",temperaturaActual
					print "Humedad actual: ",humedadActual
					# Comprobamos si quiere notificacion el usuario
					if quiereNotificacion == 'S':
						# Enviamos el tipo de notificacion de aviso o peligro
					 	if (tempMinUsuario - 2 >= temperaturaActual and temperaturaActual <= tempMinUsuario) or (tempMaxUsuario <= temperaturaActual and temperaturaActual <= tempMaxUsuario + 2):
							# Enviamos la notificacion de aviso
							if not tempAviso:
						 		self.enviarNotificacion(regId=reg_id, mensaje="temperatura Cultivo 1", tipo="aviso", idMensaje="1")
								tempAviso = True
							tempPeligro = False
					 	elif (tempMinUsuario - 5 <= temperaturaActual - 3 and temperaturaActual - 3 < tempMinUsuario - 2) or (tempMaxUsuario + 5 >= temperaturaActual and temperaturaActual > tempMaxUsuario + 2):
							# Enviamos la notificacion de peligro
							if not tempPeligro:
	                                                	self.enviarNotificacion(regId=reg_id, mensaje="temperatura Cultivo 1", tipo="peligro", idMensaje="1")		  
								tempPeligro = True
							tempAviso = False
					 	if (humMinUsuario - 2 >= humedadActual and humedadActual <= humMinUsuario) or (humMaxUsuario <= humedadActual and humedadActual <= humMaxUsuario + 2):
							# Enviamos la notificacion de aviso
							if not humAviso:
						 		self.enviarNotificacion(regId=reg_id, mensaje="humedad Cultivo 1", tipo="aviso", idMensaje="2")
								humAviso = True
							humPeligro = False
					 	elif (humMinUsuario - 5 <= humedadActual - 3 and humedadActual -3 < humMinUsuario - 2) or (humMaxUsuario + 5 >= humedadActual and humedadActual > humMaxUsuario + 2):
							# Enviamos la notificacion de peligro
							if not humPeligro:
	                                                	self.enviarNotificacion(regId=reg_id, mensaje="humedad Cultivo 1", tipo="peligro", idMensaje="2")		  
								humPeligro = True
							humAviso = False	
					else:	
						print "No quiere notificacion"
				
				# Voy comprobando esto en los distintos canales		
				canal += 1
			# Voy incrementando el tiempo cada segundo para guardar un registro al minuto
			TGuardar += 1
			# Si se ha cumplido que han pasado 60 segundos
			if TGuardar >= 60:
				# Reiniciamos el tiempo de guardar un registro otra vez
				TGuardar = 0
				# Recuperamos la temperatura, humedad y consumo actual
				self.canal.write("T\n")
				temperatura = self.canal.readline()
				self.canal.write("H\n")
				humedad = self.canal.readline()
				self.canal.write("C\n")
				consumo = self.canal.readline()
				# Le quitamos el salto de linea al final
				temperatura = temperatura.rstrip("\n")
				humedad = humedad.rstrip("\n")
				consumo = consumo.rstrip("\n")
				print temperatura,humedad,consumo
				# Creamos la consulta para insertar el valor en la base de datos
				st = "INSERT INTO registro (id_cultivo, fecha, consumo, temperatura, humedad) VALUES (1,'" + str(datetime.datetime.now()).split('.')[0] + "'," + consumo + "," + temperatura + "," + humedad +")"
				print st
				# Comprobamos que los campos no esten vacios para insertarlo
				# en la base de datos
				if temperatura != '' and humedad != '' and consumo != '':
					db = MySQLdb.connect ("localhost","root","raspberry","cultivo")
					cursor = db.cursor()
					cursor.execute (st)
					db.commit()
					db.close()
			

# Se crea la variable del tipo RS485Com que activa el thread y el socket. Se ejecuta
# de forma perpetua.		
m = RS485Com(milisTimeOut=100, port=60000)

