<?php
	/**
	 * Funcion que usaremos para asignar o quitar el cultivo al usuario.
	 *
	 * @author Carlos DaPalma Corchado <cdapalmac@gmail.com>
	 * @author Esdras Valiente Lanaspa <esdras.vl85@gmail.com>
	 * @author Andres Jordan Zambrano <andresjordanz@hotmail.com>
	 *
	 * This is free software, licensed under the GNU General Public License v3.
	 * See http://www.gnu.org/licenses/gpl.html for more information.
	 */

	// Parametros que nos llegan
        $tipo = $_POST["tipo"];
	$cultivo = $_POST["idCultivo"];
        $usuario = $_POST["idUsuario"];
	$nombre = $_POST["nombre"];
        $descripcion = $_POST["descripcion"];
	$notificar = $_POST["notificar"];
	$temperatura = $_POST["temperatura"];
	$temperatura_max = $_POST["temperaturaMax"];
	$temperatura_min = $_POST["temperaturaMin"];
	$humedad = $_POST["humedad"];
	$humedad_max = $_POST["humedadMax"];
	$humedad_min = $_POST["humedadMin"];
	$funcion_luz = $_POST["funcionLuz"];
	$hora_inicio_luz = $_POST["horaInicioLuz"];
	$hora_fin_luz = $_POST["horaFinLuz"];
	$funcion_riego = $_POST["funcionRiego"];
	$hora_inicio_riego = $_POST["horaInicioRiego"];
	$hora_fin_riego = $_POST["horaFinRiego"];
	$funcion_cubierta = $_POST["funcionCubierta"];
	$hora_inicio_cubierta = $_POST["horaInicioCubierta"];
	$hora_fin_cubierta = $_POST["horaFinCubierta"];
	// Variables para acceder a la base de datos
        $servername = "localhost";
        $username = "root";
        $password = "raspberry";
        $bdatos = "cultivo";
	// Conexión con la base de datos
        $conn = new mysqli ($servername,$username,$password,$bdatos);
	// Variable para crear la query que actualiza el cultivo
	$sql = "UPDATE cultivo SET ";
	// Dependiendo del tipo de la función crearemos una query o otra
	if ($tipo == 1) {
		$consulta = "SELECT id_usuario FROM cultivo WHERE id_cultivo = " . $cultivo;
		$result = $conn->query ($consulta);
		$row = $result->fetch_assoc();
		if ($row[id_usuario] == "" || $row[id_usuario] == $usuario) {
			// Si el tipo es 1 añadimos los campos que nos pasan por parametro registrando el cultivo para el usuario especificado por parametro
        		$sql .= "id_usuario='" . $usuario . "', nombre='" . $nombre . "', descripcion='" . $descripcion . "', notificar='" . $notificar . "', temperatura=" . $temperatura . ", temperatura_max=" . $temperatura_max . ", temperatura_min=" . $temperatura_min . ", humedad=" . $humedad . ", humedad_max=" . $humedad_max . ", humedad_min=" . $humedad_min . ", funcion_luz='" . $funcion_luz . "', hora_inicio_luz='" . $hora_inicio_luz . "', hora_fin_luz='" . $hora_fin_luz . "', funcion_riego='" . $funcion_riego . "', hora_inicio_riego='" . $hora_inicio_riego . "', hora_fin_riego='" . $hora_fin_riego . "', funcion_cubierta='" . $funcion_cubierta . "', hora_inicio_cubierta='" . $hora_inicio_cubierta . "', hora_fin_cubierta='" . $hora_fin_cubierta . "'";
		} else {
			print "N";
		}
	} else {
		// Si el tipo es distinto a 0 añadimos los campos en null para dejar el cultivo libre
        	$sql .= "id_usuario=NULL, nombre='Cultivo " . $cultivo . "', descripcion='', notificar='N', temperatura=0, temperatura_max=0, temperatura_min=0, humedad=0, humedad_max=0, humedad_min=0, funcion_luz='N', hora_inicio_luz='', hora_fin_luz='', funcion_riego='N', hora_inicio_riego='', hora_fin_riego='', funcion_cubierta='N', hora_inicio_cubierta='', hora_fin_cubierta=''";
	}
	// Añadimos la clausura where
        $sql .= " WHERE id_cultivo = " . $cultivo;
	// Ejecutamos la query
	$conn->query ($sql);
	// Cerramos la conexión a la base de datos
	$conn->close();
?>

