<?php
	/**
	 * Funcion para obtener los cultivos disponibles o de un usario.
	 *
	 * @author Carlos DaPalma Corchado <cdapalmac@gmail.com>
	 * @author Esdras Valiente Lanaspa <esdras.vl85@gmail.com>
	 * @author Andres Jordan Zambrano <andresjordanz@hotmail.com>
	 *
	 * This is free software, licensed under the GNU General Public License v3.
	 * See http://www.gnu.org/licenses/gpl.html for more information.
	 */

	// Parametros que nos llegan
	$tipo = $_POST["tipo"];
	$usuario = $_POST["usuario"];
	$idcultivo = $_POST["idCultivo"];
	// Datos para la conexión a la base de datos
	$servername = "localhost";
	$username = "root";
	$password = "raspberry";
	$bdatos = "cultivo";
	// Conecxión con la base de datos
	$conn = new mysqli ($servername, $username, $password, $bdatos);
	// Variable para crear la consulta
	$consulta = "SELECT * FROM cultivo";
	// Dependiendo de la variable tipo haremos una consulta o otra
	if ($tipo == 1) {
		// Si el tipo es 1 la consulta sera de los cultivos de un único usuario
		$consulta .= " WHERE id_usuario = '" . $usuario . "'";
	} else {
		// Por defecto la consulta sera de los cultivos que no estan regsitrados por ningun usuario
		$consulta .= " WHERE id_usuario IS NULL";
	}
	// Ejecutamos la consulta y guardamos el resultado
	$sqlResult = $conn->query ($consulta);
	// Creamos una array
	$rows = array();
	// Recorremos las filas que nos retorna la consulta
	while($r = $sqlResult->fetch_assoc()) {
		// Añadimos a la array las filas de la consulta
		$rows[] = $r;
	}
	// Cerramos la conexión a la base de datos
	$conn->close();
	// Formateamos el fichero json
	$json= json_encode($rows, JSON_PRETTY_PRINT);
        header('Content-Type: application/json');
	// Retornamos el json
        print($json);
?>

