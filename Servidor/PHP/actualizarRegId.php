<?php
	/**
	 * Funcion para registrar el registration id del usuario.
	 *
	 * @author Carlos DaPalma Corchado <cdapalmac@gmail.com>
	 * @author Esdras Valiente Lanaspa <esdras.vl85@gmail.com>
	 * @author Andres Jordan Zambrano <andresjordanz@hotmail.com>
	 *
	 * This is free software, licensed under the GNU General Public License v3.
	 * See http://www.gnu.org/licenses/gpl.html for more information.
	 */

	// Parametros que nos llegan
	$reg_id = $_POST["regId"];
	$usuario = $_POST["usuario"];
	// Variables para la conexión con la base de datos
	$servername = "localhost";
	$username = "root";
	$password = "raspberry";
	$bdatos = "cultivo";
	// Conexión con la base de datos
	$conn = new mysqli($servername, $username, $password, $bdatos);
	// Ejecutamos una consulta para comprobar si el usuario de los parametros ya esta registrado en la tabla notificacion
	$result = $conn->query("SELECT * FROM notificacion WHERE id_usuario = '" . $usuario . "'");
	// Si el numero de filas de la consulta es 0 el usuario no esta registrado
	if ($result->num_rows == 0) {
		// Añadimos el usuario con su reg_id
		$consulta = "INSERT INTO notificacion(id_usuario, reg_id) VALUES ('" . $usuario . "', '" . $reg_id . "')";
	} else {
		// Si el usuario ya esta registrado lo actualizamos
		$consulta = "UPDATE notificacion SET reg_id = '" . $reg_id . "' WHERE id_usuario = '" . $usuario  . "'";
	}
	// Ejecutamos la query
	$conn->query($consulta);
	// Cerramos la conexión a la base de datos
	$conn->close();
?>

