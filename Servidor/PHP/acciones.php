<?php
	function doFeina ($cmd) {
		$result = $socket = socket_create (AF_INET, SOCK_STREAM, SOL_TCP);
		if (!$result) {
			print "Error al crear el socket";
			return;
		}
		
		$result = socket_connect ($socket,"localhost",60000);
		if (!$result) {
			print "Error al conectar con el socket";
			return;
		}
		
		socket_send ($socket,$cmd,strlen($cmd),0);
		$buffer = "";
		for ($i=0 ; $i<500 ; $i++) {
			$c = " ";
			socket_recv ($socket,$c,1,MSG_WAITALL);
			if ($c == "\n") break;
			$buffer .= $c;
		}
		socket_close ($socket);
		
		return $buffer;
	}

	$comando = $_REQUEST["param"];
	$orden = doFeina($comando);
	print $orden;

?>

