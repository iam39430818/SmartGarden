<?php
        /**
         * Funcion para obtener si un usuario tiene o no tiene cultivos registrados.
         *
         * @author Carlos DaPalma Corchado <cdapalmac@gmail.com>
         * @author Esdras Valiente Lanaspa <esdras.vl85@gmail.com>
         * @author Andres Jordan Zambrano <andresjordanz@hotmail.com>
         *
         * This is free software, licensed under the GNU General Public License v3.
         * See http://www.gnu.org/licenses/gpl.html for more information.
         */

        // Parametros que nos llegan
        $usuario = $_POST["usuario"];
        // Datos para la conexión a la base de datos
        $servername = "localhost";
        $username = "root";
        $password = "raspberry";
	$bdatos = "cultivo";
        // Conecxión con la base de datos
        $conn = new mysqli ($servername, $username, $password, $bdatos);
        // Consulta para saber si el usuario tiene cultivos registrados
        $consulta = "SELECT * FROM cultivo WHERE id_usuario = '" . $usuario . "'";
        // Ejecutamos la consulta y guardamos el resultado
        $sqlResult = $conn->query ($consulta);
	// Guardamos la primera row
	$row = $sqlResult->fetch_assoc();
	// Si tiene filas enviamos una 'S'
	if ($row != "") {
		print "S";
	}
        $conn->close();
?>

