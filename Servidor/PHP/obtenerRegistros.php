<?php
	/**
	 * Funcion para obterner los registros de un cultivo o un usuario.
	 *
	 * @author Carlos DaPalma Corchado <cdapalmac@gmail.com>
	 * @author Esdras Valiente Lanaspa <esdras.vl85@gmail.com>
	 * @author Andres Jordan Zambrano <andresjordanz@hotmail.com>
	 *
	 * This is free software, licensed under the GNU General Public License v3.
	 * See http://www.gnu.org/licenses/gpl.html for more information.
	 */

        // Parametros que nos llegan
        $usuario = $_POST["usuario"];
        $idcultivo = $_POST["idCultivo"];
	$fecha = $_POST["fechaUsuario"];
	$tipo = $_POST["tipo"];
        // Datos para la conexión a la base de datos
        $servername = "localhost";
        $username = "root";
        $password = "raspberry";
        $bdatos = "cultivo";
        // Conexión con la base de datos
        $conn = new mysqli ($servername, $username, $password, $bdatos);
        // Variable para crear la consulta
        $consulta = "SELECT reg.id_cultivo, cult.nombre, substr(fecha, 6, 2) mes, round(avg(consumo), 2) consumo, round(avg(reg.temperatura), 2) temperatura, round(avg(reg.humedad), 2) humedad FROM registro reg, cultivo cult ";
        // Dependiendo de la variable usuario haremos una consulta o otra
        if ($usuario != null) {
		$consulta = "SELECT reg.id_cultivo, cult.nombre, substr(fecha, 6, 2) mes, round(avg(consumo), 2) consumo, round(avg(reg.temperatura), 2) temperatura, round(avg(reg.humedad), 2) humedad ";
		$consulta .= "FROM registro reg INNER JOIN cultivo cult ON cult.id_cultivo = reg.id_cultivo ";
		$consulta .= "WHERE cult.id_usuario = '" . $usuario . "' and substr(fecha, 1, 4) = '" . $fecha . "' ";
		$consulta .= "GROUP BY reg.id_cultivo";
		if ($tipo == 1) {
			$consulta .= ", substr(fecha, 6, 2)";
		}
        } else {
                // Si le pasamos el id del cultivo, retornará los registros de ese cultivo
                $consulta .= "WHERE reg.id_cultivo = " . $idcultivo . " and cult.id_cultivo = " . $idcultivo  . " and substr(fecha, 1, 4) = '" . $fecha . "' group by substr(fecha, 6, 2)";
        }
        // Ejecutamos la consulta y guardamos el resultado
        $sqlResult = $conn->query ($consulta);
        // Creamos una array
        $rows = array();
        // Recorremos las filas que nos retorna la consulta
        while($r = $sqlResult->fetch_assoc()) {
                // Añadimos a la array las filas de la consulta
                $rows[] = $r;
        }
        // Cerramos la conexión a la base de datos
        $conn->close();
        // Formateamos el fichero json
        $json= json_encode($rows, JSON_PRETTY_PRINT);
        header('Content-Type: application/json');
        // Retornamos el json
        print($json);
?>

