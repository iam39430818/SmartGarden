package org.escoladeltreball.smartgarden.database;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

/**
 * Clase de ayuda a la base de datos.
 *
 * @author Carlos DaPalma Corchado <cdapalmac@gmail.com>
 * @author Esdras Valiente Lanaspa <esdras.vl85@gmail.com>
 * @author Andres Jordan Zambrano <andresjordanz@hotmail.com>
 *         <p/>
 *         This is free software, licensed under the GNU General Public License v3.
 *         See http://www.gnu.org/licenses/gpl.html for more information.
 */
public class DatabaseHelper extends SQLiteOpenHelper {
    // Versión de la base de datos
    public static final int DATABASE_VERSION = 1;
    // Nombre de la base de datos
    public static final String DATABASE_NAME = "cultivo.db";
    // Nombre de la tabla
    public String tableName;
    // Query para crear una tabla
    public static final String CREATE_TABLE = "CREATE TABLE IF NOT EXISTS ";
    // Columna tabla historial de las notificaciones
    public static final String COLUMNAS_TABLA_HISTORIAL_NOTIFICACIONES = "(" +
            CultivoContract.HistorialNotificacion._ID + " integer primary key autoincrement," +
            CultivoContract.HistorialNotificacion.ID_CULTIVO + " text not null, " +
            CultivoContract.HistorialNotificacion.MENSAJE + " text not null)";
    // Query para borrar una tabla
    public static final String DELETE_TABLE = "DROP TABLE IF EXISTS ";

    /**
     * Metodo que usaremos para crear el DatabaseHelper.
     *
     * @param context   de la activity
     * @param tableName nombre de la tabla
     */
    public DatabaseHelper(Context context, String tableName) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
        this.tableName = tableName;
    }

    @Override
    public void onCreate(SQLiteDatabase sqLiteDatabase) {
        sqLiteDatabase.execSQL(CREATE_TABLE + tableName + COLUMNAS_TABLA_HISTORIAL_NOTIFICACIONES);
    }

    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int oldVersion, int newVersion) {
        sqLiteDatabase.execSQL(DELETE_TABLE + tableName);
        onCreate(sqLiteDatabase);
    }

    @Override
    public void onDowngrade(SQLiteDatabase sqLiteDatabase, int oldVersion, int newVersion) {
        onUpgrade(sqLiteDatabase, oldVersion, newVersion);
    }
}