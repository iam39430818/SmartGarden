package org.escoladeltreball.smartgarden.database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;

import org.escoladeltreball.smartgarden.pojo.Cultivo;
import org.escoladeltreball.smartgarden.pojo.HistorialNotificacion;

import java.util.ArrayList;

/**
 * Clase de gestion de la tabla Historial de Notificaciones.
 *
 *
 * @author Carlos DaPalma Corchado <cdapalmac@gmail.com>
 * @author Esdras Valiente Lanaspa <esdras.vl85@gmail.com>
 * @author Andres Jordan Zambrano <andresjordanz@hotmail.com>
 *
 * This is free software, licensed under the GNU General Public License v3.
 * See http://www.gnu.org/licenses/gpl.html for more information.
 */
public class HistorialNotificacionDAO {
    // Nombre de la tabla con la que trabajar
    private static final String TABLE_NAME = CultivoContract.HistorialNotificacion.HISTORIAL_NOTIFICACION;
    // Objeto con el que haremos la gestión de la base de datos
    private SQLiteDatabase database;
    // Objeto que creara la base de datos
    private DatabaseHelper dbHelper;
    // Lista de columnas de la tabla estadisticas
    private static final String[] COLUMNAS = {
            CultivoContract.HistorialNotificacion._ID,
            CultivoContract.HistorialNotificacion.ID_CULTIVO,
            CultivoContract.HistorialNotificacion.MENSAJE
    };

    /**
     * Constructor de la clase.
     *
     * @param context de la Activity
     */
    public HistorialNotificacionDAO(Context context) {
        dbHelper = new DatabaseHelper(context, TABLE_NAME);
    }

    /**
     * Metodo que usaremos para abrir la base de datos.
     */
    public void abrirHistorialNotificacionDAO() {
        // Abrimos la base de datos
        database = dbHelper.getWritableDatabase();
        dbHelper.onCreate(database);
    }

    /**
     * Método para cerrar la base de datos.
     */
    public void cerrarHistorialNotificacionDAO() {
        dbHelper.close();
    }

    /**
     * Metodo para insertar una notificacion al historial en la base de datos
     *
     * @param values los valores de cada columna
     */
    public void insertarHistorialNotificacion(String[] values) {
        ContentValues contentValues = new ContentValues();
        // Recorremos values
        for (int i = 1; i < values.length; i++) {
            // Si el valor es null o esta vacío
            if (values[i] == null || values[i].equals("")) {
                // Guardamos un null en la columna
                contentValues.putNull(COLUMNAS[i]);
            } else {
                // Si no es null guardamos el valor
                contentValues.put(COLUMNAS[i], values[i]);
            }
        }
        // Hacemos la inserción del historial de notificacion en la base de datos
        database.insertOrThrow(TABLE_NAME, null, contentValues);
    }

    /**
     * Método para actualizar un historial de notificacion en la base de datos.
     *
     * @param values valores para actualizar
     */
    public void actualizarHistorialNotificacion(String[] values) {
        ContentValues contentValues = new ContentValues();
        // Recorremos los valores
        for (int i = 1; i < values.length; i++) {
            // Si el valor es null o esta vacío
            if (values[i] == null || values[i].equals("")) {
                // Guardamos un null en la columna
                contentValues.putNull(COLUMNAS[i]);
            } else {
                // Si no es null guardamos el valor
                contentValues.put(COLUMNAS[i], values[i]);
            }
        }
        // Hacemos la actualizacion del historial de notificacion en la base de datos
        database.update(TABLE_NAME, contentValues, COLUMNAS[1] + "='" + values[1] + "'", null);
    }

    /**
     * Método para eliminar un registro en la base de datos.
     */
    public void vaciarTablaHistorialNotificaciones() {
        // Hacemos la eliminacion de un historial de notificacion en la base de datos
        database.delete(TABLE_NAME, null, null);

    }

    /**
     * Método para recuperar el historial de notificaciones.
     *
     * @return una lista con todos los cultivos
     * @throws SQLException
     */
    public ArrayList<String> obtenerListaHistorialNotificaciones() throws SQLException {
        ArrayList<String> list = new ArrayList<>();
        // Obtenemos las filas
        Cursor cursor = database.query(TABLE_NAME, COLUMNAS, null, null, null, null, null);
        // Recorremos el cursor
        while (cursor.moveToNext()) {
                    // Añadimos el cultivo a la lista
                    list.add(cursor.getString(2));
        }
        cursor.close();
        // Retornamos la lista con los cultivos
        return list;
    }

    /**
     * Método para obtener un unico cultivo por el id.
     *
     * @param mac id del cultivo
     * @return el cultivo
     */
    public boolean existeNotificacionPorMac(String mac) {
        // Obtenemos la notificacion por el id
        Cursor cursor = database.query(
                TABLE_NAME,
                COLUMNAS,
                COLUMNAS[1] + " = ?",
                new String[]{mac}, null, null, null);
        HistorialNotificacion historialNotificacion = null;
        // Movemos el cursor a la primera posición
        if (cursor.moveToFirst()) {
            //Si devuelve resultado guardamos el historial de notificacion
            historialNotificacion = new HistorialNotificacion(
                    cursor.getInt(0) + "",
                    cursor.getString(1),
                    cursor.getString(2));
        }
        cursor.close();
        // Retornamos el historial de notificacion
        return historialNotificacion != null;
    }
}
