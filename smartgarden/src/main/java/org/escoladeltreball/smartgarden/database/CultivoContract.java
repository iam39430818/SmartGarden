package org.escoladeltreball.smartgarden.database;

import android.provider.BaseColumns;

/**
 * Clase de la estructura de la base de datos.
 *
 * @author Carlos DaPalma Corchado <cdapalmac@gmail.com>
 * @author Esdras Valiente Lanaspa <esdras.vl85@gmail.com>
 * @author Andres Jordan Zambrano <andresjordanz@hotmail.com>
 *         <p/>
 *         This is free software, licensed under the GNU General Public License v3.
 *         See http://www.gnu.org/licenses/gpl.html for more information.
 */
public class CultivoContract implements BaseColumns {

    private CultivoContract() {
    }

    /**
     * Clase para la estructura de la tabla.
     */
    static abstract class HistorialNotificacion implements BaseColumns {

        // Tabla que crearemos en la base de datos
        public static final String HISTORIAL_NOTIFICACION = "historial_notificacion";
        // Las columnas de las tabla
        public static final String ID_CULTIVO = "id_cultivo";
        public static final String MENSAJE = "mensaje";
    }
}
