package org.escoladeltreball.smartgarden.fragments;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.InflateException;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import org.escoladeltreball.smartgarden.R;
import org.escoladeltreball.smartgarden.utils.VariablesGlobales;

/**
 * FragmentAjustes que se cargara cuando cliquemos en ajustes de la PantallaMenuLateral.
 *
 *
 * @author Carlos DaPalma Corchado <cdapalmac@gmail.com>
 * @author Esdras Valiente Lanaspa <esdras.vl85@gmail.com>
 * @author Andres Jordan Zambrano <andresjordanz@hotmail.com>
 *
 * This is free software, licensed under the GNU General Public License v3.
 * See http://www.gnu.org/licenses/gpl.html for more information.
 */
public class FragmentAjustes extends Fragment {

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Para que no se duplique el id removemos la view actual
        if (((VariablesGlobales) getActivity().getApplication()).getView() != null) {
            ViewGroup parent = (ViewGroup) ((VariablesGlobales) getActivity().getApplication()).getView().getParent();
            if (parent != null)
                parent.removeView(((VariablesGlobales) getActivity().getApplication()).getView());
        }
        try {
            // Asignamos el titulo del fragment
            getActivity().setTitle(getString(R.string.fragmentajustes_titulo));
            // Inflamos el layout
            ((VariablesGlobales) getActivity().getApplication()).setView(inflater.inflate(R.layout.fragment_ajustes, container, false));
        } catch (InflateException e) {
            e.printStackTrace();
        }
        // Retornamos la view inflada
        return ((VariablesGlobales) getActivity().getApplication()).getView();
    }
}


