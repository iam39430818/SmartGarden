package org.escoladeltreball.smartgarden.fragments;


import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import com.github.mikephil.charting.animation.Easing;
import com.github.mikephil.charting.charts.PieChart;
import com.github.mikephil.charting.components.Legend;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.data.PieData;
import com.github.mikephil.charting.data.PieDataSet;
import com.github.mikephil.charting.utils.ColorTemplate;
import com.github.mikephil.charting.utils.LargeValueFormatter;

import org.escoladeltreball.smartgarden.R;
import org.escoladeltreball.smartgarden.adapters.AdaptadorListviewRegistrosCultivo;
import org.escoladeltreball.smartgarden.pojo.Registro;
import org.escoladeltreball.smartgarden.utils.Utils;

import java.util.ArrayList;

/**
 * FragmentTabHostRegistrosCultivoTemperatura que se cargará en el FragmentEstadisticaCultivo.
 *
 * @author Carlos DaPalma Corchado <cdapalmac@gmail.com>
 * @author Esdras Valiente Lanaspa <esdras.vl85@gmail.com>
 * @author Andres Jordan Zambrano <andresjordanz@hotmail.com>
 *         <p/>
 *         This is free software, licensed under the GNU General Public License v3.
 *         See http://www.gnu.org/licenses/gpl.html for more information.
 */
public class FragmentTabHostRegistrosCultivoTemperatura extends Fragment {
    // View del layout actual
    private View viewFragmentTabHostRegistrosCultivoTemperatura;
    // Lista de todas los registros de temperatura del cultivo
    private ArrayList<Registro> listaRegistrosCultivoTemperatura;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Recuperamos la lista de los registros
        listaRegistrosCultivoTemperatura = getArguments().getParcelableArrayList("listaRegistrosEstadisticasCultivo");
        // Inflamos el layout en este fragment
        viewFragmentTabHostRegistrosCultivoTemperatura = inflater.inflate(R.layout.fragment_tab_host_estadisticas_cultivo_temperatura, container, false);
        // Cargamos la lista de registros
        ListView listViewTabHostRegistrosCultivoTemperatura = (ListView) viewFragmentTabHostRegistrosCultivoTemperatura.findViewById(R.id.listViewTabHostRegistrosCultivoTemperatura);
        // Recuperamos el linearLayoutErrorRegistrosCultivoTemperatura
        LinearLayout linearLayoutErrorRegistrosCultivoTemperatura = (LinearLayout) viewFragmentTabHostRegistrosCultivoTemperatura.findViewById(R.id.linearLayoutErrorRegistrosCultivoTemperatura);
        // Ponemos la imageview de no item en escala de grises
        ImageView imagen = (ImageView) viewFragmentTabHostRegistrosCultivoTemperatura.findViewById(R.id.imgViewErrorRegistrosCultivoTemperatura);
        // Coloreamos la imagen de gris
        Utils.imgViewPintarImagen(R.color.color_gris_claro, imagen);
        // Inflamos el text view de error
        TextView tvErrorRegistrosCultivoTemperatura = (TextView) viewFragmentTabHostRegistrosCultivoTemperatura.findViewById(R.id.tvErrorRegistrosCultivoTemperatura);
        // En caso que no haya items se mostrara un mensaje indicandolo
        listViewTabHostRegistrosCultivoTemperatura.setEmptyView(linearLayoutErrorRegistrosCultivoTemperatura);
        // Comprobamos que haya items y los mostramos
        if (listaRegistrosCultivoTemperatura != null) {
            // Creamos el adaptador
            AdaptadorListviewRegistrosCultivo adaptadorListviewRegistrosCultivoTemperatura = new AdaptadorListviewRegistrosCultivo(getContext(), listaRegistrosCultivoTemperatura, "T");
            // Asignamos el adaptador a la listview
            listViewTabHostRegistrosCultivoTemperatura.setAdapter(adaptadorListviewRegistrosCultivoTemperatura);
            // Cargamos los graficos
            cargarGraficosConsumoTemperatura();
        } else if (!Utils.tieneConexion(getActivity())) {
            // Indicamos un mensaje de que no dispone de conexion
            tvErrorRegistrosCultivoTemperatura.setText(getString(R.string.fragmenttabhostestadisticascultivotemperatura_tvnoitemslistviewcultivos_noconexion));
        } else {
            // Indicamos un mensaje de que no se han encontrado cultivos
            tvErrorRegistrosCultivoTemperatura.setText(getString(R.string.fragmenttabhostestadisticascultivotemperatura_tvnoitemslistviewcultivos_noencontrado));
        }
        // Retornamos la vista inflada
        return viewFragmentTabHostRegistrosCultivoTemperatura;
    }

    /**
     * Metodo que usaremos para cargar los graficos.
     */
    private void cargarGraficosConsumoTemperatura() {
        // Obtenemos el grafico de temperatura del layout
        PieChart graficoTemperatura = (PieChart) viewFragmentTabHostRegistrosCultivoTemperatura.findViewById(R.id.estadisticasCultivoGraficoTemperatura);
        // No lo mostramos los valores en %
        graficoTemperatura.setUsePercentValues(false);
        // No le asignamos ningun titulo
        graficoTemperatura.setDescription("");
        // Pintamos el titulo en el centro del grafico
        graficoTemperatura.setDrawCenterText(true);
        // Asignamos el texto del centro del grafico
        graficoTemperatura.setCenterText(getString(R.string.fragmenttabhostestadisticascultivotemperatura_titulo_grafico));
        // Habilitamos que se pueda rotar
        graficoTemperatura.setRotationEnabled(true);
        // Ocultamos el texto de los valores del grafico
        graficoTemperatura.setDrawSliceText(false);
        // Deshacer destacados
        graficoTemperatura.highlightValues(null);
        graficoTemperatura.invalidate();
        graficoTemperatura.animateY(1500, Easing.EasingOption.EaseInOutQuad);
        graficoTemperatura.setDrawHoleEnabled(true);
        graficoTemperatura.setHoleColorTransparent(true);
        graficoTemperatura.setTransparentCircleColor(Color.WHITE);
        graficoTemperatura.setDragDecelerationFrictionCoef(0.95f);
        graficoTemperatura.setHoleRadius(45f);
        graficoTemperatura.setTransparentCircleRadius(48f);
        graficoTemperatura.setRotationAngle(0);

        // Creamos la lista de los datos de los graficos
        ArrayList<String> listaMeses = new ArrayList<>();
        ArrayList<Entry> listaEntryMeses = new ArrayList<>();
        // Los valores de numero de meses los asignamos como integer
        float numeroDeMeses;
        // Obtenemos la lista de meses con  el numero de meses
        for (int i = 0; i < listaRegistrosCultivoTemperatura.size(); i++) {
            listaMeses.add(listaRegistrosCultivoTemperatura.get(i).getMes());
            numeroDeMeses = Float.valueOf("" + Float.parseFloat(listaRegistrosCultivoTemperatura.get(i).getTemperatura()));
            listaEntryMeses.add(new Entry(numeroDeMeses, -1));
        }

        // Añadimos los colores
        ArrayList<Integer> colors = new ArrayList<>();
        for (int c : ColorTemplate.VORDIPLOM_COLORS)
            colors.add(c);
        for (int c : ColorTemplate.PASTEL_COLORS)
            colors.add(c);
        for (int c : ColorTemplate.JOYFUL_COLORS)
            colors.add(c);
        for (int c : ColorTemplate.COLORFUL_COLORS)
            colors.add(c);
        for (int c : ColorTemplate.LIBERTY_COLORS)
            colors.add(c);
        colors.add(ColorTemplate.getHoloBlue());

        // Creamos la leyenda del grafico de temperatura
        Legend lPie1 = graficoTemperatura.getLegend();
        lPie1.setPosition(Legend.LegendPosition.LEFT_OF_CHART_CENTER);
        lPie1.setXEntrySpace(7f);
        // Asignamos el espacio entre lineas
        lPie1.setYEntrySpace(5f);
        lPie1.setYOffset(0f);
        // Asignamos el tamaño de la letra
        lPie1.setTextSize(16f);
        // Creamos el valor del numero de meses
        PieDataSet valorGraficoNMeses = new PieDataSet(listaEntryMeses, "");
        valorGraficoNMeses.setSliceSpace(3f);
        valorGraficoNMeses.setSelectionShift(5f);
        valorGraficoNMeses.setColors(colors);
        // Creamos los valores del grafico de temperatura
        PieData valoresGraficosNMeses = new PieData(listaMeses, valorGraficoNMeses);
        valoresGraficosNMeses.setValueFormatter(new LargeValueFormatter());
        valoresGraficosNMeses.setValueTextSize(16f);
        valoresGraficosNMeses.setValueTextColor(Color.BLACK);
        // Asignamos los valores al grafico de temperatura
        graficoTemperatura.setData(valoresGraficosNMeses);
        // Mostramos el grafico de temperatura
        graficoTemperatura.setVisibility(View.VISIBLE);

    }

}
