package org.escoladeltreball.smartgarden.fragments;

import android.os.Bundle;
import android.preference.CheckBoxPreference;
import android.preference.Preference;
import android.preference.PreferenceFragment;
import android.preference.PreferenceManager;
import android.support.design.widget.Snackbar;

import org.escoladeltreball.smartgarden.R;
import org.escoladeltreball.smartgarden.gcm.AsyncTaskDesregistrarDispositivoGCM;
import org.escoladeltreball.smartgarden.gcm.AsyncTaskRegistrarDispositivoGCM;
import org.escoladeltreball.smartgarden.utils.VariablesGlobales;
import org.escoladeltreball.smartgarden.utils.Utils;

/**
 * AjustesPreferenceFragment que se cargara en el FragmentAjustes.
 *
 * @author Carlos DaPalma Corchado <cdapalmac@gmail.com>
 * @author Esdras Valiente Lanaspa <esdras.vl85@gmail.com>
 * @author Andres Jordan Zambrano <andresjordanz@hotmail.com>
 *         <p/>
 *         This is free software, licensed under the GNU General Public License v3.
 *         See http://www.gnu.org/licenses/gpl.html for more information.
 */
public class AjustesPreferenceFragment extends PreferenceFragment {

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        // Asignamos nuestras preferencias
        PreferenceManager preferenceManager = getPreferenceManager();
        preferenceManager.setSharedPreferencesName("ajustes_smartgarden");
        // Cargamos el layout de las preferencias desde el archivo XML
        addPreferencesFromResource(R.xml.ajustes);

        // Recuperamos el checkbox notificaciones
        ((VariablesGlobales) getActivity().getApplication()).setCheckBoxPreference((CheckBoxPreference) getPreferenceManager().findPreference("opcChckNotificaciones"));
        // Añadimos un escuchador de cambios al checkbox de notificaciones
        ((VariablesGlobales) getActivity().getApplication()).getCheckBoxPreference().setOnPreferenceChangeListener(new Preference.OnPreferenceChangeListener() {
            public boolean onPreferenceChange(Preference preference, Object newValue) {
                if (Utils.tieneConexion(getActivity()) && newValue instanceof Boolean) {
                    Boolean boolVal = (Boolean) newValue;
                    switch (boolVal.toString()) {
                        // Si tiene el checkbox de las preferencias activado y lo desactiva
                        case "false":
                            // Desregistramos el dispositivo de GCM
                            new AsyncTaskDesregistrarDispositivoGCM(getActivity()).execute();
                            break;
                        // Si tiene el checkbox de las preferencias desactivado y lo activa
                        default:
                            // Registramos el dispositivo a GCM
                            new AsyncTaskRegistrarDispositivoGCM(getActivity(), true).execute();
                            break;
                    }
                    // En caso que no disponga de conexion a la red mostramos un mensaje indicandolo
                } else {
                    Snackbar snackbar = Snackbar.make(getActivity().findViewById(android.R.id.content),
                            getString(R.string.ajustespreferencefragment_snackbar_texto), Snackbar.LENGTH_LONG);
                    Utils.snackbarTextoVerde(getActivity(), snackbar);
                    snackbar.show();
                    return false;
                }
                return true;
            }
        });
    }
}