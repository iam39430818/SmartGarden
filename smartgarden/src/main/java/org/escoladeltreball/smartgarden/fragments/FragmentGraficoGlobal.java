package org.escoladeltreball.smartgarden.fragments;


import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.github.mikephil.charting.charts.LineChart;
import com.github.mikephil.charting.components.Legend;
import com.github.mikephil.charting.components.XAxis;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.data.LineData;
import com.github.mikephil.charting.data.LineDataSet;
import com.github.mikephil.charting.utils.ColorTemplate;

import org.escoladeltreball.smartgarden.R;
import org.escoladeltreball.smartgarden.pojo.Registro;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Map;
import java.util.TreeMap;


/**
 * FragmentGraficoGlobal que se cargara cuando seleccionemos un item de la action bar en
 * FragmentEstadisticasGlobales.
 *
 * @author Carlos DaPalma Corchado <cdapalmac@gmail.com>
 * @author Esdras Valiente Lanaspa <esdras.vl85@gmail.com>
 * @author Andres Jordan Zambrano <andresjordanz@hotmail.com>
 *         <p/>
 *         This is free software, licensed under the GNU General Public License v3.
 *         See http://www.gnu.org/licenses/gpl.html for more information.
 */
public class FragmentGraficoGlobal extends Fragment {
    // View que se infla en el fragment
    private View viewFragmentGraficoGlobal;
    private String tipoGrafico;
    private TreeMap<Integer, Registro> treeMapRegistros;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // Inflamos el layout de este fragment
        viewFragmentGraficoGlobal = inflater.inflate(R.layout.fragment_grafico_global, container, false);
        // Recuperamos el tipo de grafico que queremos mostrar
        tipoGrafico = getArguments().getString("graficoTipo");
        // Si el tipo de grafico es null mostramos el titulo por defecto
        if (tipoGrafico != null) {
            // Por cada tipo seteamos un titulo
            switch (tipoGrafico) {
                case "E":
                    // Titulo para el grafico de electricidad
                    getActivity().setTitle(getString(R.string.fragmentgraficoglobal_titulo_electricidad));
                    break;
                case "T":
                    // Titulo para el grafico de temperatura
                    getActivity().setTitle(getString(R.string.fragmentgraficoglobal_titulo_temperatura));
                    break;
                default:
                    // Titulo para el grafico de humedad
                    getActivity().setTitle(getString(R.string.fragmentgraficoglobal_titulo_humedad));
                    break;
            }
        }
        // Recuperamos la lista de FragmentEstadisticasGlobales
        treeMapRegistros = (TreeMap<Integer, Registro>)
                getArguments().getSerializable("treeMapEstadisticasGlobalesRegistros");
        if (treeMapRegistros == null) {
            treeMapRegistros = new TreeMap<>();
        }
        // Cargamos el gráfico
        cargarGraficoGlobal();
        // Retornamos el layout inflado
        return viewFragmentGraficoGlobal;
    }

    /**
     * Método que usaremos para cargar el gráfico.
     */
    private void cargarGraficoGlobal() {
        LineChart lineChart = (LineChart) viewFragmentGraficoGlobal.findViewById(R.id.estadisticasGlobalesGraficoElectricidad);
        // Creamos la lista de los nombres de los cultivos
        ArrayList<String> nombreCultivos = new ArrayList<>();
        for (Map.Entry<Integer, Registro> entry : treeMapRegistros.entrySet()) {
            Registro registro = entry.getValue();
            nombreCultivos.add(registro.getNombre());
        }
        // Creamos la lista de las estadisticas de cada cultivo
        ArrayList<float[]> estadisticasCultivos = new ArrayList<>();
        switch (tipoGrafico) {
            case "E":
                for (Map.Entry<Integer, Registro> entry : treeMapRegistros.entrySet()) {
                    Registro registro = entry.getValue();
                    float[] valores = new float[12];
                    for (int i = 0; i < valores.length; i++) {
                        valores[i] = registro.getValorMesesConsumo(i);
                    }
                    estadisticasCultivos.add(valores);
                }
                break;
            case "T":
                for (Map.Entry<Integer, Registro> entry : treeMapRegistros.entrySet()) {
                    Registro registro = entry.getValue();
                    float[] valores = new float[12];
                    for (int i = 0; i < valores.length; i++) {
                        valores[i] = registro.getValorMesesTemperatura(i);
                    }
                    estadisticasCultivos.add(valores);
                }
                break;
            case "H":
                for (Map.Entry<Integer, Registro> entry : treeMapRegistros.entrySet()) {
                    Registro registro = entry.getValue();
                    float[] valores = new float[12];
                    for (int i = 0; i < valores.length; i++) {
                        valores[i] = registro.getValorMesesHumedad(i);
                    }
                    estadisticasCultivos.add(valores);
                }
                break;
        }
        // Guardamos los colores
        ArrayList<Integer> colores = colores();
        // Creamos las lineas para cada cultivo con sus estadsticas
        ArrayList<LineDataSet> dataset = new ArrayList<>();
        for (int i = 0; i < nombreCultivos.size(); i++) {
            String nombreDelCultivo = nombreCultivos.get(i).length() > 10 ? nombreCultivos.get(i).substring(0, 10) : nombreCultivos.get(i);
            dataset.add(crearLineaCultivo(estadisticasCultivos.get(i), nombreDelCultivo, colores.get(i + 1)));
        }
        // Añadimos los datos al grafico
        LineData datos = new LineData(meses(), dataset);
        lineChart.setData(datos);
        lineChart.animateXY(2000, 2000);
        lineChart.setGridBackgroundColor(128);
        setXAxis(lineChart);
        lineChart.getAxisRight().setDrawLabels(false);
        lineChart.setBorderColor(255);
        lineChart.getLegend().setEnabled(true);
        lineChart.setNoDataText(getActivity().getString(R.string.fragment_grafico_global_sin_cultivos));
        lineChart.setDescription("");
        lineChart.getLegend().setPosition(Legend.LegendPosition.RIGHT_OF_CHART);
        lineChart.invalidate();
    }

    /**
     * Método para crear las lines del gráfico.
     *
     * @param estadisticas  los datos de cada mes
     * @param nombreCultivo el nomg¡bre del cultivo
     * @return la linea con los datos
     */
    private LineDataSet crearLineaCultivo(float[] estadisticas, String nombreCultivo, int color) {
        ArrayList<Entry> datosEstadisticas = new ArrayList<>();
        // Recorremos los datos y lo añadimos a la linea
        for (int i = 0; i < estadisticas.length; i++) {
            float consumo = estadisticas[i];
            datosEstadisticas.add(new Entry(consumo, i));
        }
        LineDataSet lineDataSet = new LineDataSet(datosEstadisticas, nombreCultivo);
        lineDataSet.setDrawCubic(true);
        lineDataSet.setColor(color);
        return lineDataSet;
    }

    /**
     * Los meses del año.
     *
     * @return array list con los meses
     */
    private ArrayList<String> meses() {
        ArrayList<String> arrayListMeses = new ArrayList<>();
        String[] meses = getResources().getStringArray(R.array.arrayMesesAbreviados);
        Collections.addAll(arrayListMeses, meses);
        return arrayListMeses;
    }

    /**
     * Posición de los meses en el gráfico.
     *
     * @param chart el gráfico
     */
    private void setXAxis(LineChart chart) {
        XAxis xAxis = chart.getXAxis();
        xAxis.setPosition(XAxis.XAxisPosition.BOTTOM);
        xAxis.setTextSize(10f);
        xAxis.setTextColor(Color.BLACK);
        xAxis.setDrawGridLines(true);
        xAxis.setDrawAxisLine(true);
    }

    /**
     * Los colores que le pasaremos a las lineas.
     *
     * @return array list de los colores
     */
    private ArrayList<Integer> colores() {
        // Añadimos los colores
        ArrayList<Integer> colors = new ArrayList<>();
        for (int c : ColorTemplate.VORDIPLOM_COLORS)
            colors.add(c);
        for (int c : ColorTemplate.PASTEL_COLORS)
            colors.add(c);
        for (int c : ColorTemplate.JOYFUL_COLORS)
            colors.add(c);
        for (int c : ColorTemplate.COLORFUL_COLORS)
            colors.add(c);
        for (int c : ColorTemplate.LIBERTY_COLORS)
            colors.add(c);
        colors.add(ColorTemplate.getHoloBlue());
        return colors;
    }
}