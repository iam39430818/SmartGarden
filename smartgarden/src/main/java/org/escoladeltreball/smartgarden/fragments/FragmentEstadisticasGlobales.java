package org.escoladeltreball.smartgarden.fragments;


import android.app.ProgressDialog;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.escoladeltreball.smartgarden.R;
import org.escoladeltreball.smartgarden.adapters.AdaptadorViewPagerFragmentEstadisticasGlobales;
import org.escoladeltreball.smartgarden.json.JSONPullParserRegistros;
import org.escoladeltreball.smartgarden.pojo.Registro;
import org.escoladeltreball.smartgarden.utils.ObtenerDatosJSON;
import org.escoladeltreball.smartgarden.utils.Utils;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.TreeMap;


/**
 * FragmentEstadisticasGlobales que se cargara cuando cliquemos en estadisticas de la PantallaMenuLateral.
 *
 * @author Carlos DaPalma Corchado <cdapalmac@gmail.com>
 * @author Esdras Valiente Lanaspa <esdras.vl85@gmail.com>
 * @author Andres Jordan Zambrano <andresjordanz@hotmail.com>
 *         <p/>
 *         This is free software, licensed under the GNU General Public License v3.
 *         See http://www.gnu.org/licenses/gpl.html for more information.
 */
public class FragmentEstadisticasGlobales extends Fragment {
    // Tablayout del FragmentEstadisticasGlobales
    private TabLayout tabLayout;
    // ViewPager del FragmentEstadisticasGlobales
    private ViewPager viewPager;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // Asigno el titulo a la action bar
        getActivity().setTitle(getString(R.string.fragmentestadisticasglobales_titulo));
        // Mostramos los items de la action bar
        setHasOptionsMenu(true);
        // Inflamos el layout que tiene el tabhost customizado en la parte superior
        View viewFragmentEstadisticasGlobales = inflater.inflate(R.layout.fragment_estadisticas_globales, container, false);
        // Recuperamos el tab layout
        tabLayout = (TabLayout) viewFragmentEstadisticasGlobales.findViewById(R.id.tabLayoutFragmentEstadisticasGlobales);
        // Recuperamos el view pager
        viewPager = (ViewPager) viewFragmentEstadisticasGlobales.findViewById(R.id.viewPagerFragmentEstadisticasGlobales);
        // Hilo para cargar las estadisticas globales
        new CargarEstadisticasGlobales(Build.SERIAL).execute();
        // Retornamos la vista inflada
        return viewFragmentEstadisticasGlobales;
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        CrearMenu(menu);
    }

    private void CrearMenu(Menu menu) {
        MenuItem estadisticas = menu.add(0, 0, 0, getString(R.string.action_bar_grafico_estadisticasglobales));
        estadisticas.setIcon(R.drawable.ic_estadisticas);
        estadisticas.setShowAsAction(MenuItem.SHOW_AS_ACTION_IF_ROOM | MenuItem.SHOW_AS_ACTION_WITH_TEXT);

        MenuItem sincronizar = menu.add(1, 1, 1, R.string.action_bar_sincronizar_estadisticasglobales);
        sincronizar.setIcon(R.drawable.ic_sincronizar);
        sincronizar.setShowAsAction(MenuItem.SHOW_AS_ACTION_IF_ROOM | MenuItem.SHOW_AS_ACTION_WITH_TEXT);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        return SeleccionOpcionMenu(item);
    }

    private boolean SeleccionOpcionMenu(MenuItem item) {
        switch (item.getItemId()) {
            case 1:
                // Obtenemos los datos del servidor y los actualizamos
                new CargarEstadisticasGlobales(Build.SERIAL).execute();
                return true;
        }
        return false;
    }

    /**
     * Hilo que usaremos para la obtencion de las estadisticas globales.
     */
    private class CargarEstadisticasGlobales extends AsyncTask<Void, Void, Void> {
        // Lista con los totales y los cultivos
        private ArrayList<Registro> listaEstadisticasGlobalesTotalesRegistros;
        // Lista con los meses y los cultivos
        private TreeMap<Integer, Registro> treeMapEstadisticasGlobalesRegistros;
        // Progress dialog para mostrar el progreso
        private ProgressDialog dialogEstadisticasCultivo;
        // Variable para guardar el id del usuario
        private String idUsuario;

        // Constructor para recuperar el id del usuario
        public CargarEstadisticasGlobales(String idUsuario) {
            this.idUsuario = idUsuario;
        }

        @Override
        protected void onPreExecute() {
            // Progress dialog que se mostrara para cargar las estadisticas globales
            dialogEstadisticasCultivo = new ProgressDialog(getActivity());
            dialogEstadisticasCultivo.setTitle(getString(R.string.fragmentestadisticasglobales_dialog_titulo));
            dialogEstadisticasCultivo.setMessage(getString(R.string.fragmentestadisticasglobales_dialog_mensaje));
            dialogEstadisticasCultivo.setProgressStyle(ProgressDialog.STYLE_SPINNER);
            dialogEstadisticasCultivo.setIndeterminate(true);
            dialogEstadisticasCultivo.setCancelable(false);
            dialogEstadisticasCultivo.show();
        }

        @Override
        protected Void doInBackground(Void... arg0) {
            // Obtenemos la lista de los registros con y sin los totales
            ObtenerListaRegistros();
            ObtenerListaTotalesRegistros();
            return null;
        }

        // Metodo que hará la función del tratamiento del JSON
        protected void onPostExecute(Void Void) {
            super.onPostExecute(Void);
            // Ocultamos el progress dialog
            dialogEstadisticasCultivo.dismiss();
            // Creamos el bundle que enviaremos a los diferentes fragments del view pager
            // con al lista de los registro obtenidos
            Bundle bundle = new Bundle();
            bundle.putSerializable("treeMapEstadisticasGlobalesRegistros", treeMapEstadisticasGlobalesRegistros);
            bundle.putParcelableArrayList("listaEstadisticasGlobalesTotalesRegistros", listaEstadisticasGlobalesTotalesRegistros);
            // Creamos el adaptador para el view pager
            AdaptadorViewPagerFragmentEstadisticasGlobales adaptadorViewPagerFragmentEstadisticasGlobales =
                    new AdaptadorViewPagerFragmentEstadisticasGlobales(getActivity(), getChildFragmentManager(), bundle);
            // Asignamos al view pager al adaptador
            viewPager.setAdapter(adaptadorViewPagerFragmentEstadisticasGlobales);
            // Hacemos un hilo para el setupWithViewPager porque sino no funciona debido a un fallo en la
            // libreria de soporte
            tabLayout.post(new Runnable() {
                @Override
                public void run() {
                    tabLayout.setupWithViewPager(viewPager);
                }
            });
        }

        /**
         * Metodo que usaremos para obtener la lista de los registros con los totales de cada cultivo.
         */
        private void ObtenerListaTotalesRegistros() {
            // Creamos una instancia de ObtenerDatosJSON
            ObtenerDatosJSON datosJSON = new ObtenerDatosJSON();
            // Creamos los parametros para la funcion
            List<NameValuePair> parametros = new ArrayList<>();
            // Añadimos a los parametros el id del usuario y el año actual
            parametros.add(new BasicNameValuePair("usuario", idUsuario));
            parametros.add(new BasicNameValuePair("fechaUsuario", Calendar.getInstance().get(Calendar.YEAR) + ""));
            // Hacemos la solicitud de obtener el documento JSON, en este caso un POST
            String documentoJSON = datosJSON.obtenerDocumentoJSON(Utils.URL_OBTENER_REGISTROS, ObtenerDatosJSON.POST, parametros);
            // Parseamos el documento obtenido
            JSONPullParserRegistros jsonPullParser = new JSONPullParserRegistros(getActivity(), documentoJSON);
            listaEstadisticasGlobalesTotalesRegistros = jsonPullParser.parseArrayList();
        }

        /**
         * Metodo que usaremos para obtener la lista de los registros con los valores de cada cultivo.
         */
        private void ObtenerListaRegistros() {
            // Creamos una instancia de ObtenerDatosJSON
            ObtenerDatosJSON datosJSON = new ObtenerDatosJSON();
            // Creamos los parametros para la funcion
            List<NameValuePair> parametros = new ArrayList<>();
            // Añadimos a los parametros el id del usuario, el año actual y el tipo
            parametros.add(new BasicNameValuePair("usuario", idUsuario));
            parametros.add(new BasicNameValuePair("fechaUsuario", Calendar.getInstance().get(Calendar.YEAR) + ""));
            parametros.add(new BasicNameValuePair("tipo", "1"));
            // Hacemos la solicitud de obtener el documento JSON, en este caso un POST
            String documentoJSON = datosJSON.obtenerDocumentoJSON(Utils.URL_OBTENER_REGISTROS, ObtenerDatosJSON.POST, parametros);
            // Parseamos el documento obtenido
            JSONPullParserRegistros jsonPullParser = new JSONPullParserRegistros(getActivity(), documentoJSON);
            treeMapEstadisticasGlobalesRegistros = jsonPullParser.parseTreeMap();
        }
    }

}
