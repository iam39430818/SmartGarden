package org.escoladeltreball.smartgarden.fragments;


import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import com.github.amlcurran.showcaseview.ShowcaseView;
import com.github.amlcurran.showcaseview.targets.ViewTarget;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.escoladeltreball.smartgarden.R;
import org.escoladeltreball.smartgarden.activities.PantallaRegistroCultivo;
import org.escoladeltreball.smartgarden.pojo.Cultivo;
import org.escoladeltreball.smartgarden.utils.ObtenerDatosJSON;
import org.escoladeltreball.smartgarden.utils.Utils;

import java.util.ArrayList;
import java.util.List;

/**
 * FragmentCultivo que se cargará en pulsar un item de la lista de FragmentCultivosRegistrados.
 *
 * @author Carlos DaPalma Corchado <cdapalmac@gmail.com>
 * @author Esdras Valiente Lanaspa <esdras.vl85@gmail.com>
 * @author Andres Jordan Zambrano <andresjordanz@hotmail.com>
 *         <p/>
 *         This is free software, licensed under the GNU General Public License v3.
 *         See http://www.gnu.org/licenses/gpl.html for more information.
 */
public class FragmentCultivo extends Fragment implements View.OnClickListener {
    // Variable para guardar el cultivo
    private Cultivo cultivo;
    // TextView y ImageButtons de la vista
    private TextView nombreDelCultivo, descripcionDelCultivo;
    private ImageView buttonInfoFuncManual;
    private ImageButton botonLuz, botonRiego, botonCubierta;
    // Los estados de las funciones de luz, riego y la cubierta
    private boolean estadoLuz, estadoRiego, estadoCubierta;
    // Los items de la action bar
    private MenuItem ic_estadisticas, ic_editar, ic_borrar;
    private ShowcaseView showcaseView;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // Inflamos el Fragment con el layout y lo guardamos
        View viewCultivo = inflater.inflate(R.layout.fragment_cultivo, container, false);
        // Asigno el titulo a la action bar
        getActivity().setTitle(getString(R.string.titulo_fragment_cultivo));
        // Mostramos los items de la action bar
        setHasOptionsMenu(true);
        // Recuperamos el cultivo de FragmentCultivosRegistrados
        cultivo = getArguments().getParcelable("cultivo");
        // Recuperamos los TextView del nombre y la descripción del cultivo
        nombreDelCultivo = (TextView) viewCultivo.findViewById(R.id.nombreDelCultivo);
        descripcionDelCultivo = (TextView) viewCultivo.findViewById(R.id.descripcionDelCultivo);
        // Recuperamos los ImageButton de las acciones
        botonLuz = (ImageButton) viewCultivo.findViewById(R.id.luz);
        botonRiego = (ImageButton) viewCultivo.findViewById(R.id.riego);
        botonCubierta = (ImageButton) viewCultivo.findViewById(R.id.cubierta);
        // Inflamos el imageButton de información en las funciones automáticas y mostramos la información
        buttonInfoFuncManual = (ImageView) viewCultivo.findViewById(R.id.imgViewInfoFuncManuales);
        // Registramos los ImageButton
        botonLuz.setOnClickListener(this);
        botonRiego.setOnClickListener(this);
        botonCubierta.setOnClickListener(this);
        buttonInfoFuncManual.setOnClickListener(this);
        // Actualizamos la informacion del cultivo
        new ActualizarInformacionCultivo().execute();
        // Retornamos la view
        return viewCultivo;
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        // Creamos los items del menu de la ActionBar
        ic_estadisticas = crearItemMenu(menu, 0, getString(R.string.action_bar_estadisticas_cultivo), R.drawable.ic_estadisticas);
        ic_editar = crearItemMenu(menu, 1, getString(R.string.action_bar_editar_cultivo), R.drawable.ic_editar);
        ic_borrar = crearItemMenu(menu, 2, getString(R.string.action_bar_borrar_cultivo), R.drawable.ic_borrar);
        // Cuando se crea el menu creamos la ayuda y la mostramos en caso que se tenga que mostrar
        ShowcaseView.Builder showcaseViewBuilder = Utils.crearShowCaseView(getActivity(),
                this.getClass().getSimpleName(),
                new ViewTarget(((ViewGroup) getActivity().findViewById(R.id.actionbar)).getChildAt(2)),
                getString(R.string.fragmentcultivo_showcaseview_estadisticaseditareliminar_titulo),
                getString(R.string.fragmentcultivo_showcaseview_estadisticaseditareliminar_texto));
        if (showcaseViewBuilder != null) {
            // Desabilitamos los iconos de la action bar
            ic_estadisticas.setEnabled(false);
            ic_editar.setEnabled(false);
            ic_borrar.setEnabled(false);
            showcaseViewBuilder.setOnClickListener(clicksShowCaseView);
            showcaseView = showcaseViewBuilder.build();
            showcaseView.setButtonText(getString(android.R.string.ok));
        }
    }

    /**
     * Método que crea un item en la ActionBAr
     *
     * @param menu        menú de la ActionBar
     * @param posicion    posición del item en la ActionBar
     * @param descripcion descripción del item
     * @param icono       icono que mostraremos
     */
    private MenuItem crearItemMenu(Menu menu, int posicion, String descripcion, int icono) {
        // Creamos el item
        MenuItem menuItem = menu.add(posicion, posicion, posicion, descripcion);
        // Añadimos el icono
        menuItem.setIcon(icono);
        // Mostramos el item como una acción
        menuItem.setShowAsAction(MenuItem.SHOW_AS_ACTION_IF_ROOM | MenuItem.SHOW_AS_ACTION_WITH_TEXT);
        return menuItem;
    }

    /**
     * Metodo que se llamara al seleccionar un item de la action bar.
     *
     * @param item seleccionado de la action bar
     * @return true si se selecciona un item, sino false
     */
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        Bundle bundle = new Bundle();
        switch (item.getItemId()) {
            case 0:
                // Item de estadísticas
                Fragment fragmentEstadisticasCultivo = new FragmentEstadisticasCultivo();
                // Enviaremos al fragment el cultivo seleccionado
                bundle.putString("idCultivo", cultivo.getId_cultivo());
                bundle.putString("nombre", cultivo.getNombre());
                // Añadimos los datos para ser tratados en el fragment
                fragmentEstadisticasCultivo.setArguments(bundle);
                // Cargamos el fragment
                getActivity().getSupportFragmentManager().beginTransaction()
                        .replace(R.id.frame_fragment_principal, fragmentEstadisticasCultivo).commit();
                return true;
            case 1:
                // Item editar
                Intent intent = new Intent(getActivity(), PantallaRegistroCultivo.class);
                bundle.putParcelable("cultivo", cultivo);
                // Añadimos los datos a la Activity
                intent.putExtras(bundle);
                // Iniciamos la pantalla esperando una respuesta
                startActivityForResult(intent, 1);
                return true;
            case 2:
                // Item borrar
                alertaDeEliminacion();
                return true;
        }
        return false;
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent intent) {
        super.onActivityResult(requestCode, resultCode, intent);
        // Si hay respuesta de la Activity es que se a modificado el cultivo
        if (resultCode == Activity.RESULT_OK && requestCode == 1) {
            // Obtenemos el nuevo cultivo
            cultivo = intent.getExtras().getParcelable("cultivo");
            // Actualizamos la informacion del cultivo
            new ActualizarInformacionCultivo().execute();
        }
    }

    @Override
    public void onClick(View view) {
        // Según el boton pulsado de información o luz, riego o cubierta.
        // Si pulsamos L, R o C cambiamos de una imagen o otra, dependiendo del
        // estado en el que este cambiamos de apagado a
        // encendido
        if (view == buttonInfoFuncManual) {
            Utils.mostrarAlertDialog(getActivity(),
                    getString(R.string.fragmentcultivo_info_titulo),
                    getString(R.string.fragmentcultivo_info_funciones_manuales),
                    getString(R.string.alerta_dialog_aceptar), "", false);
        } else if (view == botonLuz) {
            new CambiarEstado().execute("L");
        } else if (view == botonRiego) {
            new CambiarEstado().execute("R");
        } else {
            new CambiarEstado().execute("C");
        }
    }

    /**
     * Método que cambia una imagen de un ImageButton
     *
     * @param boton           el ImageButton que queremos modificar
     * @param estado          el estado en el que esta la imagen
     * @param imagenApagada   la imagen apagada
     * @param imagenEncendida la imagen encendida
     * @return el estado actual de la imagen apagada o encendida
     */
    private boolean cambiarImagen(ImageButton boton, boolean estado, int imagenApagada, int imagenEncendida) {
        if (estado) {
            // Si el estado es true la imagen  es la encendida
            // Configuramos el ImageButton con la imagen apagada
            boton.setImageDrawable(ContextCompat.getDrawable(getActivity(), imagenApagada));
            // Cambiamos el estado a false de luz apagada
            estado = false;
        } else {
            // Si el estado es false
            // Configuramos el ImageButton con la imagen encendida
            boton.setImageDrawable(ContextCompat.getDrawable(getActivity(), imagenEncendida));
            // Cambiamos el estado a true de imagen encendida
            estado = true;
        }
        // Retornamos el estado actual del ImageButton
        return estado;
    }


    /**
     * Metodo para actualizar la informacion del cultivo.
     */
    private class ActualizarInformacionCultivo extends AsyncTask<Void, Void, String> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            // Setteamos el nombre y la descripción en los TextView
            nombreDelCultivo.setText(cultivo.getNombre());
            descripcionDelCultivo.setText(cultivo.getDescripcion());
        }

        @Override
        protected String doInBackground(Void... voids) {
            // Retornamos el numero para saber que funciones están activadas
            return funcionesManualesEnCurso();
        }

        @Override
        protected void onPostExecute(String numeroFuncionAutomaticaEnCurso) {
            super.onPostExecute(numeroFuncionAutomaticaEnCurso);
            if (!numeroFuncionAutomaticaEnCurso.isEmpty() && !numeroFuncionAutomaticaEnCurso.equals("Error al conectar con el socket")) {
                // Cambiamos las imagenes de las acciones en caso que haya funciones automaticas en curso
                if (Integer.parseInt(numeroFuncionAutomaticaEnCurso) == 7) {
                    estadoLuz = cambiarImagen(botonLuz, false, R.drawable.ic_luz_apagada, R.drawable.ic_luz_encendida);
                    estadoRiego = cambiarImagen(botonRiego, false, R.drawable.ic_riego_apagado, R.drawable.ic_riego_encendido);
                    estadoCubierta = cambiarImagen(botonCubierta, false, R.drawable.ic_cubierta_cerrada, R.drawable.ic_cubierta_abierta);
                } else if (Integer.parseInt(numeroFuncionAutomaticaEnCurso) == 6) {
                    estadoRiego = cambiarImagen(botonRiego, false, R.drawable.ic_riego_apagado, R.drawable.ic_riego_encendido);
                    estadoCubierta = cambiarImagen(botonCubierta, false, R.drawable.ic_cubierta_cerrada, R.drawable.ic_cubierta_abierta);
                } else if (Integer.parseInt(numeroFuncionAutomaticaEnCurso) == 5) {
                    estadoLuz = cambiarImagen(botonLuz, false, R.drawable.ic_luz_apagada, R.drawable.ic_luz_encendida);
                    estadoCubierta = cambiarImagen(botonCubierta, false, R.drawable.ic_cubierta_cerrada, R.drawable.ic_cubierta_abierta);
                } else if (Integer.parseInt(numeroFuncionAutomaticaEnCurso) == 4) {
                    estadoCubierta = cambiarImagen(botonCubierta, false, R.drawable.ic_cubierta_cerrada, R.drawable.ic_cubierta_abierta);
                } else if (Integer.parseInt(numeroFuncionAutomaticaEnCurso) == 3) {
                    estadoLuz = cambiarImagen(botonLuz, false, R.drawable.ic_luz_apagada, R.drawable.ic_luz_encendida);
                    estadoRiego = cambiarImagen(botonRiego, false, R.drawable.ic_riego_apagado, R.drawable.ic_riego_encendido);
                } else if (Integer.parseInt(numeroFuncionAutomaticaEnCurso) == 2) {
                    estadoRiego = cambiarImagen(botonRiego, false, R.drawable.ic_riego_apagado, R.drawable.ic_riego_encendido);
                } else if (Integer.parseInt(numeroFuncionAutomaticaEnCurso) == 1) {
                    estadoLuz = cambiarImagen(botonLuz, false, R.drawable.ic_luz_apagada, R.drawable.ic_luz_encendida);
                }
            } else {
                // Si no se ha podido obtener las funciones activadas mostramos un mensaje
                final Snackbar snackbar = Snackbar.make(getActivity().findViewById(android.R.id.content),
                        getString(R.string.fragmentcultivo_snackbar_nohayconexion_servidor), Snackbar.LENGTH_LONG);
                // Le daremos la opcion de reintentarlo
                snackbar.setAction(getString(R.string.fragmentcultivo_snackbar_reintentar), new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        snackbar.dismiss();
                        // Intentamos otra vez la accion
                        new ActualizarInformacionCultivo().execute();
                    }
                });

                Utils.snackbarTextoVerde(getActivity(), snackbar);
                snackbar.show();
            }
        }
    }

    /**
     * Hilo que usaremos para eliminar el cultivo.
     */
    private class CambiarEstado extends AsyncTask<String, Void, String> {
        // Guardamos el tipo de accion L, R o C
        private String tipoAccion;

        @Override
        protected String doInBackground(String... accion) {
            // Obtenemos el tipo de accion ejecutada
            tipoAccion = accion[0];
            // Comprobamos que la funcion no este habilitada para poder ejecutar la acción
            if (tipoAccion.equals("L")) {
                if (cultivo.getFuncionLuz() != null && cultivo.getFuncionLuz().equals("S")) {
                    deshabilitarHabilitarFuncionAutomatica();
                }
            } else if (tipoAccion.equals("R")) {
                if (cultivo.getFuncionRiego() != null && cultivo.getFuncionRiego().equals("S")) {
                    deshabilitarHabilitarFuncionAutomatica();
                }
            } else if (tipoAccion.equals("C")) {
                if (cultivo.getFuncionCubierta() != null && cultivo.getFuncionCubierta().equals("S")) {
                    deshabilitarHabilitarFuncionAutomatica();
                }
            }
            // Actualizamos los datos en el servidor y ejecutamos la accion
            if (deshabilitarHabilitarFuncionAutomatica() != null && ejecutarAccionCultivo() != null) {
                return "CORRECTO";
            }

            return null;
        }

        @Override
        protected void onPostExecute(String resultado) {
            super.onPostExecute(resultado);
            if (resultado != null && resultado.equals("CORRECTO")) {
                // Cambiamos la imagen dependiendo del tipo de accion
                if (tipoAccion.equals("L")) {
                    estadoLuz = cambiarImagen(botonLuz, estadoLuz, R.drawable.ic_luz_apagada, R.drawable.ic_luz_encendida);
                } else if (tipoAccion.equals("R")) {
                    estadoRiego = cambiarImagen(botonRiego, estadoRiego, R.drawable.ic_riego_apagado, R.drawable.ic_riego_encendido);
                } else if (tipoAccion.equals("C")) {
                    estadoCubierta = cambiarImagen(botonCubierta, estadoCubierta, R.drawable.ic_cubierta_cerrada, R.drawable.ic_cubierta_abierta);
                }
            } else {
                // Si no se ha ejecutado bien la accion mostramos un mensaje
                final Snackbar snackbar = Snackbar.make(getActivity().findViewById(android.R.id.content),
                        getString(R.string.fragmentcultivo_snackbar_noaccion), Snackbar.LENGTH_LONG);
                // Le daremos la opcion de reintentarlo
                snackbar.setAction(getString(R.string.fragmentcultivo_snackbar_reintentar), new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        snackbar.dismiss();
                        // Intentamos otra vez la accion
                        new CambiarEstado().execute(tipoAccion);
                    }
                });

                Utils.snackbarTextoVerde(getActivity(), snackbar);
                snackbar.show();
            }
        }

        // Metodo que usaremos para habilitar o deshabilitar las funciones automáticas en caso
        // de que estén habilitadas
        private String deshabilitarHabilitarFuncionAutomatica() {
            // Mensaje para habilitar o deshabilitar la funcion automatica
            String deshabilitarHabilitarFuncionesAutomaticas = "";
            // Ejecutamos la accion dependiendo del tipo
            if (tipoAccion.equals("L")) {
                if (!cultivo.getHoraInicioLuz().isEmpty() && !cultivo.getHoraFinLuz().isEmpty()) {
                    deshabilitarHabilitarFuncionesAutomaticas += programarDeshabilitarFuncionAutomatica
                            (cultivo.getHoraInicioLuz(), cultivo.getHoraFinLuz());
                }
            } else if (tipoAccion.equals("R")) {
                if (!cultivo.getHoraInicioRiego().isEmpty() && !cultivo.getHoraFinRiego().isEmpty()) {
                    deshabilitarHabilitarFuncionesAutomaticas += programarDeshabilitarFuncionAutomatica
                            (cultivo.getHoraInicioRiego(), cultivo.getHoraFinRiego());
                }
            } else if (tipoAccion.equals("C")) {
                if (!cultivo.getHoraInicioCubierta().isEmpty() && !cultivo.getHoraFinCubierta().isEmpty()) {
                    deshabilitarHabilitarFuncionesAutomaticas += programarDeshabilitarFuncionAutomatica
                            (cultivo.getHoraInicioCubierta(), cultivo.getHoraFinCubierta());
                }
            }
            if (!deshabilitarHabilitarFuncionesAutomaticas.isEmpty()) {
                //Creamos una instancia de ObtenerDatosJSON
                ObtenerDatosJSON datosJSON = new ObtenerDatosJSON();
                // Creamos los parametros para la funcion
                List<NameValuePair> parametros = new ArrayList<>();
                // En caso que tenga habilitada la función automática enviaremos como parametro la deshabilitacion o
                // habilitacion de la función automática
                parametros.add(new BasicNameValuePair("accionCultivo", deshabilitarHabilitarFuncionesAutomaticas));
                // Hacemos la solicitud de obtener el documento JSON, en este caso un POST
                return datosJSON.obtenerDocumentoJSON
                        (Utils.URL_ACCION_CULTIVO, ObtenerDatosJSON.POST, parametros);
            }
            return "NO SE HACE NADA";
        }

        /**
         * Metodo que usaremos para ejecutar la accion del cultivo.
         *
         * @return distinto de null si va correctamente
         */
        private String ejecutarAccionCultivo() {
            //Creamos una instancia de ObtenerDatosJSON
            ObtenerDatosJSON datosJSON = new ObtenerDatosJSON();
            // Creamos los parametros para la funcion
            List<NameValuePair> parametros = new ArrayList<>();
            // La accion que ejecutara el cultivo
            String ejecutarAccion = "";
            // Ejecutamos la accion dependiendo del tipo
            if (tipoAccion.equals("L")) {
                ejecutarAccion = estadoLuz ? "A4" : "E4";
            } else if (tipoAccion.equals("R")) {
                ejecutarAccion = estadoRiego ? "A5" : "E5";
            } else if (tipoAccion.equals("C")) {
                ejecutarAccion = estadoCubierta ? "A6" : "E6";
            }
            parametros.add(new BasicNameValuePair("accionCultivo", ejecutarAccion));
            // Hacemos la solicitud de obtener el documento JSON, en este caso un POST
            return datosJSON.obtenerDocumentoJSON
                    (Utils.URL_ACCION_CULTIVO, ObtenerDatosJSON.POST, parametros);
        }

        // Metodo que usaremos para deshabilitar la funcion automatica en caso que este activada
        private String programarDeshabilitarFuncionAutomatica(String horaInicio,
                                                              String horaFin) {
            // Iniciamos el resultado
            String resultado = "";
            // Obtenemos la hora de inicio y hora de fin bien formarteadas
            horaInicio = horaInicio.replace(":", "");
            horaFin = horaFin.replace(":", "");
            // Dependiendo del tipo de accion la habilito o no
            if (tipoAccion.equals("L")) {
                resultado = estadoLuz ? "P41" + horaInicio + horaFin : "P40" + horaInicio + horaFin;
            } else if (tipoAccion.equals("R")) {
                resultado = estadoRiego ? "P51" + horaInicio + horaFin : "P50" + horaInicio + horaFin;
            } else if (tipoAccion.equals("C")) {
                resultado = estadoCubierta ? "P61" + horaInicio + horaFin : "P60" + horaInicio + horaFin;
            }
            // Retornamos el resultado
            return resultado;
        }


    }

    /**
     * Metodo que usaremos para comprobar si una funcion automatica esta en curso.
     *
     * @return un numero dependiendo de las que esten activadas
     */
    private String funcionesManualesEnCurso() {
        //Creamos una instancia de ObtenerDatosJSON
        ObtenerDatosJSON datosJSON = new ObtenerDatosJSON();
        // Creamos los parametros para la funcion
        List<NameValuePair> parametros = new ArrayList<>();
        parametros.add(new BasicNameValuePair("accionCultivo", "X"));
        // Hacemos la solicitud de obtener el documento JSON, en este caso un POST
        return datosJSON.obtenerDocumentoJSON
                (Utils.URL_ACCION_CULTIVO, ObtenerDatosJSON.POST, parametros).trim();
    }

    /**
     * Hilo que usaremos para eliminar el cultivo.
     */
    private class EliminarCultivo extends AsyncTask<Void, Void, String> {

        @Override
        protected String doInBackground(Void... arg0) {
            //Creamos una instancia de ObtenerDatosJSON
            ObtenerDatosJSON datosJSON = new ObtenerDatosJSON();
            // Creamos los parametros para la funcion
            List<NameValuePair> parametros = new ArrayList<>();
            // El id del cultivo
            parametros.add(new BasicNameValuePair("idCultivo", cultivo.getId_cultivo()));
            // Hacemos la solicitud de obtener el documento JSON, en este caso un POST
            return datosJSON.obtenerDocumentoJSON
                    (Utils.URL_ACTUALIZAR_CULTIVO, ObtenerDatosJSON.POST, parametros);
        }

        @Override
        protected void onPostExecute(String resultado) {
            super.onPostExecute(resultado);
            if (resultado != null) {
                // Cargamos el fragment cultivos registrados
                Fragment fragmentCultivosRegistrados = new FragmentCultivosRegistrados();
                // Cargamos el fragment
                getActivity().getSupportFragmentManager().beginTransaction()
                        .replace(R.id.frame_fragment_principal, fragmentCultivosRegistrados).commit();
            } else {
                // Si no se ha ejecutado bien la eliminacion mostramos un mensaje
                final Snackbar snackbar = Snackbar.make(getActivity().findViewById(android.R.id.content),
                        getString(R.string.fragmentcultivo_snackbar_nohayconexion), Snackbar.LENGTH_LONG);
                // Le daremos la opcion de reintentarlo
                snackbar.setAction(getString(R.string.fragmentcultivo_snackbar_reintentar), new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        snackbar.dismiss();
                        // Intentamos otra vez eliminar el cultivo
                        new EliminarCultivo().execute();
                    }
                });

                Utils.snackbarTextoVerde(getActivity(), snackbar);
                snackbar.show();
            }
        }
    }

    /**
     * Método que alerta de que se eliminara el cultivo
     */
    private void alertaDeEliminacion() {
        // Creamos el AlertDialog
        AlertDialog.Builder alertDialog = new AlertDialog.Builder(getActivity());
        // Añadimos el mensaje ha mostrar
        alertDialog.setMessage(getString(R.string.pregunta_eliminar_fragment_cultivo) + " '" + cultivo.getNombre() + "'?");
        // Añadimos un boton afirmativo
        alertDialog.setPositiveButton(getString(R.string.si_fragment_cultivo), new AlertDialog.OnClickListener() {
            public void onClick(DialogInterface dlg, int i) {
                dlg.cancel();
                // Eliminamos el cultivo si la respuesta del usuario es afirmativa
                new EliminarCultivo().execute();
            }
        });
        // Añadimos un boton negativo
        alertDialog.setNegativeButton(getString(R.string.no_fragment_cultivo), new AlertDialog.OnClickListener() {
            public void onClick(DialogInterface dlg, int i) {
                // No hacemos nada si la respuesta del usuario es negativa
                dlg.cancel();
            }
        });
        alertDialog.show();
    }


    /**
     * Escuchador de los clicks del showcaseview.
     */
    View.OnClickListener clicksShowCaseView = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            // Ocultamos el showcaseview
            showcaseView.hide();
            // Habilitamos los iconos de la action bar
            ic_estadisticas.setEnabled(true);
            ic_editar.setEnabled(true);
            ic_borrar.setEnabled(true);
        }
    };
}