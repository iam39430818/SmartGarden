package org.escoladeltreball.smartgarden.fragments;


import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import com.github.mikephil.charting.animation.Easing;
import com.github.mikephil.charting.charts.PieChart;
import com.github.mikephil.charting.components.Legend;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.data.PieData;
import com.github.mikephil.charting.data.PieDataSet;
import com.github.mikephil.charting.utils.ColorTemplate;
import com.github.mikephil.charting.utils.LargeValueFormatter;

import org.escoladeltreball.smartgarden.R;
import org.escoladeltreball.smartgarden.adapters.AdaptadorListviewRegistrosCultivo;
import org.escoladeltreball.smartgarden.pojo.Registro;
import org.escoladeltreball.smartgarden.utils.Utils;

import java.util.ArrayList;

/**
 * FragmentTabHostRegistrosCultivoHumedad que se cargará en el FragmentEstadisticaCultivo.
 *
 * @author Carlos DaPalma Corchado <cdapalmac@gmail.com>
 * @author Esdras Valiente Lanaspa <esdras.vl85@gmail.com>
 * @author Andres Jordan Zambrano <andresjordanz@hotmail.com>
 *         <p/>
 *         This is free software, licensed under the GNU General Public License v3.
 *         See http://www.gnu.org/licenses/gpl.html for more information.
 */
public class FragmentTabHostRegistrosCultivoHumedad extends Fragment {
    // View del layout actual
    private View viewFragmentTabHostRegistrosCultivoHumedad;
    // Lista de todas los consumos del cultivo
    private ArrayList<Registro> listaRegistrosCultivoHumedad;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Recuperamos la lista de los registros
        listaRegistrosCultivoHumedad = getArguments().getParcelableArrayList("listaRegistrosEstadisticasCultivo");
        // Inflamos el layout en este fragment
        viewFragmentTabHostRegistrosCultivoHumedad = inflater.inflate(R.layout.fragment_tab_host_estadisticas_cultivo_humedad, container, false);
        // Cargamos la lista de registros
        ListView listViewTabHostEstadisticasCultivoHumedad = (ListView) viewFragmentTabHostRegistrosCultivoHumedad.findViewById(R.id.listViewTabHostEstadisticasCultivoHumedad);
        // Recuperamos el linearLayoutErrorRegistrosCultivoHumedad
        LinearLayout linearLayoutErrorRegistrosCultivoHumedad = (LinearLayout) viewFragmentTabHostRegistrosCultivoHumedad.findViewById(R.id.linearLayoutErrorRegistrosCultivoHumedad);
        // Ponemos la imageview de no item en escala de grises
        ImageView imagen = (ImageView) viewFragmentTabHostRegistrosCultivoHumedad.findViewById(R.id.imgViewErrorRegistrosCultivoHumedad);
        // Coloreamos la imagen de gris
        Utils.imgViewPintarImagen(R.color.color_gris_claro, imagen);
        // Inflamos el text view
        TextView tvErrorRegistrosCultivoHumedad = (TextView) viewFragmentTabHostRegistrosCultivoHumedad.findViewById(R.id.tvErrorRegistrosCultivoHumedad);
        // En caso que no haya items se mostrara un mensaje indicandolo
        listViewTabHostEstadisticasCultivoHumedad.setEmptyView(linearLayoutErrorRegistrosCultivoHumedad);
        // Comprobamos que haya items y los mostramos
        if (listaRegistrosCultivoHumedad != null) {
            // Creamos el adaptador
            AdaptadorListviewRegistrosCultivo adaptadorListviewRegistrosCultivoHumedad = new AdaptadorListviewRegistrosCultivo(getContext(), listaRegistrosCultivoHumedad, "H");
            // Asignamos el adaptador a la listview
            listViewTabHostEstadisticasCultivoHumedad.setAdapter(adaptadorListviewRegistrosCultivoHumedad);
            // Cargamos los graficos
            cargarGraficoHumedad();
        } else if (!Utils.tieneConexion(getActivity())) {
            // Indicamos un mensaje de que no dispone de conexion
            tvErrorRegistrosCultivoHumedad.setText(getString(R.string.fragmenttabhostestadisticascultivohumedad_tvnoitemslistviewcultivos_noconexion));
        } else {
            // Indicamos un mensaje de que no se han encontrado cultivos
            tvErrorRegistrosCultivoHumedad.setText(getString(R.string.fragmenttabhostestadisticascultivohumedad_tvnoitemslistviewcultivos_noencontrado));
        }
        // Retornamos la vista inflada
        return viewFragmentTabHostRegistrosCultivoHumedad;
    }

    /**
     * Metodo que usaremos para cargar los graficos.
     */
    private void cargarGraficoHumedad() {
        // Obtenemos el grafico de humedad del layout
        PieChart graficoHumedad = (PieChart) viewFragmentTabHostRegistrosCultivoHumedad.findViewById(R.id.estadisticasCultivoGraficoHumedad);
        // No lo mostramos los valores en %
        graficoHumedad.setUsePercentValues(false);
        // No le asignamos ningun titulo
        graficoHumedad.setDescription("");
        // Pintamos el titulo en el centro del grafico
        graficoHumedad.setDrawCenterText(true);
        // Asignamos el texto del centro del grafico
        graficoHumedad.setCenterText(getString(R.string.fragmenttabhostestadisticascultivohumedad_titulo_grafico));
        // Habilitamos que se pueda rotar
        graficoHumedad.setRotationEnabled(true);
        // Ocultamos el texto de los valores del grafico
        graficoHumedad.setDrawSliceText(false);
        // Deshacer destacados
        graficoHumedad.highlightValues(null);
        graficoHumedad.invalidate();
        graficoHumedad.animateY(1500, Easing.EasingOption.EaseInOutQuad);
        graficoHumedad.setDrawHoleEnabled(true);
        graficoHumedad.setHoleColorTransparent(true);
        graficoHumedad.setTransparentCircleColor(Color.WHITE);
        graficoHumedad.setDragDecelerationFrictionCoef(0.95f);
        graficoHumedad.setHoleRadius(45f);
        graficoHumedad.setTransparentCircleRadius(48f);
        graficoHumedad.setRotationAngle(0);

        // Creamos la lista de los datos de los graficos
        ArrayList<String> listaMeses = new ArrayList<>();
        ArrayList<Entry> listaEntryMeses = new ArrayList<>();
        // Los valores de numero de meses los asignamos como integer
        float numeroDeMeses;
        // Obtenemos la lista de meses con el numero de meses
        for (int i = 0; i < listaRegistrosCultivoHumedad.size(); i++) {
            listaMeses.add(listaRegistrosCultivoHumedad.get(i).getMes());
            numeroDeMeses = Float.valueOf("" + Float.parseFloat(listaRegistrosCultivoHumedad.get(i).getHumedad()));
            listaEntryMeses.add(new Entry(numeroDeMeses, -1));
        }

        // Añadimos los colores
        ArrayList<Integer> colors = new ArrayList<>();
        for (int c : ColorTemplate.VORDIPLOM_COLORS)
            colors.add(c);
        for (int c : ColorTemplate.PASTEL_COLORS)
            colors.add(c);
        for (int c : ColorTemplate.JOYFUL_COLORS)
            colors.add(c);
        for (int c : ColorTemplate.COLORFUL_COLORS)
            colors.add(c);
        for (int c : ColorTemplate.LIBERTY_COLORS)
            colors.add(c);
        colors.add(ColorTemplate.getHoloBlue());

        // Creamos la leyenda del grafico de humedad
        Legend lPie1 = graficoHumedad.getLegend();
        lPie1.setPosition(Legend.LegendPosition.LEFT_OF_CHART_CENTER);
        lPie1.setXEntrySpace(7f);
        // Asignamos el espacio entre lineas
        lPie1.setYEntrySpace(5f);
        lPie1.setYOffset(0f);
        // Asignamos el tamaño de la letra
        lPie1.setTextSize(16f);
        // Creamos el valor del numero de meses
        PieDataSet valorGraficoNMeses = new PieDataSet(listaEntryMeses, "");
        valorGraficoNMeses.setSliceSpace(3f);
        valorGraficoNMeses.setSelectionShift(5f);
        valorGraficoNMeses.setColors(colors);
        // Creamos los valores del grafico de humedad
        PieData valoresGraficosNMeses = new PieData(listaMeses, valorGraficoNMeses);
        valoresGraficosNMeses.setValueFormatter(new LargeValueFormatter());
        valoresGraficosNMeses.setValueTextSize(16f);
        valoresGraficosNMeses.setValueTextColor(Color.BLACK);
        // Asignamos los valores al grafico de humedad
        graficoHumedad.setData(valoresGraficosNMeses);
        // Mostramos el grafico el humedad
        graficoHumedad.setVisibility(View.VISIBLE);

    }

}


