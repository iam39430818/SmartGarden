package org.escoladeltreball.smartgarden.fragments;


import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import com.github.amlcurran.showcaseview.ShowcaseView;
import com.github.amlcurran.showcaseview.targets.ViewTarget;

import org.escoladeltreball.smartgarden.R;
import org.escoladeltreball.smartgarden.activities.PantallaRegistroCultivo;
import org.escoladeltreball.smartgarden.adapters.AdaptadorListViewCultivosDisponibles;
import org.escoladeltreball.smartgarden.json.JSONPullParserCultivos;
import org.escoladeltreball.smartgarden.pojo.Cultivo;
import org.escoladeltreball.smartgarden.utils.ObtenerDatosJSON;
import org.escoladeltreball.smartgarden.utils.Utils;
import org.escoladeltreball.smartgarden.utils.VariablesGlobales;

import java.util.ArrayList;

/**
 * FragmentBienvenida que se cargara en el AdaptadorViewPagerFragmentPantallaBienvenida.
 *
 * @author Carlos DaPalma Corchado <cdapalmac@gmail.com>
 * @author Esdras Valiente Lanaspa <esdras.vl85@gmail.com>
 * @author Andres Jordan Zambrano <andresjordanz@hotmail.com>
 *         <p/>
 *         This is free software, licensed under the GNU General Public License v3.
 *         See http://www.gnu.org/licenses/gpl.html for more information.
 */
public class FragmentBusquedaCultivos extends Fragment implements AdapterView.OnItemClickListener, SwipeRefreshLayout.OnRefreshListener {

    // ArrayList de los cultivos disponibles encontrados
    private ArrayList<Cultivo> listaCultivos;
    // ListView que mostrar los cultivos disponibles
    private ListView listViewCultivosDisponibles;
    // Adaptador para listview de los cultivos disponibles
    private AdaptadorListViewCultivosDisponibles adaptadorListViewCultivosDisponibles;
    // Boolean para mostrar el mensaje de ayuda para buscar dispositivos
    private boolean mostrarAyudaRefrescar = true;
    // El contenedor de refrescar deslizando hacia abajo
    private SwipeRefreshLayout contenedorRefresco;
    // El text view que nos indicara el progreso
    private TextView tvMensajeListViewCultivosDisponibles;
    // La ayuda que se mostrará al entrar en este fragment
    private ShowcaseView ayudaBuscarCultivos;
    // Id del cultivo que se irá a registrar
    private int positionItemPulsadoLista;
    // Item del menu de la action bar
    private MenuItem ic_refrescar;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Asigno el titulo a la action bar
        getActivity().setTitle(getString(R.string.fragmentbusquedacultivos_titulo));
        // Mostramos los items de la action bar
        setHasOptionsMenu(true);
        // Inflamos el layout de este fragment
        View viewFragmentBusquedaCultivos = inflater.inflate(R.layout.fragment_busqueda_cultivos, container, false);
        // Recuperamos el contenedor de refresco
        contenedorRefresco = (SwipeRefreshLayout) viewFragmentBusquedaCultivos.findViewById(R.id.contenedorSwipeRefresh);
        // Añadimos el escuchador de refresco
        contenedorRefresco.setOnRefreshListener(this);
        // Configuramos los colores del contendor de refresco
        configurarColoresContenedorRefresco();
        // Recuperamos el imageview del logo de la aplicacion
        ImageView imageViewLogo = (ImageView) viewFragmentBusquedaCultivos.findViewById(R.id.imgViewNoItemsBusquedaCultivos);
        // Lo ponemos el escala de grises
        Utils.imgViewEscalaGrises(imageViewLogo);
        // Recuperamos el tvMensajeListViewCultivosDisponibles
        tvMensajeListViewCultivosDisponibles = (TextView) viewFragmentBusquedaCultivos.findViewById(R.id.tvMensajeListViewBusquedaCultivos);
        // Recuperamos el linearLayoutNoItems
        LinearLayout linearLayoutNoItems = (LinearLayout) viewFragmentBusquedaCultivos.findViewById(R.id.linearLayoutNoItemsBusquedaCultivos);
        // Lista que mostrara los cultivos disponibles
        listViewCultivosDisponibles = (ListView) viewFragmentBusquedaCultivos.findViewById(R.id.listViewBusquedaCultivos);
        // En caso que no haya items se mostrara un mensaje indicandolo
        listViewCultivosDisponibles.setEmptyView(linearLayoutNoItems);
        // Añadimos el escuchador de clicks en los items
        listViewCultivosDisponibles.setOnItemClickListener(this);
        // Añadimos un escuchador de scroll para coordinal el contenedor de refresco y la listview
        listViewCultivosDisponibles.setOnScrollListener(scrollListView);
        // Iniciamos el hilo para obtener los cultivos
        if (!Utils.comprobarPrimerRegistroDeCultivo(getActivity())) {
            // Ejecutamos el hilo para obtener los cultivos disponibles
            new ObtenerCultivos().execute();
        }
        // Retornamos la vista inflada
        return viewFragmentBusquedaCultivos;
    }

    /**
     * Este metodo sirve para configurar los colores del contenedor de refresco
     */
    private void configurarColoresContenedorRefresco() {
        contenedorRefresco.setColorSchemeResources(
                R.color.color_actionbar,
                R.color.color_statusbar);
    }


    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        CrearMenu(menu);
    }

    private void CrearMenu(Menu menu) {
        // Mostramos el ic_refrescar
        ic_refrescar = menu.add(0, 0, 0, getString(R.string.action_bar_refrescar));
        ic_refrescar.setIcon(R.drawable.ic_refrescar);
        ic_refrescar.setShowAsAction(
                MenuItem.SHOW_AS_ACTION_IF_ROOM |
                        MenuItem.SHOW_AS_ACTION_WITH_TEXT);

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        return SeleccionOpcionMenu(item);
    }

    private boolean SeleccionOpcionMenu(MenuItem item) {
        switch (item.getItemId()) {
            case 0:
                if (ayudaBuscarCultivos != null && ayudaBuscarCultivos.isShowing()) {
                    ayudaBuscarCultivos.hide();
                }
                if (listaCultivos != null) {
                    // En caso que seleccion el ic_refrescar se resfresca la lista de los dispositivos
                    refrescarCultivosDisponibles();
                }
                // Ejecutamos el hilo para obtener los cultivos disponibles
                new ObtenerCultivos().execute();
                return true;
        }
        return false;
    }


    /**
     * Este metodo sirve para refrescar la lista de cultivos disponibles
     */
    private void refrescarCultivosDisponibles() {
        // Limpiamos la lista anterior antes de volver a buscar
        listaCultivos.clear();
        adaptadorListViewCultivosDisponibles.notifyDataSetChanged();

    }


    /**
     * Este metodo sirve para cargar/mostrar datos cuando el usuario esta viendo el fragment
     *
     * @param fragmentVisible es un boolean que nos indica si esta el usuario viendo el fragment
     */
    @Override
    public void setUserVisibleHint(boolean fragmentVisible) {
        super.setUserVisibleHint(fragmentVisible);
        // Si es la primera vez que abre el fragment se le mostrara el mensaje, sino no
        if (fragmentVisible && mostrarAyudaRefrescar) {
            ayudaBuscarCultivos = new ShowcaseView.Builder(getActivity())
                    // Obtenemos de la action bar el item que indicaremos
                    .setTarget(new ViewTarget(((ViewGroup) getActivity().findViewById(R.id.actionbar)).getChildAt(1)))
                            // Le asigamos el estilo
                    .setStyle(R.style.CustomShowcaseTheme)
                            // Le asignamos un titulo
                    .setContentTitle(R.string.fragmentbusquedacultivos_tituloshowcaseview)
                            // Le asignamos el mensaje de contenido
                    .setContentText(R.string.fragmentbusquedacultivos_contenidoshowcaseview)
                            // Al pulsar se oculte
                    .hideOnTouchOutside()
                            // Lo creamos
                    .build();
            mostrarAyudaRefrescar = false;
        }
        if (fragmentVisible) {
            // Ejecutamos el hilo para obtener los cultivos disponibles
            new ObtenerCultivos().execute();
        }
    }


    /**
     * Este metodo nos servira para parar la busqueda del receptor cuando cambie de Fragment o
     * Activity.
     */
    @Override
    public void onPause() {
        super.onPause();
        if (contenedorRefresco != null) {
            // Ocultamos el contenedor de refresco
            contenedorRefresco.setRefreshing(false);
            contenedorRefresco.destroyDrawingCache();
            contenedorRefresco.clearAnimation();
        }
    }

    /**
     * Este metodo escuchara los clicks de cada item de la listview.
     *
     * @param parent   el adaptador
     * @param view     la vista que se ha pulsado
     * @param position la posicion de la vista en la listview
     * @param id       del item
     */
    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        // Obtenemos la MAC del dispositivo que se registrará
        positionItemPulsadoLista = position;
        // Cargamos la pantalla registro cultivo pasandole el nombre y la mac
        Intent intent = new Intent(getActivity(), PantallaRegistroCultivo.class);
        Bundle bundle = new Bundle();
        bundle.putParcelable("cultivo", listaCultivos.get(position));
        intent.putExtras(bundle);
        // Recibiremos una respuesta de ese registro para saber si hay que borrarlo de la lista
        startActivityForResult(intent, 1);
    }

    /**
     * Metodo que nos servira para recibir informacion del intent que se muestra al pulsar un item
     * de la lista.
     *
     * @param requestCode el código de solicitud que permite identificar que este resultado se devuelve.
     * @param resultCode  el código de resultado devuelto por la actividad a través de setResult ().
     * @param data        los datos del intent
     */
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 1) {
            if (resultCode == Activity.RESULT_OK) {
                // Eliminamos el cultivo de la lista para que no lo pueda volver a registrar
                listaCultivos.remove(positionItemPulsadoLista);
                // Notificamos al adaptador del cambio en la lista
                adaptadorListViewCultivosDisponibles.notifyDataSetChanged();
                // Comprobamos si hay que mostrar el image button de siguiente
                if (((VariablesGlobales) getActivity().getApplication()).getImgBtnSiguiente() != null) {
                    ((VariablesGlobales) getActivity().getApplication()).setMostrarImgBtnSiguiente(true);
                    ((VariablesGlobales) getActivity().getApplication()).getImgBtnSiguiente().setVisibility(View.VISIBLE);
                }
            }
        }
    }

    /**
     * Metodo que nos servira para refrescar la lista deslizando el dedo hacia abajo
     */
    @Override
    public void onRefresh() {
        // No dejamos que pueda refrescar desde el icono de la action bar
        ic_refrescar.setEnabled(false);
        // No dejamos pulsar los items de la listview
        listViewCultivosDisponibles.setEnabled(false);
        // Refrescamos la lista de dispositivos
        if (listaCultivos != null) {
            refrescarCultivosDisponibles();
        }
        // Mostramos un mensaje indicando que se estan buscando dispositivos
        tvMensajeListViewCultivosDisponibles.setText(getString(R.string.fragmenttbusquedacultivos_tvnoitemslistviewdispositivos_buscando));
        // Ejecutamos el hilo para obtener los cultivos disponibles
        new ObtenerCultivos().execute();
    }

    /**
     * Scroll que usaremos para la list view
     */
    AbsListView.OnScrollListener scrollListView = new AbsListView.OnScrollListener() {
        @Override
        public void onScrollStateChanged(AbsListView view, int scrollState) {

        }

        @Override
        public void onScroll(AbsListView view, int primerItemVisible, int visibleItemCount, int totalItemCount) {
            int filaSuperiorPosicionVertical = (listViewCultivosDisponibles == null || listViewCultivosDisponibles.getChildCount() == 0) ? 0 : listViewCultivosDisponibles.getChildAt(0).getTop();
            contenedorRefresco.setEnabled(primerItemVisible == 0 && filaSuperiorPosicionVertical >= 0);
        }
    };

    /**
     * Hilo que usaremos para conectar al servidor y obtener la lista de los cultivos disponibles
     */
    private class ObtenerCultivos extends AsyncTask<Void, Void, Void> {
        // Dialogo que se mostrara cuando se inicie la busqueda de los cultivos disponibles
        private ProgressDialog progressDialogBuscarCultivos;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            // Comprobamos si el contenedor de refresco esta buscando
            if ((contenedorRefresco != null && !contenedorRefresco.isRefreshing())
                    && (ayudaBuscarCultivos != null && !ayudaBuscarCultivos.isShowing())) {
                // Mostramos el progress dialog
                progressDialogBuscarCultivos = new ProgressDialog(getActivity());
                progressDialogBuscarCultivos.setTitle(getString(R.string.fragmentbusquedacultivos_progressdialogdispositivos_titulo));
                progressDialogBuscarCultivos.setMessage(getString(R.string.fragmentbusquedacultivos_progressdialogdispositivos_mensaje));
                progressDialogBuscarCultivos.setProgressStyle(ProgressDialog.STYLE_SPINNER);
                progressDialogBuscarCultivos.setIndeterminate(true);
                progressDialogBuscarCultivos.setCancelable(false);
                progressDialogBuscarCultivos.show();
            }
        }

        @Override
        protected Void doInBackground(Void... arg0) {
            // Creamos una instancia de ObtenerDatosJSON
            ObtenerDatosJSON datosJSON = new ObtenerDatosJSON();
            // Hacemos la solicitud de obtener el documento JSON, en este caso un GET
            String documentoJSON = datosJSON.obtenerDocumentoJSON(Utils.URL_CULTIVOS, ObtenerDatosJSON.GET);
            // Parseamos el documento obtenido
            JSONPullParserCultivos jsonPullParser = new JSONPullParserCultivos(documentoJSON);
            listaCultivos = jsonPullParser.parse();
            return null;
        }

        @Override
        protected void onPostExecute(Void result) {
            super.onPostExecute(result);
            // Cuando acabe ocultamos la progress dialog
            if (progressDialogBuscarCultivos != null) {
                progressDialogBuscarCultivos.dismiss();
            }
            // Comprobamos que haya items y los mostramos
            if (listaCultivos != null) {
                // Creamos el adaptador
                adaptadorListViewCultivosDisponibles = new AdaptadorListViewCultivosDisponibles(getContext(), listaCultivos);
                // Asignamos el adaptador a la listview
                listViewCultivosDisponibles.setAdapter(adaptadorListViewCultivosDisponibles);
            } else if (!Utils.tieneConexion(getActivity())) {
                // Indicamos un mensaje de que no dispone de conexion
                tvMensajeListViewCultivosDisponibles.setText(getString(R.string.fragmenttbusquedacultivos_tvnoitemslistviewdispositivos_noconexion));
            } else {
                // Indicamos un mensaje de que no se han encontrado cultivos
                tvMensajeListViewCultivosDisponibles.setText(getString(R.string.fragmenttbusquedacultivos_tvnoitemslistviewdispositivos_noencontrado));
            }
            // En caso que este el contenedor de refresco activo lo ocultamos
            if (contenedorRefresco != null) {
                // Ocultamos el contenedor de refresco
                contenedorRefresco.setRefreshing(false);
            }
            // Dejamos pulsar los items de la listview
            listViewCultivosDisponibles.setEnabled(true);
            // Dejamos que pueda refrescar desde el icono de la action bar
            ic_refrescar.setEnabled(true);
        }
    }
}