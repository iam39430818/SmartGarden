package org.escoladeltreball.smartgarden.fragments;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import org.escoladeltreball.smartgarden.R;

/**
 * FragmentBienvenida que se cargara en el AdaptadorViewPagerFragmentPantallaBienvenida.
 *
 *
 * @author Carlos DaPalma Corchado <cdapalmac@gmail.com>
 * @author Esdras Valiente Lanaspa <esdras.vl85@gmail.com>
 * @author Andres Jordan Zambrano <andresjordanz@hotmail.com>
 *
 * This is free software, licensed under the GNU General Public License v3.
 * See http://www.gnu.org/licenses/gpl.html for more information.
 */
public class FragmentBienvenida extends Fragment {

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflamos y retornamos el layout del FragmentBienvenida
        return inflater.inflate(R.layout.fragment_bienvenida, container, false);
    }
}
