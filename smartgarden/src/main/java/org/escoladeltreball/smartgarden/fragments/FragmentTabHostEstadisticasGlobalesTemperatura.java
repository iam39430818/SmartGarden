package org.escoladeltreball.smartgarden.fragments;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import org.escoladeltreball.smartgarden.R;
import org.escoladeltreball.smartgarden.adapters.AdaptadorListViewCultivosGlobales;
import org.escoladeltreball.smartgarden.pojo.Registro;
import org.escoladeltreball.smartgarden.utils.Utils;

import java.util.ArrayList;

/**
 * FragmentTabHostEstadisticasGlobalesTemperatura que se cargará en el FragmentEstadisticasGlobales.
 *
 * @author Carlos DaPalma Corchado <cdapalmac@gmail.com>
 * @author Esdras Valiente Lanaspa <esdras.vl85@gmail.com>
 * @author Andres Jordan Zambrano <andresjordanz@hotmail.com>
 *         <p/>
 *         This is free software, licensed under the GNU General Public License v3.
 *         See http://www.gnu.org/licenses/gpl.html for more information.
 */
public class FragmentTabHostEstadisticasGlobalesTemperatura extends Fragment {
    // Bundle del FragmentEstadisticasGlobales
    private Bundle bundle;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Mostramos los items de la action bar
        setHasOptionsMenu(true);
        // Recuperamos el bundle
        bundle = getArguments();
        // Recuperamos la lista de los totales de las estadisticas
        ArrayList<Registro> listaEstadisticasGlobalesTotalesRegistros = bundle.getParcelableArrayList("listaEstadisticasGlobalesTotalesRegistros");
        // Inflamos el layout en este fragment
        View viewFragmentTabHostEstadisticasGlobalesTemperatura = inflater.inflate(R.layout.fragment_tab_host_estadisticas_globales_temperatura, container, false);
        // Lista que mostrara las estadisticas globales de temperatura
        ListView listViewTabHostEstadisticasGlobalesTemperatura = (ListView) viewFragmentTabHostEstadisticasGlobalesTemperatura.findViewById(R.id.listViewTabHostEstadisticasGlobalesTemperatura);
        // Recuperamos el linearLayoutEstadisticasGlobalesTemperatura
        LinearLayout linearLayoutEstadisticasGlobalesTemperatura = (LinearLayout) viewFragmentTabHostEstadisticasGlobalesTemperatura.findViewById(R.id.linearLayoutEstadisticasGlobalesTemperatura);
        // Ponemos la imageview de no items en escala de grises
        ImageView imageView = (ImageView) viewFragmentTabHostEstadisticasGlobalesTemperatura.findViewById(R.id.imgViewEstadisticasGlobalesTemperatura);
        Utils.imgViewPintarImagen(R.color.color_gris_claro, imageView);
        // Inflamos el text view
        TextView tvErrorEstadisticasGlobalesTemperatura = (TextView) viewFragmentTabHostEstadisticasGlobalesTemperatura.findViewById(R.id.tvErrorEstadisticasGlobalesTemperatura);
        // En caso que no haya items se mostrara el tvError
        listViewTabHostEstadisticasGlobalesTemperatura.setEmptyView(linearLayoutEstadisticasGlobalesTemperatura);
        // Comprobamos que haya items y los mostramos
        if (listaEstadisticasGlobalesTotalesRegistros != null) {
            // Creamos el adaptador para convertir la lista en views
            AdaptadorListViewCultivosGlobales adaptadorListViewCultivosGlobales =
                    new AdaptadorListViewCultivosGlobales(getContext(), listaEstadisticasGlobalesTotalesRegistros, "T");
            // Asignamos el adaptador a la ListView
            listViewTabHostEstadisticasGlobalesTemperatura.setAdapter(adaptadorListViewCultivosGlobales);
        } else if (!Utils.tieneConexion(getActivity())) {
            // Indicamos un mensaje de que no dispone de conexion
            tvErrorEstadisticasGlobalesTemperatura.setText(getString(R.string.fragmenttabhostestadisticasglobalestemperatura_tvnoitemslistviewcultivos_noconexion));
        } else {
            // Indicamos un mensaje de que no se han encontrado registros disponibles
            tvErrorEstadisticasGlobalesTemperatura.setText(getString(R.string.fragmenttabhostestadisticasglobalestemperatura_tvnoitemslistviewcultivos_noencontrado));
        }
        // Retornamos el layout inflado
        return viewFragmentTabHostEstadisticasGlobalesTemperatura;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        return SeleccionOpcionMenu(item);
    }

    private boolean SeleccionOpcionMenu(MenuItem item) {
        switch (item.getItemId()) {
            case 0:
                // Asignamos el tipo de grafico que queremos
                bundle.putString("graficoTipo", "T");
                // Creamos el fragment y le enviamos la lista de los meses
                FragmentGraficoGlobal fragmentGraficoGlobal = new FragmentGraficoGlobal();
                fragmentGraficoGlobal.setArguments(bundle);
                getActivity().getSupportFragmentManager().beginTransaction()
                        .replace(R.id.frame_fragment_principal, fragmentGraficoGlobal)
                        .commit();
                return true;
        }
        return false;
    }
}