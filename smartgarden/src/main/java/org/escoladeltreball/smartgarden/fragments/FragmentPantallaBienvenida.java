package org.escoladeltreball.smartgarden.fragments;


import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.content.IntentCompat;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.RadioGroup;

import org.escoladeltreball.smartgarden.R;
import org.escoladeltreball.smartgarden.activities.PantallaMenuLateral;
import org.escoladeltreball.smartgarden.adapters.AdaptadorViewPagerFragmentPantallaBienvenida;
import org.escoladeltreball.smartgarden.utils.Utils;
import org.escoladeltreball.smartgarden.utils.VariablesGlobales;


/**
 * FragmentPantallaBienvenida que se cargará en la PantallaBienvenida.
 *
 * @author Carlos DaPalma Corchado <cdapalmac@gmail.com>
 * @author Esdras Valiente Lanaspa <esdras.vl85@gmail.com>
 * @author Andres Jordan Zambrano <andresjordanz@hotmail.com>
 *         <p/>
 *         This is free software, licensed under the GNU General Public License v3.
 *         See http://www.gnu.org/licenses/gpl.html for more information.
 */
public class FragmentPantallaBienvenida extends Fragment implements ViewPager.OnPageChangeListener, View.OnClickListener, RadioGroup.OnCheckedChangeListener {
    // Variable que guarda la vista del view pager
    private ViewPager viewPager;
    // Los circulos que aparecen en el pie
    private RadioGroup rdoGroupCirculosPie;
    private ImageButton imgBtnAtras;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflamos el layout de este fragment
        View viewFragmentPantallaBienvenida = inflater.inflate(R.layout.fragment_pantalla_bienvenida, container, false);

        // Recuperamos el view pager
        viewPager = (ViewPager) viewFragmentPantallaBienvenida.findViewById(R.id.viewpagerFragmentPantallaBienvenida);
        // Creamos el adaptador para el view pager
        AdaptadorViewPagerFragmentPantallaBienvenida adaptadorViewPagerFragmentPantallaBienvenida = new AdaptadorViewPagerFragmentPantallaBienvenida(getActivity().getSupportFragmentManager());
        // Asignamos al view pager al adaptador
        viewPager.setAdapter(adaptadorViewPagerFragmentPantallaBienvenida);
        // Asignamos el item que se mostrara por defecto
        viewPager.setCurrentItem(0);

        // Recuperamos los circulos del pie del view pager
        rdoGroupCirculosPie = (RadioGroup) viewFragmentPantallaBienvenida.findViewById(R.id.rdoGroupCirculosPie);
        // Activamos el check del rdoBtnCiruclo1 por defecto
        rdoGroupCirculosPie.check(R.id.rdoBtnCirculo1);
        // Recuperamos los image butyon de siguiente y atras del pie del view pager
        ((VariablesGlobales) getActivity().getApplication()).setImgBtnSiguiente((ImageButton) viewFragmentPantallaBienvenida.findViewById(R.id.imgBtnSiguiente));
        imgBtnAtras = (ImageButton) viewFragmentPantallaBienvenida.findViewById(R.id.imgBtnAtras);
        // Ocultamos el boton de atras por defecto
        imgBtnAtras.setVisibility(View.GONE);

        // Asignamos al view pager un escuchador de cambios de pagina
        viewPager.addOnPageChangeListener(this);

        // Asignamos al grupo de radio buttons y los botones que aparecen en el pie un escuchador de clicks
        rdoGroupCirculosPie.setOnCheckedChangeListener(this);
        ((VariablesGlobales) getActivity().getApplication()).getImgBtnSiguiente().setOnClickListener(this);
        imgBtnAtras.setOnClickListener(this);

        // Retornamos la vista inflada
        return viewFragmentPantallaBienvenida;
    }

    @Override
    public void onPageScrollStateChanged(int state) {
    }

    @Override
    public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
    }

    /**
     * Metodo que se llamara cuando se cambie la pagina del view pager.
     * Dependiento de la posicion cambiamos la imagen de los items del pie a seleccionada.
     * También ocultamos la action bar dependiento de la pantalla que se muestre.
     *
     * @param position el numero de la pagina del view pager seleccionada.
     */
    @Override
    public void onPageSelected(int position) {
        // Dependiendo de la pagina selecciona mostramos el radio button checked y los image button
        // de siguiente y atras correspondientes
        if (position == 0) {
            rdoGroupCirculosPie.check(R.id.rdoBtnCirculo1);
            Utils.obtenerActionBar(getActivity()).hide();
            ((VariablesGlobales) getActivity().getApplication()).getImgBtnSiguiente().setVisibility(View.VISIBLE);
            imgBtnAtras.setVisibility(View.GONE);
        } else if (position == 1) {
            rdoGroupCirculosPie.check(R.id.rdoBtnCirculo2);
            Utils.obtenerActionBar(getActivity()).show();
            if (((VariablesGlobales) getActivity().getApplication()).isMostrarImgBtnSiguiente()) {
                ((VariablesGlobales) getActivity().getApplication()).getImgBtnSiguiente().setVisibility(View.VISIBLE);
            } else {
                ((VariablesGlobales) getActivity().getApplication()).getImgBtnSiguiente().setVisibility(View.GONE);
            }
            imgBtnAtras.setVisibility(View.VISIBLE);
        }
    }

    /**
     * Metodo que usaremos para las funciones del boton de siguiente y atras.
     *
     * @param view del item pulsado.
     */
    @Override
    public void onClick(View view) {
        if (view == ((VariablesGlobales) getActivity().getApplication()).getImgBtnSiguiente()) {
            if (viewPager.getCurrentItem() == 0) {
                viewPager.setCurrentItem(1);
            } else {
                Intent intent = new Intent(getActivity(), PantallaMenuLateral.class);
                // Añadimos estas lineas para cerrar la app al presionar back
                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | IntentCompat.FLAG_ACTIVITY_CLEAR_TASK);
                startActivity(intent);
            }
        } else {
            viewPager.setCurrentItem(0);
        }
    }

    /**
     * Metodo que usaremos en caso que pulse en un radio button del pie.
     * Dependiendo del radio button pulsado cambiamos el fragment del view pager
     * que se ha de mostrar.
     *
     * @param radioGroup el grupo de los radio button
     * @param checkedId  el id del radio button pulsado
     */
    @Override
    public void onCheckedChanged(RadioGroup radioGroup, int checkedId) {
        switch (checkedId) {
            case R.id.rdoBtnCirculo1:
                viewPager.setCurrentItem(0);
                break;
            default:
                viewPager.setCurrentItem(1);
                break;
        }
    }
}