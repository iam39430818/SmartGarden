package org.escoladeltreball.smartgarden.fragments;


import android.app.ProgressDialog;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.escoladeltreball.smartgarden.R;
import org.escoladeltreball.smartgarden.adapters.AdaptadorViewPagerFragmentEstadisticasCultivo;
import org.escoladeltreball.smartgarden.json.JSONPullParserRegistros;
import org.escoladeltreball.smartgarden.pojo.Registro;
import org.escoladeltreball.smartgarden.utils.ObtenerDatosJSON;
import org.escoladeltreball.smartgarden.utils.Utils;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;


/**
 * FragmentEstadisticasCultivo que se cargará en el FragmentCultivo.
 *
 * @author Carlos DaPalma Corchado <cdapalmac@gmail.com>
 * @author Esdras Valiente Lanaspa <esdras.vl85@gmail.com>
 * @author Andres Jordan Zambrano <andresjordanz@hotmail.com>
 *         <p/>
 *         This is free software, licensed under the GNU General Public License v3.
 *         See http://www.gnu.org/licenses/gpl.html for more information.
 */
public class FragmentEstadisticasCultivo extends Fragment {
    // Tablayout del FragmentEstadisticasCultivo
    private TabLayout tabLayout;
    // ViewPager del FragmentEstadisticasCultivo
    private ViewPager viewPager;
    // Identificador del cultivo seleccionado
    private String idCultivo;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Recuperamos los datos enviados de FragmentCultivo
        idCultivo = getArguments().getString("idCultivo");
        // Asigno el titulo a la action bar que sera el nombre del cultivo seleccionado
        getActivity().setTitle(getArguments().getString("nombre"));
        // Mostramos los items de la action bar
        setHasOptionsMenu(true);
        // Inflamos el layout que tiene el tabhost customizado en la parte superior
        View viewFragmentEstadisticasCultivo = inflater.inflate(R.layout.fragment_estadisticas_cultivo, container, false);
        // Recuperamos el tab layout
        tabLayout = (TabLayout) viewFragmentEstadisticasCultivo.findViewById(R.id.tabLayoutFragmentEstadisticasCultivo);
        // Recuperamos el view pager
        viewPager = (ViewPager) viewFragmentEstadisticasCultivo.findViewById(R.id.viewPagerFragmentEstadisticasCultivo);
        // Hilo para cargar las estadisticas del cultivo
        new CargarEstadisticasCultivo(idCultivo).execute();
        // Retornamos la vista inflada
        return viewFragmentEstadisticasCultivo;
    }


    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        CrearMenu(menu);
    }

    private void CrearMenu(Menu menu) {
        // Mostramos el ic_anadir
        MenuItem mnu1 = menu.add(0, 0, 0, getString(R.string.action_bar_sincronizar_estadisticascultivo));
        mnu1.setIcon(R.drawable.ic_sincronizar);
        mnu1.setShowAsAction(
                MenuItem.SHOW_AS_ACTION_IF_ROOM |
                        MenuItem.SHOW_AS_ACTION_WITH_TEXT);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        return SeleccionOpcionMenu(item);
    }

    private boolean SeleccionOpcionMenu(MenuItem item) {
        switch (item.getItemId()) {
            case 0:
                // Obtenemos los datos del servidor y los actualizamos
                new CargarEstadisticasCultivo(idCultivo).execute();
                return true;
        }
        return false;
    }

    // Metodo que usaremos para la obtencion de las estadisticas del cultivo
    private class CargarEstadisticasCultivo extends AsyncTask<Void, Void, ArrayList<Registro>> {
        // Caja de dialogo
        private ProgressDialog dialogEstadisticasCultivo;
        // Id del cultivo seleccionado
        private String idCultivo;

        // Constructor para obtener el id del cultivo
        public CargarEstadisticasCultivo(String idCultivo) {
            this.idCultivo = idCultivo;
        }

        @Override
        protected void onPreExecute() {
            // Dialogo que se mostrara para cargar los datos del cultivo
            dialogEstadisticasCultivo = new ProgressDialog(getActivity());
            dialogEstadisticasCultivo.setTitle(getString(R.string.fragment_estadisticas_cultivo_dialog_titulo));
            dialogEstadisticasCultivo.setMessage(getString(R.string.fragment_estadisticas_cultivo_dialog_mensaje));
            dialogEstadisticasCultivo.setProgressStyle(ProgressDialog.STYLE_SPINNER);
            dialogEstadisticasCultivo.setIndeterminate(true);
            dialogEstadisticasCultivo.setCancelable(false);
            dialogEstadisticasCultivo.show();
        }

        @Override
        protected ArrayList<Registro> doInBackground(Void... arg0) {
            // Creamos una instancia de ObtenerDatosJSON
            ObtenerDatosJSON datosJSON = new ObtenerDatosJSON();
            // Creamos los parametros para la funcion
            List<NameValuePair> parametros = new ArrayList<>();
            // Un 1 en el tipo para que nos devuelva nuestros cultivos
            parametros.add(new BasicNameValuePair("idCultivo", idCultivo));
            parametros.add(new BasicNameValuePair("fechaUsuario", Calendar.getInstance().get(Calendar.YEAR) + ""));
            // Hacemos la solicitud de obtener el documento JSON, en este caso un POST
            String documentoJSON = datosJSON.obtenerDocumentoJSON(Utils.URL_OBTENER_REGISTROS, ObtenerDatosJSON.POST, parametros);
            // Parseamos el documento obtenido
            JSONPullParserRegistros jsonPullParser = new JSONPullParserRegistros(getActivity(), documentoJSON);
            return jsonPullParser.parseArrayList();
        }

        // Metodo que hará la función del tratamiento del JSON
        protected void onPostExecute(ArrayList<Registro> listaRegistros) {
            super.onPostExecute(listaRegistros);
            // Ocultamos el dialogo
            dialogEstadisticasCultivo.dismiss();
            // Creamos los parametros que se enviaran a los fragments del view pager
            Bundle bundle = new Bundle();
            bundle.putParcelableArrayList("listaRegistrosEstadisticasCultivo", listaRegistros);
            // Creamos el adaptador para el view pager
            AdaptadorViewPagerFragmentEstadisticasCultivo adaptadorViewPagerFragmentEstadisticasCultivo =
                    new AdaptadorViewPagerFragmentEstadisticasCultivo(getActivity(), getChildFragmentManager(), bundle);
            // Asignamos al view pager al adaptador
            viewPager.setAdapter(adaptadorViewPagerFragmentEstadisticasCultivo);
            // Hacemos un hilo para el setupWithViewPager porque sino no funciona debido a un fallo en la
            // libreria de soporte
            tabLayout.post(new Runnable() {
                @Override
                public void run() {
                    tabLayout.setupWithViewPager(viewPager);
                }
            });
        }
    }
}
