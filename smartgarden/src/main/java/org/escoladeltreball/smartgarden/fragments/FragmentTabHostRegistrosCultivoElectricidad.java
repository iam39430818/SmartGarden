package org.escoladeltreball.smartgarden.fragments;


import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import com.github.mikephil.charting.animation.Easing;
import com.github.mikephil.charting.charts.PieChart;
import com.github.mikephil.charting.components.Legend;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.data.PieData;
import com.github.mikephil.charting.data.PieDataSet;
import com.github.mikephil.charting.utils.ColorTemplate;
import com.github.mikephil.charting.utils.LargeValueFormatter;

import org.escoladeltreball.smartgarden.R;
import org.escoladeltreball.smartgarden.adapters.AdaptadorListviewRegistrosCultivo;
import org.escoladeltreball.smartgarden.pojo.Registro;
import org.escoladeltreball.smartgarden.utils.Utils;

import java.util.ArrayList;

/**
 * FragmentTabHostRegistrosCultivoElectricidad que se cargará en el FragmentEstadisticaCultivo.
 *
 * @author Carlos DaPalma Corchado <cdapalmac@gmail.com>
 * @author Esdras Valiente Lanaspa <esdras.vl85@gmail.com>
 * @author Andres Jordan Zambrano <andresjordanz@hotmail.com>
 *         <p/>
 *         This is free software, licensed under the GNU General Public License v3.
 *         See http://www.gnu.org/licenses/gpl.html for more information.
 */
public class FragmentTabHostRegistrosCultivoElectricidad extends Fragment {
    // View del layout actual
    private View viewFragmentTabHostEstadisticasCultivoElectricidad;
    // Lista de todos los consumos del cultivo
    private ArrayList<Registro> listaEstadisticasCultivoElectricidad;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Recuperamos la lista de los registros
        listaEstadisticasCultivoElectricidad = getArguments().getParcelableArrayList("listaRegistrosEstadisticasCultivo");
        // Inflamos el layout en este fragment
        viewFragmentTabHostEstadisticasCultivoElectricidad = inflater.inflate(R.layout.fragment_tab_host_estadisticas_cultivo_electricidad, container, false);
        // Cargamos la list view de registros
        ListView listViewRegistros = (ListView) viewFragmentTabHostEstadisticasCultivoElectricidad.findViewById(R.id.listViewTabHostEstadisticasCultivoElectricidad);
        // Recuperamos el layoutNoItemsListaCultivoElectricidad
        LinearLayout layoutNoItemsListaCultivoElectricidad = (LinearLayout) viewFragmentTabHostEstadisticasCultivoElectricidad.findViewById(R.id.linearLayoutErrorRegistrosCultivoElectricidad);
        // Ponemos la imageview de no item en escala de grises
        ImageView imagen = (ImageView) viewFragmentTabHostEstadisticasCultivoElectricidad.findViewById(R.id.imgViewErrorRegistrosCultivoElectricidad);
        // Coloreamos la imagen de gris
        Utils.imgViewPintarImagen(R.color.color_gris_claro, imagen);
        // Inflamos el text view de error
        TextView tvErrorRegistrosCultivoElectricidad = (TextView) viewFragmentTabHostEstadisticasCultivoElectricidad.findViewById(R.id.tvErrorRegistrosCultivoElectricidad);
        // En caso que no haya items se mostrara un mensaje indicandolo
        listViewRegistros.setEmptyView(layoutNoItemsListaCultivoElectricidad);
        // Comprobamos que haya items y los mostramos
        if (listaEstadisticasCultivoElectricidad != null) {
            // Creamos el adaptador
            AdaptadorListviewRegistrosCultivo adaptadorListviewRegistrosCultivoElectricidad = new AdaptadorListviewRegistrosCultivo(getContext(), listaEstadisticasCultivoElectricidad, "E");
            // Asignamos el adaptador a la listview
            listViewRegistros.setAdapter(adaptadorListviewRegistrosCultivoElectricidad);
            // Cargamos los graficos
            cargarGraficoElectricidad();
        } else if (!Utils.tieneConexion(getActivity())) {
            // Indicamos un mensaje de que no dispone de conexion
            tvErrorRegistrosCultivoElectricidad.setText(getString(R.string.fragmenttabhostestadisticascultivoelectricidad_tvnoitemslistviewcultivos_noconexion));
        } else {
            // Indicamos un mensaje de que no se han encontrado cultivos
            tvErrorRegistrosCultivoElectricidad.setText(getString(R.string.fragmenttabhostestadisticascultivoelectricidad_tvnoitemslistviewcultivos_noencontrado));
        }
        // Retornamos la vista inflada
        return viewFragmentTabHostEstadisticasCultivoElectricidad;
    }

    /**
     * Metodo que usaremos para cargar los graficos.
     */
    private void cargarGraficoElectricidad() {
        // Obtenemos el grafico de electricidad del layout
        PieChart graficoConsumoElectrico = (PieChart) viewFragmentTabHostEstadisticasCultivoElectricidad.findViewById(R.id.estadisticasCultivoGraficoElectricidad);
        // No lo mostramos los valores en %
        graficoConsumoElectrico.setUsePercentValues(false);
        // No le asignamos ningun titulo
        graficoConsumoElectrico.setDescription("");
        // Pintamos el titulo en el centro del grafico
        graficoConsumoElectrico.setDrawCenterText(true);
        // Asignamos el texto del centro del grafico
        graficoConsumoElectrico.setCenterText(getString(R.string.fragmenttabhostestadisticascultivoelectricidad_titulo_grafico));
        // Habilitamos que se pueda rotar
        graficoConsumoElectrico.setRotationEnabled(true);
        // Ocultamos el texto de los valores del grafico
        graficoConsumoElectrico.setDrawSliceText(false);
        // Deshacer destacados
        graficoConsumoElectrico.highlightValues(null);
        graficoConsumoElectrico.invalidate();
        graficoConsumoElectrico.animateY(1500, Easing.EasingOption.EaseInOutQuad);
        graficoConsumoElectrico.setDrawHoleEnabled(true);
        graficoConsumoElectrico.setHoleColorTransparent(true);
        graficoConsumoElectrico.setTransparentCircleColor(Color.WHITE);
        graficoConsumoElectrico.setDragDecelerationFrictionCoef(0.95f);
        graficoConsumoElectrico.setHoleRadius(45f);
        graficoConsumoElectrico.setTransparentCircleRadius(48f);
        graficoConsumoElectrico.setRotationAngle(0);

        // Creamos la lista de los datos de los graficos
        ArrayList<String> listaMeses = new ArrayList<>();
        ArrayList<Entry> listaEntryMeses = new ArrayList<>();
        // Los valores de numero de meses los asignamos como integer
        float numeroDeMeses;
        // Obtenemos la lista de meses con el numero de meses
        for (int i = 0; i < listaEstadisticasCultivoElectricidad.size(); i++) {
            listaMeses.add(listaEstadisticasCultivoElectricidad.get(i).getMes());
            numeroDeMeses = Float.valueOf("" + Float.parseFloat(listaEstadisticasCultivoElectricidad.get(i).getConsumo()));
            listaEntryMeses.add(new Entry(numeroDeMeses, -1));
        }

        // Añadimos los colores
        ArrayList<Integer> colors = new ArrayList<>();
        for (int c : ColorTemplate.VORDIPLOM_COLORS)
            colors.add(c);
        for (int c : ColorTemplate.PASTEL_COLORS)
            colors.add(c);
        for (int c : ColorTemplate.JOYFUL_COLORS)
            colors.add(c);
        for (int c : ColorTemplate.COLORFUL_COLORS)
            colors.add(c);
        for (int c : ColorTemplate.LIBERTY_COLORS)
            colors.add(c);
        colors.add(ColorTemplate.getHoloBlue());

        // Creamos la leyenda del grafico de electricidad
        Legend lPie1 = graficoConsumoElectrico.getLegend();
        lPie1.setPosition(Legend.LegendPosition.LEFT_OF_CHART_CENTER);
        lPie1.setXEntrySpace(7f);
        // Asignamos el espacio entre lineas
        lPie1.setYEntrySpace(5f);
        lPie1.setYOffset(0f);
        // Asignamos el tamaño de la letra
        lPie1.setTextSize(16f);
        // Creamos el valor del numero de meses
        PieDataSet valorGraficoNMeses = new PieDataSet(listaEntryMeses, "");
        valorGraficoNMeses.setSliceSpace(3f);
        valorGraficoNMeses.setSelectionShift(5f);
        valorGraficoNMeses.setColors(colors);
        // Creamos los valores del grafico de electricidad
        PieData valoresGraficosNMeses = new PieData(listaMeses, valorGraficoNMeses);
//        valoresGraficosNMeses.setValueFormatter(new LargeValueFormatter());
        valoresGraficosNMeses.setValueTextSize(16f);
        valoresGraficosNMeses.setValueTextColor(Color.BLACK);
        // Asignamos los valores al grafico de electricidad
        graficoConsumoElectrico.setData(valoresGraficosNMeses);
        // Mostramos el grafico de electricidad
        graficoConsumoElectrico.setVisibility(View.VISIBLE);

    }

}


