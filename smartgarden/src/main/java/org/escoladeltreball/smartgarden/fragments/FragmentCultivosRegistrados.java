package org.escoladeltreball.smartgarden.fragments;


import android.app.ProgressDialog;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.text.InputType;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.SearchView;
import android.widget.TextView;

import com.github.amlcurran.showcaseview.ShowcaseView;
import com.github.amlcurran.showcaseview.targets.ViewTarget;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.escoladeltreball.smartgarden.R;
import org.escoladeltreball.smartgarden.adapters.AdaptadorListViewCultivosRegistrados;
import org.escoladeltreball.smartgarden.json.JSONPullParserCultivos;
import org.escoladeltreball.smartgarden.pojo.Cultivo;
import org.escoladeltreball.smartgarden.utils.ObtenerDatosJSON;
import org.escoladeltreball.smartgarden.utils.Utils;

import java.util.ArrayList;
import java.util.List;

/**
 * FragmentCultivosRegistrados que se cargará en la PantallaMenuLateral.
 *
 * @author Carlos DaPalma Corchado <cdapalmac@gmail.com>
 * @author Esdras Valiente Lanaspa <esdras.vl85@gmail.com>
 * @author Andres Jordan Zambrano <andresjordanz@hotmail.com>
 *         <p/>
 *         This is free software, licensed under the GNU General Public License v3.
 *         See http://www.gnu.org/licenses/gpl.html for more information.
 */
public class FragmentCultivosRegistrados extends Fragment implements AdapterView.OnItemClickListener {
    // ArrayList de los dispositivos bluetooh encontrados
    private ArrayList<Cultivo> listaCultivos = new ArrayList<>();
    // View que se inflara en este fragment
    private View viewFragmentCultivosRegistrados;
    // ListView que mostrar los cultivos disponibles
    private ListView listViewCultivosRegistrados;
    // Adaptador para listview de los cultivos registrados
    private AdaptadorListViewCultivosRegistrados adaptadorListViewCultivosRegistrados;
    // Variable que nos servira para contar las pantallas que va viendo el usuario
    private int contadorClicksShowCaseView = 0;
    // ShowCaseView de ayuda
    private ShowcaseView showcaseView;
    // SearchView donde se filtra la busqueda de la listview
    private SearchView filtroBusquedaCultivos;
    // Items de la action bar para añadir y sincronizar la lista de cultivos
    private MenuItem ic_anadir, ic_sincronizar;
    // El text view que nos indicara el progreso
    private TextView tvMensajeListViewCultivosRegistrados;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Asignamos el titulo a la action bar
        getActivity().setTitle(getString(R.string.fragmentcultivosregistrados_titulo));
        // Mostramos los items de la action bar
        setHasOptionsMenu(true);
        // Inflamos el layout de este fragment
        viewFragmentCultivosRegistrados = inflater.inflate(R.layout.fragment_cultivos_registrados, container, false);
        // Recuperamos el layoutNoItemsListViewCultivosRegistrados
        LinearLayout layoutNoItems = (LinearLayout) viewFragmentCultivosRegistrados.findViewById(R.id.layoutNoItemsListaCultivosRegistrados);
        // Lista que mostrara los cultivos registrados
        listViewCultivosRegistrados = (ListView) viewFragmentCultivosRegistrados.findViewById(R.id.listViewCultivosRegistrados);
        // Ponemos la imageview de no item en escala de grises
        ImageView imagen = (ImageView) viewFragmentCultivosRegistrados.findViewById(R.id.imgViewNoItemsCultivosRegistrados);
        Utils.imgViewEscalaGrises(imagen);
        // Recuperamos el tvMensajeListViewCultivosRegistrados
        tvMensajeListViewCultivosRegistrados = (TextView) viewFragmentCultivosRegistrados.findViewById(R.id.tvMensajeListViewCultivosRegistrados);
        // En caso que no haya items se mostrara un mensaje indicandolo
        listViewCultivosRegistrados.setEmptyView(layoutNoItems);
        listViewCultivosRegistrados.setTextFilterEnabled(true);
        // Añadimos el escuchador de clicks en los items
        listViewCultivosRegistrados.setOnItemClickListener(this);
        // Objeto para filtrar la busqueda en la lista
        filtroBusquedaCultivos = (SearchView) viewFragmentCultivosRegistrados.findViewById(R.id.searchViewCultivosRegistrados);
        filtroBusquedaCultivos.setVisibility(View.VISIBLE);
        // Que se muestre la linea de escribir
        filtroBusquedaCultivos.setIconifiedByDefault(false);
        filtroBusquedaCultivos.setOnQueryTextListener(escuchadorSearchView);
        // Que no se muestre al lado de la X el boton de submit
        filtroBusquedaCultivos.setSubmitButtonEnabled(false);
        // Para que no aparezca el foco en el filtro de busqueda
        filtroBusquedaCultivos.setFocusable(false);
        // Retornamos la vista inflada
        return viewFragmentCultivosRegistrados;
    }

    /**
     * Escuchador del filtroBusquedaCultivos para el texto insertado
     */
    SearchView.OnQueryTextListener escuchadorSearchView = new SearchView.OnQueryTextListener() {
        @Override
        public boolean onQueryTextSubmit(String newText) {
            return false;
        }

        @Override
        public boolean onQueryTextChange(String newText) {
            if (adaptadorListViewCultivosRegistrados != null) {
                // Si el campo queda vacio se envia una cadena nula, sino, se filtra esa cadena
                if (TextUtils.isEmpty(newText)) {
                    adaptadorListViewCultivosRegistrados.getFilter().filter(null);
                } else {
                    adaptadorListViewCultivosRegistrados.getFilter().filter(newText);
                }
            }
            return true;
        }
    };

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        CrearMenu(menu);
        // Cuando se crea el menu creamos la ayuda y la mostramos en caso que se tenga que mostrar
        ShowcaseView.Builder showcaseViewBuilder = Utils.crearShowCaseView(getActivity(),
                this.getClass().getSimpleName(),
                new ViewTarget(((ViewGroup) getActivity().findViewById(R.id.actionbar)).getChildAt(1)),
                getString(R.string.fragmentcultivosregistrados_showcaseview_menu_titulo),
                getString(R.string.fragmentcultivosregistrados_showcaseview_menu_texto));
        if (showcaseViewBuilder != null) {
            // Deshabilitamos el drawer layout
            getActivity().findViewById(R.id.drawer_layout_menu_principal).setEnabled(false);
            // Deshabilitamos el searchview
            filtroBusquedaCultivos.setInputType(InputType.TYPE_NULL);
            // Deshabilitamos el boton de añadir y sincronizar de la action bar
            ic_anadir.setEnabled(false);
            ic_sincronizar.setEnabled(false);
            // Añadimos el escuchador de clicks y creamos el showcaseview
            showcaseViewBuilder.setOnClickListener(clicksShowCaseView);
            showcaseView = showcaseViewBuilder.build();
            showcaseView.setButtonText(getString(android.R.string.ok));
        } else {
            // Ejecutamos el hilo para obtener los cultivos registrados
            new ObtenerCultivosRegistrados().execute();
        }
    }

    private void CrearMenu(Menu menu) {
        // Añadimos a la action bar el ic_anadir
        ic_anadir = menu.add(0, 0, 0, getString(R.string.action_bar_anadir_cultivosregistrados));
        ic_anadir.setIcon(R.drawable.ic_anadir);
        ic_anadir.setShowAsAction(
                MenuItem.SHOW_AS_ACTION_IF_ROOM |
                        MenuItem.SHOW_AS_ACTION_WITH_TEXT);
        // Añadimos a la action bar el ic_sincronizar
        ic_sincronizar = menu.add(1, 1, 1, getString(R.string.action_bar_sincronizar_cultivosregistrados));
        ic_sincronizar.setIcon(R.drawable.ic_sincronizar);
        ic_sincronizar.setShowAsAction(MenuItem.SHOW_AS_ACTION_IF_ROOM | MenuItem.SHOW_AS_ACTION_WITH_TEXT);

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        return SeleccionOpcionMenu(item);
    }

    private boolean SeleccionOpcionMenu(MenuItem item) {
        switch (item.getItemId()) {
            case 0:
                Fragment fragment = new FragmentBusquedaCultivos();
                // En caso que seleccione el ic_anadir irá a la pantalla de busqueda de cultivo
                getActivity().getSupportFragmentManager().beginTransaction()
                        .replace(R.id.frame_fragment_principal, fragment)
                        .commit();
                return true;
            case 1:
                if (listaCultivos != null) {
                    // En caso que seleccion el ic_refrescar se resfresca la lista de los dispositivos
                    refrescarCultivosDisponibles();
                }
                // En caso de sincronizar borramos lo que haya en la searchView
                filtroBusquedaCultivos.setQuery("", false);
                // Ejecutamos el hilo para obtener los cultivos registrados
                new ObtenerCultivosRegistrados().execute();
                return true;
        }
        return false;
    }

    /**
     * Escuchador de clicks de los items de la listViewCultivosRegistrados.
     *
     * @param adaptador adaptador de la listview
     * @param view      pulsada
     * @param posicion  de la view en la lista
     * @param id        de la view
     */

    @Override
    public void onItemClick(AdapterView<?> adaptador, View view, int posicion, long id) {
        Fragment fragment = new FragmentCultivo();
        Bundle bundle = new Bundle();
        bundle.putParcelable("cultivo", listaCultivos.get(posicion));
        fragment.setArguments(bundle);
        getActivity().getSupportFragmentManager().beginTransaction()
                .replace(R.id.frame_fragment_principal, fragment)
                .commit();
    }

    /**
     * Escuchador de los clicks del showcaseview.
     */
    View.OnClickListener clicksShowCaseView = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            // Depende de los clicks mostramos el showcaseview indicado o lo ocultamos
            switch (contadorClicksShowCaseView) {
                case 0:
                    showcaseView.setShowcase(new ViewTarget(viewFragmentCultivosRegistrados.findViewById(R.id.searchViewCultivosRegistrados)), true);
                    showcaseView.setContentTitle(getString(R.string.fragmentcultivosregistrados_showcaseview_searchview_titulo));
                    showcaseView.setContentText(getString(R.string.fragmentcultivosregistrados_showcaseview_searchview_texto));
                    break;
                case 1:
                    showcaseView.setShowcase(new ViewTarget(((ViewGroup) getActivity().findViewById(R.id.actionbar)).getChildAt(2)), true);
                    showcaseView.setContentTitle(getString(R.string.fragmentcultivosregistrados_showcaseview_anadir_titulo));
                    showcaseView.setContentText(getString(R.string.fragmentcultivosregistrados_showcaseview_anadir_texto));
                    showcaseView.setButtonText(getString(android.R.string.ok));
                    break;
                default:
                    showcaseView.hide();
                    // Ejecutamos el hilo para obtener los cultivos registrados
                    new ObtenerCultivosRegistrados().execute();
                    // Habilitamos el drawer layout
                    getActivity().findViewById(R.id.drawer_layout_menu_principal).setEnabled(true);
                    // Habilitamos el searchview
                    filtroBusquedaCultivos.setInputType(InputType.TYPE_CLASS_TEXT);
                    // Habilitamos el boton de añadir y sincronizar de la action bar
                    ic_anadir.setEnabled(true);
                    ic_sincronizar.setEnabled(true);
                    break;
            }
            contadorClicksShowCaseView++;
        }
    };

    /**
     * Este metodo sirve para refrescar la lista de cultivos registrados
     */
    private void refrescarCultivosDisponibles() {
        // Limpiamos la lista anterior antes de volver a buscar
        listaCultivos.clear();
        adaptadorListViewCultivosRegistrados.notifyDataSetChanged();

    }

    /**
     * Hilo que usaremos para conectar al servidor y obtener la lista de los cultivos registrados
     */
    private class ObtenerCultivosRegistrados extends AsyncTask<Void, Void, Void> {
        // Dialogo que se mostrara cuando se inicie la busqueda de los cultivos registrados
        private ProgressDialog progressDialogObtenerCultivos;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            // Mostramos el progress dialog
            progressDialogObtenerCultivos = new ProgressDialog(getActivity());
            progressDialogObtenerCultivos.setTitle(getString(R.string.fragmentcultivosregistrados_progressdialogcultivos_titulo));
            progressDialogObtenerCultivos.setMessage(getString(R.string.fragmentcultivosregistrados_progressdialogcultivos_mensaje));
            progressDialogObtenerCultivos.setProgressStyle(ProgressDialog.STYLE_SPINNER);
            progressDialogObtenerCultivos.setIndeterminate(true);
            progressDialogObtenerCultivos.setCancelable(false);
            progressDialogObtenerCultivos.show();
        }

        @Override
        protected Void doInBackground(Void... arg0) {
            // Creamos una instancia de ObtenerDatosJSON
            ObtenerDatosJSON datosJSON = new ObtenerDatosJSON();
            // Creamos los parametros para la funcion
            List<NameValuePair> parametros = new ArrayList<>();
            // Un 1 ene l tipo para que nos devuelva nuestros cultivos
            parametros.add(new BasicNameValuePair("tipo", "1"));
            // Los cultivos de este usuario
            parametros.add(new BasicNameValuePair("usuario", Build.SERIAL));
            // Hacemos la solicitud de obtener el documento JSON, en este caso un POST
            String documentoJSON = datosJSON.obtenerDocumentoJSON(Utils.URL_CULTIVOS, ObtenerDatosJSON.POST, parametros);
            // Parseamos el documento obtenido
            JSONPullParserCultivos jsonPullParser = new JSONPullParserCultivos(documentoJSON);
            listaCultivos = jsonPullParser.parse();
            return null;
        }

        @Override
        protected void onPostExecute(Void result) {
            super.onPostExecute(result);
            // Cuando acabe ocultamos la progress dialog
            if (progressDialogObtenerCultivos != null) {
                progressDialogObtenerCultivos.dismiss();
            }
            // Comprobamos que haya items y los mostramos
            if (listaCultivos != null) {
                // Creamos el adaptador
                adaptadorListViewCultivosRegistrados = new AdaptadorListViewCultivosRegistrados(getContext(), listaCultivos);
                // Asignamos el adaptador a la listview
                listViewCultivosRegistrados.setAdapter(adaptadorListViewCultivosRegistrados);
            } else if (!Utils.tieneConexion(getActivity())) {
                // Indicamos un mensaje de que no dispone de conexion
                tvMensajeListViewCultivosRegistrados.setText(getString(R.string.fragmentcultivosregistrados_tvnoitemslistviewcultivos_noconexion));
            } else {
                // Indicamos un mensaje de que no se han encontrado cultivos
                tvMensajeListViewCultivosRegistrados.setText(getString(R.string.fragmentcultivosregistrados_tvnoitemslistviewcultivos_noencontrado));
            }
        }
    }
}
