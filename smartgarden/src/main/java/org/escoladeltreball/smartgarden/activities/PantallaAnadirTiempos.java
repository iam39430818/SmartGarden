package org.escoladeltreball.smartgarden.activities;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.CompoundButton;
import android.widget.Switch;
import android.widget.TimePicker;

import org.escoladeltreball.smartgarden.R;
import org.escoladeltreball.smartgarden.pojo.Funcion;
import org.escoladeltreball.smartgarden.utils.Utils;

import java.util.ArrayList;

/**
 * Activity que se cargará al seleccionar un item de la action bar de PantallaGestionTiempos.
 *
 * @author Carlos DaPalma Corchado <cdapalmac@gmail.com>
 * @author Esdras Valiente Lanaspa <esdras.vl85@gmail.com>
 * @author Andres Jordan Zambrano <andresjordanz@hotmail.com>
 *         <p/>
 *         This is free software, licensed under the GNU General Public License v3.
 *         See http://www.gnu.org/licenses/gpl.html for more information.
 */
public class PantallaAnadirTiempos extends AppCompatActivity implements CompoundButton.OnCheckedChangeListener {
    // Variable para guardar la función con la que trabajemos
    private Funcion funcion;
    // Switch para activar la funcion
    private Switch switchActivar;
    // TimePicker de la hora de inicio
    private TimePicker horaInicio;
    // TimePicker de la hora de fin
    private TimePicker horaFin;
    // Datos pasados de la PantallaGestionTiempos
    private Bundle extras;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pantalla_anadir_tiempos);
        // Mostramos la flecha de back en la ActionBar
        Utils.obtenerActionBar(this).setDisplayHomeAsUpEnabled(true);
        // Switch para activar, desactivar la funcion
        switchActivar = (Switch) findViewById(R.id.switchActivarFucnion);
        // Añadimos el escuchador al switch
        switchActivar.setOnCheckedChangeListener(this);
        // Recuperamos los TimePicker de la view
        horaInicio = (TimePicker) findViewById(R.id.horaInicioAnadirTiempos);
        horaFin = (TimePicker) findViewById(R.id.horaFinAnadirTiempos);
        // Recuperamos los datos pasados de la PantallaRegistroCultivo
        extras = getIntent().getExtras();
        // Guardamos la funcion
        funcion = extras.getParcelable("funcion");
        // Si la funcion no es null
        if (funcion != null && !funcion.getHoraInicio().isEmpty() && !funcion.getHoraFin().isEmpty()) {
            // Completamos los campos de la función seleccionada
            completarCampos();
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Añadimos las unidades en la hora de inicio y fin en caso de ser menor de 10
        String sHoraInicio = anadirUnidad(horaInicio.getCurrentHour(), horaInicio.getCurrentMinute());
        String sHoraFin = anadirUnidad(horaFin.getCurrentHour(), horaFin.getCurrentMinute());
        // El item seleccionado de la ActionBar
        switch (item.getItemId()) {
            // Flecha de back
            case android.R.id.home:
                actualizarFuncion(sHoraInicio, sHoraFin);
                // Iniciamos un intent
                Intent intent = new Intent();
                // Añadimos las funciones para que las trate la PantallaGestionTiempos
                extras.putParcelable("funcion", funcion);
                intent.putExtras(extras);
                // Indicamos un resultado positivo indicando que la lista retornada a sido modificada
                setResult(Activity.RESULT_OK, intent);
                // Finalizamos la Activity
                finish();
                return true;
        }
        return false;
    }

    /**
     * Método que completa los campos de la vista cuando se selecciona una función.
     */
    private void completarCampos() {
        // Activamos o desactivamos el switch
        switchActivar.setChecked(funcion.getEstado().equals("S"));
        // Guardamos las horas de la función
        String[] horas = {funcion.getHoraInicio(), funcion.getHoraFin()};
        // Guardamos los TimePicker
        ArrayList<TimePicker> timePickers = new ArrayList<>();
        timePickers.add(horaInicio);
        timePickers.add(horaFin);
        // Recorremos la lista de TimePicker setteando las horas y los minutos
        for (int i = 0; i < timePickers.size(); i++) {
            // Asignamos la hora de incio y fin al timepicker de inicio y fin
            timePickers.get(i).setCurrentHour(Integer.parseInt(horas[i].split(":")[0]));
            timePickers.get(i).setCurrentMinute(Integer.parseInt(horas[i].split(":")[1]));
        }
    }

    /**
     * Método que actualiza una función.
     *
     * @param horaInicio la hora de inicio de la función
     * @param horaFin    la hora de fin de la función
     */
    private void actualizarFuncion(String horaInicio, String horaFin) {
        // Setteamos la hora de inicio y la hora de fin de la función
        funcion.setHoraInicio(horaInicio);
        funcion.setHoraFin(horaFin);
    }

    /**
     * Método que añade un zero delante a las horas con una unidad.
     *
     * @param hora   las horas del TimePicker
     * @param minuto los minutos del TimePicker
     * @return el String con la hora y los minutos
     */
    private String anadirUnidad(int hora, int minuto) {
        // Guardamos la hora y los minutos
        String sHora = hora + "", sMinuto = minuto + "";
        // Si la hora es menor de 10
        if (hora < 10) {
            // Añadimos un 0
            sHora = "0" + hora;
        }
        // Si los minutos son menores de 10
        if (minuto < 10) {
            // Añadimos un 0
            sMinuto = "0" + minuto;
        }
        // Retornamos el String con las horas y los minutos
        return sHora + ":" + sMinuto;
    }

    @Override
    public void onCheckedChanged(CompoundButton compoundButton, boolean isChecked) {
        // Cambiamos el estado de la función
        funcion.setEstado(isChecked ? "S" : "N");
    }
}