package org.escoladeltreball.smartgarden.activities;

import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.design.widget.NavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;

import org.escoladeltreball.smartgarden.R;
import org.escoladeltreball.smartgarden.database.HistorialNotificacionDAO;
import org.escoladeltreball.smartgarden.fragments.FragmentAjustes;
import org.escoladeltreball.smartgarden.fragments.FragmentCultivosRegistrados;
import org.escoladeltreball.smartgarden.fragments.FragmentEstadisticasGlobales;
import org.escoladeltreball.smartgarden.gcm.AsyncTaskRegistrarDispositivoGCM;
import org.escoladeltreball.smartgarden.utils.Utils;

/**
 * Activity que se cargará despues de registrar un cultivo en la aplicación después
 * de la PantallaPrincipal.
 *
 * @author Carlos DaPalma Corchado <cdapalmac@gmail.com>
 * @author Esdras Valiente Lanaspa <esdras.vl85@gmail.com>
 * @author Andres Jordan Zambrano <andresjordanz@hotmail.com>
 *         <p/>
 *         This is free software, licensed under the GNU General Public License v3.
 *         See http://www.gnu.org/licenses/gpl.html for more information.
 */
public class PantallaMenuLateral extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener {
    // Variable que guardara el layout que incluye la activity y el navigation view
    private DrawerLayout drawerLayout;
    // Variable que guardara el layout del navigation view
    public NavigationView navView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pantalla_menu_lateral);

        // Vaciamos la tabla de historial de notificaciones
        HistorialNotificacionDAO historialNotificacionDAO = new HistorialNotificacionDAO(this);
        historialNotificacionDAO.abrirHistorialNotificacionDAO();
        historialNotificacionDAO.vaciarTablaHistorialNotificaciones();
        historialNotificacionDAO.cerrarHistorialNotificacionDAO();

        // Registramos el dispositivo a GCM
        new AsyncTaskRegistrarDispositivoGCM(this, false).execute();

        //Asignamos nuestra action bar pesonalizada
        Toolbar toolbar = (Toolbar) findViewById(R.id.actionbar);
        setSupportActionBar(toolbar);
        // Asignamos la imagen de back
        Drawable homeAsUpIndicator = ContextCompat.getDrawable(this, R.drawable.ic_menu);
        // Mostramos la flecha de back personalizada
        Utils.obtenerActionBar(this).setHomeAsUpIndicator(homeAsUpIndicator);
        Utils.obtenerActionBar(this).setDisplayHomeAsUpEnabled(true);
        // Obtenemos nuestro drawer layout que contiene el layout de la activity y nuestra navigation view
        drawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout_menu_principal);
        // Obtenemos el navigationView
        navView = (NavigationView) findViewById(R.id.navigationViewMenuPrincipal);
        // Le añadimos el escuchador de clicks implementado en la clase
        navView.setNavigationItemSelectedListener(this);
        // Seleccionamos la primera opcion del menu por defecto
        onNavigationItemSelected(navView.getMenu().findItem(R.id.menu_mis_cultivos));
    }

    /**
     * Escuchador de clicks del navigation view que mira cual es el item pulsado.
     *
     * @param menuItem item del menu seleccionado
     * @return true simpre al pulsar en algun item del menu
     */
    @Override
    public boolean onNavigationItemSelected(MenuItem menuItem) {
        boolean fragmentTransaction;
        Fragment fragment = null;
        switch (menuItem.getItemId()) {
            case R.id.menu_mis_cultivos:
                fragment = new FragmentCultivosRegistrados();
                fragmentTransaction = true;
                break;
            case R.id.menu_estadisticas:
                fragment = new FragmentEstadisticasGlobales();
                fragmentTransaction = true;
                break;
            case R.id.menu_ajustes:
                fragment = new FragmentAjustes();
                fragmentTransaction = true;
                break;
            default:
                fragmentTransaction = false;
        }
        // En caso que se genere el fragment se lo muestra en el frame del menu principal
        if (fragmentTransaction) {
            getSupportFragmentManager().beginTransaction()
                    .replace(R.id.frame_fragment_principal, fragment)
                    .commit();
            // Marcamos el check de la opcion del menu
            menuItem.setChecked(true);
        }
        // Ocultamos el navigation view
        drawerLayout.closeDrawers();

        return true;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        super.onCreateOptionsMenu(menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        return SeleccionOpcionMenu(item);
    }

    /**
     * Metodo que se llamara al seleccionar un item de la action bar.
     *
     * @param item seleccionado de la action bar
     * @return true si se selecciona un item, sino false
     */
    private boolean SeleccionOpcionMenu(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                // En caso que este habilitado y no este mostrado el navigation view lo mostramos
                if (drawerLayout.isEnabled() && !navView.isShown()) {
                    drawerLayout.openDrawer(GravityCompat.START);
                } else {
                    drawerLayout.closeDrawers();
                }
                return true;
        }
        return false;
    }
}