package org.escoladeltreball.smartgarden.activities;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;

import org.escoladeltreball.smartgarden.R;
import org.escoladeltreball.smartgarden.utils.Utils;


/**
 * Activity que se cargará en el primer inicio de la aplicación después de la PantallaPrincipal.
 *
 * @author Carlos DaPalma Corchado <cdapalmac@gmail.com>
 * @author Esdras Valiente Lanaspa <esdras.vl85@gmail.com>
 * @author Andres Jordan Zambrano <andresjordanz@hotmail.com>
 *
 * This is free software, licensed under the GNU General Public License v3.
 * See http://www.gnu.org/licenses/gpl.html for more information.
 */
public class PantallaBienvenida extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        // Inflamos el layout de la activity de pantalla de bienvenida
        setContentView(R.layout.activity_pantalla_bienvenida);
        //Asignamos nuestra action bar pesonalizada
        Toolbar toolbar = (Toolbar) findViewById(R.id.actionbar);
        setSupportActionBar(toolbar);
        // Ocultamos la action bar antes de mostrar el FragmentBienvenida
        Utils.obtenerActionBar(this).hide();

    }


}
