package org.escoladeltreball.smartgarden.activities;

import android.app.Activity;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.text.method.ScrollingMovementMethod;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Checkable;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.NumberPicker;
import android.widget.Switch;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.escoladeltreball.smartgarden.R;
import org.escoladeltreball.smartgarden.pojo.Cultivo;
import org.escoladeltreball.smartgarden.pojo.Funcion;
import org.escoladeltreball.smartgarden.utils.LimitarLineasEditText;
import org.escoladeltreball.smartgarden.utils.ObtenerDatosJSON;
import org.escoladeltreball.smartgarden.utils.Utils;

import java.util.ArrayList;
import java.util.List;

/**
 * Activity que se cargará al pulsar un item de la lista de FragmentBusquedaCultivos o un item
 * de la action bar del FragmentCultivosRegistrados.
 *
 * @author Carlos DaPalma Corchado <cdapalmac@gmail.com>
 * @author Esdras Valiente Lanaspa <esdras.vl85@gmail.com>
 * @author Andres Jordan Zambrano <andresjordanz@hotmail.com>
 *         <p/>
 *         This is free software, licensed under the GNU General Public License v3.
 *         See http://www.gnu.org/licenses/gpl.html for more information.
 */
public class PantallaRegistroCultivo extends AppCompatActivity implements View.OnClickListener,
        CompoundButton.OnCheckedChangeListener, NumberPicker.OnValueChangeListener {
    // Views del layout del registro
    private ImageButton buttonLuz, buttonRegar, buttonCubierta;
    private ImageView imgCancelarLuz, imgCancelarRiego, imgCancelarCubierta;
    private NumberPicker numberPickerMinTemperatura, numberPickerMaxTemperatura, numberPickerTemperaturaIdonea;
    private NumberPicker numberPickerMinHumedad, numberPickerMaxHumedad, numberPickerHumedadIdonea;
    private EditText etNombre, etDescripcion;
    private boolean quiereNotificaciones;
    private ImageView buttonInfoFuncNotificaciones;
    private ImageView buttonInfoFuncAutomaticas;
    private Switch activaNotificaciones;
    // Bundle que se recibe FragmentBusquedaCultivos o FragmentCultivosRegistrados
    private Bundle extras;
    private Cultivo cultivo;
    private String estado;
    private Funcion funcionLuz;
    private Funcion funcionRiego;
    private Funcion funcionCubierta;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pantalla_registro_cultivo);
        // Titulo pantalla activity
        setTitle(getString(R.string.activitypantallaregistrocultivo_titulo_activity_pantalla_registro_cultivo));
        // Recuperamos los datos que nos envia el intent de FragmentBusquedaCultivos o FragmentCultivosRegistrados
        extras = getIntent().getExtras();
        // Inflamos el etNombre del layout
        etNombre = (EditText) findViewById(R.id.etNombreDelCultivo);
        // Inflamos el etDescripcion del layout
        etDescripcion = (EditText) findViewById(R.id.etDescripcionDelCultivo);
        // Limitamos el número de lineas a 4
        etDescripcion.addTextChangedListener(new LimitarLineasEditText(etDescripcion, 7));
        // Inflamos el Switch
        activaNotificaciones = (Switch) findViewById(R.id.swNotificaciones);
        // Ponemos el listener para hacer click en el switch
        activaNotificaciones.setOnClickListener(this);
        // Ponemos el listener para arrastrar el Switch
        activaNotificaciones.setOnCheckedChangeListener(this);

        // Inflamos del layout los iconos de cancelar, lo pintamos en escala de grises y le ponemos listener
        imgCancelarLuz = (ImageView) findViewById(R.id.cancelarLuz);
        Utils.imgViewPintarImagen(R.color.color_negro, imgCancelarLuz);
        imgCancelarLuz.setOnClickListener(this);
        imgCancelarRiego = (ImageView) findViewById(R.id.cancelarRiego);
        Utils.imgViewPintarImagen(R.color.color_negro, imgCancelarRiego);
        imgCancelarRiego.setOnClickListener(this);
        imgCancelarCubierta = (ImageView) findViewById(R.id.cancelarCubierta);
        Utils.imgViewPintarImagen(R.color.color_negro, imgCancelarCubierta);
        imgCancelarCubierta.setOnClickListener(this);

        // Inflamos los numberPickers y le ponemos a todos un mínimo y un máximo, ademas del listener
        numberPickerMinTemperatura = (NumberPicker) findViewById(R.id.npMinimoTemperatura);
        configurarNumberPicker(numberPickerMinTemperatura);
        numberPickerMaxTemperatura = (NumberPicker) findViewById(R.id.npMaximoTemperatura);
        configurarNumberPicker(numberPickerMaxTemperatura);
        numberPickerTemperaturaIdonea = (NumberPicker) findViewById(R.id.npTemperaturaIdonea);
        configurarNumberPicker(numberPickerTemperaturaIdonea);
        numberPickerMinHumedad = (NumberPicker) findViewById(R.id.npMinimoHumedad);
        configurarNumberPicker(numberPickerMinHumedad);
        numberPickerMaxHumedad = (NumberPicker) findViewById(R.id.npMaximoHumedad);
        configurarNumberPicker(numberPickerMaxHumedad);
        numberPickerHumedadIdonea = (NumberPicker) findViewById(R.id.npHumedadIdonea);
        configurarNumberPicker(numberPickerHumedadIdonea);
        // Desactivamos todos los numberPickers de inicio
        activadoDesactivado(false);

        // Inflamos el imageButton de información en las notificaciones y mostramos la información
        buttonInfoFuncNotificaciones = (ImageView) findViewById(R.id.imgViewInfoFuncNotificaciones);
        buttonInfoFuncNotificaciones.setOnClickListener(this);
        // Inflamos el imageButton de información en las funciones automáticas y mostramos la información
        buttonInfoFuncAutomaticas = (ImageView) findViewById(R.id.imgViewInfoFuncAutomaticas);
        buttonInfoFuncAutomaticas.setOnClickListener(this);

        // Recuperamos las views y le añadimos el escuchador
        buttonLuz = (ImageButton) findViewById(R.id.luz);
        buttonLuz.setOnClickListener(this);
        buttonCubierta = (ImageButton) findViewById(R.id.cubierta);
        buttonCubierta.setOnClickListener(this);
        buttonRegar = (ImageButton) findViewById(R.id.riego);
        buttonRegar.setOnClickListener(this);

        // Le asignamos que sea movible
        ((EditText) findViewById(R.id.etDescripcionDelCultivo)).setMovementMethod(new ScrollingMovementMethod());
        // Creamos el scroll dentro de la scroll view del texto descripcion
        findViewById(R.id.etDescripcionDelCultivo).setOnTouchListener(scrollTextoDescripcion);

        // En caso de que el cultivo no sea nuevo recogemos los
        // datos de la base de datos y los añadimos a la vista
        completarCamposDesdeBaseDeDatos();
    }

    /**
     * Metodo que usaremos para hacer scroll en el editText de la descripción.
     */
    View.OnTouchListener scrollTextoDescripcion = new View.OnTouchListener() {
        @Override
        public boolean onTouch(View v, MotionEvent event) {
            if (v.getId() == R.id.etDescripcionDelCultivo) {
                v.getParent().requestDisallowInterceptTouchEvent(true);
                if ((event.getAction() & MotionEvent.ACTION_MASK) == MotionEvent.ACTION_UP) {
                    v.getParent().requestDisallowInterceptTouchEvent(false);
                }
            }
            return false;
        }
    };


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        super.onCreateOptionsMenu(menu);
        // Mostramos el ic_guardar
        crearItemMenu(menu, 0, getString(R.string.activitypantallaregistrocultivo_action_bar_guardar), R.drawable.ic_guardar);
        // Mostramos el ic_cancelar
        crearItemMenu(menu, 1, getString(R.string.activitypantallaregistrocultivo_action_bar_cancelar), R.drawable.ic_cancelar);
        return true;
    }

    /**
     * Método que crea un item en la ActionBAr
     *
     * @param menu        menú de la ActionBar
     * @param posicion    posición del item en la ActionBar
     * @param descripcion descripción del item
     * @param icono       icono que mostraremos
     */
    private MenuItem crearItemMenu(Menu menu, int posicion, String descripcion, int icono) {
        // Creamos el item
        MenuItem menuItem = menu.add(posicion, posicion, posicion, descripcion);
        // Añadimos el icono
        menuItem.setIcon(icono);
        // Mostramos el item como una acción
        menuItem.setShowAsAction(MenuItem.SHOW_AS_ACTION_IF_ROOM | MenuItem.SHOW_AS_ACTION_WITH_TEXT);
        return menuItem;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case 0:
                if (Utils.tieneConexion(this)) {
                    // Añadimos el cultivo
                    guardarCultivo();
                } else {
                    Snackbar snackbar = Snackbar.make(findViewById(android.R.id.content),
                            getString(R.string.activitypantallaregistrocultivo_texto_sin_conexion), Snackbar.LENGTH_LONG);
                    Utils.snackbarTextoVerde(PantallaRegistroCultivo.this, snackbar);
                    snackbar.show();
                }
                return true;
            case 1:
                // Terminamos la activity
                finish();
                return true;
        }
        return false;
    }


    @Override
    public void onCheckedChanged(CompoundButton compoundButton, boolean isChecked) {
        activadoDesactivado(isChecked);
        quiereNotificaciones = isChecked;
    }

    @Override
    public void onClick(View view) {
        Intent intent = new Intent(this, PantallaAnadirTiempos.class);
        if (view == buttonLuz) {
            // Enviamos a la PantallaGestionTiempos una "L" para que sepa que esta activada
            extras.putString("tipo", "L");
            extras.putParcelable("funcion", funcionLuz);
            intent.putExtras(extras);
            startActivityForResult(intent, 1);
        } else if (view == buttonRegar) {
            // Enviamos a la PantallaGestionTiempos una "R" para que sepa que esta activada
            extras.putString("tipo", "R");
            extras.putParcelable("funcion", funcionRiego);
            intent.putExtras(extras);
            startActivityForResult(intent, 1);
        } else if (view == buttonCubierta) {
            // Enviamos a la PantallaGestionTiempos una "R" para que sepa que esta activada
            extras.putString("tipo", "C");
            extras.putParcelable("funcion", funcionCubierta);
            intent.putExtras(extras);
            startActivityForResult(intent, 1);
        } else if (view == imgCancelarLuz) {
            copiaEstadosYDeshacer(false, funcionLuz);
            funcionLuz.setEstado("N");
            mostrarXFunciones(imgCancelarLuz, funcionLuz, "L");
            final Snackbar snackbar = Snackbar.make(findViewById(android.R.id.content),
                    getString(R.string.pantallaregistrocultivo_snackbarimgcancelarluz_texto),
                    Snackbar.LENGTH_LONG);
            snackbar.setAction(getString(R.string.pantallaregistrocultivo_snackbarimgcancelarluz_action), new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    copiaEstadosYDeshacer(true, funcionLuz);
                    mostrarXFunciones(imgCancelarLuz, funcionLuz, "L");
                    snackbar.dismiss();
                }
            });
            Utils.snackbarTextoVerde(PantallaRegistroCultivo.this, snackbar);
            snackbar.show();
        } else if (view == imgCancelarRiego) {
            copiaEstadosYDeshacer(false, funcionRiego);
            funcionRiego.setEstado("N");
            mostrarXFunciones(imgCancelarRiego, funcionRiego, "R");
            final Snackbar snackbar = Snackbar.make(findViewById(android.R.id.content),
                    getString(R.string.pantallaregistrocultivo_snackbarimgcancelarRiego_texto),
                    Snackbar.LENGTH_LONG);
            snackbar.setAction(getString(R.string.pantallaregistrocultivo_snackbarimgcancelarluz_action), new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    copiaEstadosYDeshacer(true, funcionRiego);
                    mostrarXFunciones(imgCancelarRiego, funcionRiego, "R");
                    snackbar.dismiss();
                }
            });
            Utils.snackbarTextoVerde(PantallaRegistroCultivo.this, snackbar);
            snackbar.show();
        } else if (view == imgCancelarCubierta) {
            copiaEstadosYDeshacer(false, funcionCubierta);
            funcionCubierta.setEstado("N");
            mostrarXFunciones(imgCancelarCubierta, funcionCubierta, "C");
            final Snackbar snackbar = Snackbar.make(findViewById(android.R.id.content),
                    getString(R.string.pantallaregistrocultivo_snackbarimgcancelarcubierta_texto),
                    Snackbar.LENGTH_LONG);
            snackbar.setAction(getString(R.string.pantallaregistrocultivo_snackbarimgcancelarluz_action), new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    copiaEstadosYDeshacer(true, funcionCubierta);
                    mostrarXFunciones(imgCancelarCubierta, funcionCubierta, "C");
                    snackbar.dismiss();
                }
            });
            Utils.snackbarTextoVerde(PantallaRegistroCultivo.this, snackbar);
            snackbar.show();
        } else if (view == buttonInfoFuncNotificaciones) {
            Utils.mostrarAlertDialog(PantallaRegistroCultivo.this,
                    getString(R.string.activitypantallaregistrocultivo_info_titulo),
                    getString(R.string.activitypantallaregistrocultivo_info_notificaciones),
                    getString(R.string.alerta_dialog_aceptar), "", false);
        } else if (view == buttonInfoFuncAutomaticas) {
            Utils.mostrarAlertDialog(PantallaRegistroCultivo.this,
                    getString(R.string.activitypantallaregistrocultivo_info_titulo),
                    getString(R.string.activitypantallaregistrocultivo_info_funciones_automaticas),
                    getString(R.string.alerta_dialog_aceptar), "", false);
        } else if (((Checkable) view).isChecked()) {
            activadoDesactivado(true);
        } else {
            activadoDesactivado(false);
        }
    }

    /**
     * Método que copia los estado para poder deshacer la desactivación.
     *
     * @param deshacer boolean que indica que se quiere recuperar los estado anteriores
     * @param funcion  las funciones que queremos guardar
     */
    private void copiaEstadosYDeshacer(boolean deshacer, Funcion funcion) {
        // En caso de querer recuperar el estado anteriores
        if (deshacer) {
            // Setteamos la fucnion con el estado guardado
            funcion.setEstado(estado);
        } else {
            // Guardamos el estado de la función
            estado = funcion.getEstado();
        }
    }

    /**
     * Metodo que usaremos para cuando cambia el valor del time picker.
     *
     * @param numberPicker que se ha seleccionado
     * @param antiguoValor el valor anterior del time picker
     * @param nuevoValor   el nuevo valor asignado del time picker
     */
    @Override
    public void onValueChange(NumberPicker numberPicker, int antiguoValor, int nuevoValor) {
        controlarValoresNumberPicker(numberPickerMinTemperatura, numberPickerTemperaturaIdonea, numberPickerMaxTemperatura);
        controlarValoresNumberPicker(numberPickerMinHumedad, numberPickerHumedadIdonea, numberPickerMaxHumedad);
    }

    /**
     * Este metodo activa o desactiva todos los numberPickers a la vez
     *
     * @param estado el estado que queremos poner en los numberPickers
     */
    private void activadoDesactivado(boolean estado) {
        numberPickerMinTemperatura.setEnabled(estado);
        numberPickerMaxTemperatura.setEnabled(estado);
        numberPickerTemperaturaIdonea.setEnabled(estado);
        numberPickerMinHumedad.setEnabled(estado);
        numberPickerMaxHumedad.setEnabled(estado);
        numberPickerHumedadIdonea.setEnabled(estado);
    }

    /**
     * Método para cargar un cultivo desde la base de datos.
     */
    private void completarCamposDesdeBaseDeDatos() {
        cultivo = extras.getParcelable("cultivo");
        assert cultivo != null;
        etNombre.setText(cultivo.getNombre());
        etDescripcion.setText(cultivo.getDescripcion());
        activaNotificaciones.setChecked(cultivo.getNotificar().equals("S"));
        numberPickerTemperaturaIdonea.setValue(Integer.parseInt(cultivo.getTemperatura()));
        numberPickerMaxTemperatura.setValue(Integer.parseInt(cultivo.getTemperaturaMaxima()));
        numberPickerMinTemperatura.setValue(Integer.parseInt(cultivo.getTemperaturaMinima()));
        numberPickerHumedadIdonea.setValue(Integer.parseInt(cultivo.getHumedad()));
        numberPickerMaxHumedad.setValue(Integer.parseInt(cultivo.getHumedadMaxima()));
        numberPickerMinHumedad.setValue(Integer.parseInt(cultivo.getHumedadMinima()));
        funcionLuz = new Funcion(cultivo.getFuncionLuz(), cultivo.getHoraInicioLuz(), cultivo.getHoraFinLuz());
        funcionRiego = new Funcion(cultivo.getFuncionRiego(), cultivo.getHoraInicioRiego(), cultivo.getHoraFinRiego());
        funcionCubierta = new Funcion(cultivo.getFuncionCubierta(), cultivo.getHoraInicioCubierta(), cultivo.getHoraFinCubierta());
        mostrarXFunciones(imgCancelarLuz, funcionLuz, "L");
        mostrarXFunciones(imgCancelarRiego, funcionRiego, "R");
        mostrarXFunciones(imgCancelarCubierta, funcionCubierta, "C");
    }

    /**
     * Metodo que usaremos para añadir un cultivo.
     *
     * @return true si se añade correctamente o false si no se puede añadir.
     */
    private boolean guardarCultivo() {
        // Comprobamos que el nombre del cultivo no este vacío
        if (!etNombre.getText().toString().isEmpty()) {
            cultivo = new Cultivo(
                    cultivo.getId_cultivo(), cultivo.getId_usuario(),
                    etNombre.getText().toString(), etDescripcion.getText().toString(),
                    quiereNotificaciones ? "S" : "N", numberPickerTemperaturaIdonea.getValue() + "",
                    numberPickerMaxTemperatura.getValue() + "", numberPickerMinTemperatura.getValue() + "",
                    numberPickerHumedadIdonea.getValue() + "", numberPickerMaxHumedad.getValue() + "",
                    numberPickerMinHumedad.getValue() + "", funcionLuz.getEstado(),
                    funcionLuz.getHoraInicio(), funcionLuz.getHoraFin(),
                    funcionRiego.getEstado(), funcionRiego.getHoraInicio(),
                    funcionRiego.getHoraFin(), funcionCubierta.getEstado(),
                    funcionCubierta.getHoraInicio(), funcionCubierta.getHoraFin());
            // Iniciamos el hilo de acceso al servidor
            new ActualizarCultivo().execute();
        } else {
            // Si el nombre del cultivo esta vacío mostramos un mensaje
            Snackbar snackbar = Snackbar.make(findViewById(android.R.id.content),
                    getString(R.string.pantallaregistrocultivo_snackbar_nohaynombrecultivo), Snackbar.LENGTH_LONG);
            Utils.snackbarTextoVerde(PantallaRegistroCultivo.this, snackbar);
            snackbar.show();
            return false;
        }
        return true;
    }

    /**
     * Método que muestra las funciones.
     *
     * @param imageView la imagen que queremos ocultar
     * @param funcion   la fucnion que queremos modificar
     */
    private void mostrarXFunciones(ImageView imageView, Funcion funcion, String tipo) {
        // Si el estado de la funcion es true activamos la x del button de la funcion
        if (funcion.getEstado().equals("S")) {
            switch (tipo) {
                case "L":
                    imageView.setVisibility(View.VISIBLE);
                    buttonLuz.setImageDrawable(ContextCompat.getDrawable(this, R.drawable.ic_luz_encendida));
                    break;
                case "R":
                    imageView.setVisibility(View.VISIBLE);
                    buttonRegar.setImageDrawable(ContextCompat.getDrawable(this, R.drawable.ic_riego_encendido));
                    break;
                default:
                    imageView.setVisibility(View.VISIBLE);
                    buttonCubierta.setImageDrawable(ContextCompat.getDrawable(this, R.drawable.ic_cubierta_abierta));
                    break;
            }
        } else {
            // Si la funcion no esta activa escondemos la x del button de la funcion
            switch (tipo) {
                case "L":
                    imageView.setVisibility(View.GONE);
                    buttonLuz.setImageDrawable(ContextCompat.getDrawable(this, R.drawable.ic_luz_apagada));
                    break;
                case "R":
                    imageView.setVisibility(View.GONE);
                    buttonRegar.setImageDrawable(ContextCompat.getDrawable(this, R.drawable.ic_riego_apagado));
                    break;
                default:
                    imageView.setVisibility(View.GONE);
                    buttonCubierta.setImageDrawable(ContextCompat.getDrawable(this, R.drawable.ic_cubierta_cerrada));
                    break;
            }
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        // Si obtenemos una respuesta significa que la funcion se ha modificado
        if (resultCode == Activity.RESULT_OK && requestCode == 1) {
            // Guardamos la funcion de la respuesta del intent
            Funcion funcionData = data.getExtras().getParcelable("funcion");
            // Guardamos el tipo
            String tipo = data.getExtras().getString("tipo");
            // Si la funcion y el tipo de data no es null
            if (funcionData != null && tipo != null) {
                // Añadimos la nueva funcion que nos envia la PantallaAnadirTiempos
                switch (tipo) {
                    case "L":
                        // Creamos la funcion luz con los datos del intent
                        funcionLuz = new Funcion(funcionData.getEstado(), funcionData.getHoraInicio(), funcionData.getHoraFin());
                        mostrarXFunciones(imgCancelarLuz, funcionLuz, tipo);
                        break;
                    case "R":
                        // Creamos la funcion riego con los datos del intent
                        funcionRiego = new Funcion(funcionData.getEstado(), funcionData.getHoraInicio(), funcionData.getHoraFin());
                        mostrarXFunciones(imgCancelarRiego, funcionRiego, tipo);
                        break;
                    default:
                        // Creamos la funcion cubierta con los datos del intent
                        funcionCubierta = new Funcion(funcionData.getEstado(), funcionData.getHoraInicio(), funcionData.getHoraFin());
                        mostrarXFunciones(imgCancelarCubierta, funcionCubierta, tipo);
                        break;
                }
            }
        }
    }

    /**
     * Método que controlará que los valores de los numberPicker no sean mas pequeños que el anterior.
     *
     * @param numberPickerMin   el valor de la humedad o temperatura mínima.
     * @param numberPickerMedio el valor de la humedad o temperatura ideal.
     * @param numberPickerMax   el valor de la humedad o temperatura máxima
     */
    public void controlarValoresNumberPicker(NumberPicker numberPickerMin, NumberPicker numberPickerMedio, NumberPicker numberPickerMax) {
        // Miramos que el del medio no se mas pequeño que el del minimo,
        // que el de máximo no sea mas pequeño que de mínimo
        // y que el de máximo no sea mas pequeño que el del medio
        if (numberPickerMedio.getValue() <= numberPickerMin.getValue()
                || numberPickerMax.getValue() <= numberPickerMin.getValue()
                || numberPickerMax.getValue() <= numberPickerMedio.getValue()) {

            // establecemos de valor del mínimo  el valor del del medio - 1
            numberPickerMin.setValue(numberPickerMedio.getValue() - 1);
            // establecemos de valor del máximo  el valor del del medio + 1
            numberPickerMax.setValue(numberPickerMedio.getValue() + 1);

            // Si el valor de el del medio es 0, los ponemos todos a 0
            if (numberPickerMedio.getValue() == 0) {
                numberPickerMin.setValue(0);
                numberPickerMax.setValue(0);
                // Si el valor de el del medio es 60, los ponemos todos a 60
            } else if (numberPickerMedio.getValue() == 60) {
                numberPickerMin.setValue(60);
                numberPickerMax.setValue(60);
            }
        }
    }

    /**
     * Hilo que usaremos para conectar al servidor y actualizar el cultivo
     */
    private class ActualizarCultivo extends AsyncTask<Void, Void, String> {

        @Override
        protected String doInBackground(Void... arg0) {
            if (actualizarServidor() != null && programarAccionCultivo() != null) {
                return "CORRECTO";
            }
            return null;
        }

        @Override
        protected void onPostExecute(String resultado) {
            super.onPostExecute(resultado);
            if (resultado != null && !resultado.equals("N")) {
                // Si es la primera vez que añadimos un cultivo lo indicamos para que no
                // nos muestre la bienvenida
                Utils.registroPrimerCultivo(PantallaRegistroCultivo.this);
                Intent intent = new Intent();
                extras.putParcelable("cultivo", cultivo);
                intent.putExtras(extras);
                // Indicamos que se ha guardado el cultivo
                setResult(Activity.RESULT_OK, intent);
                // Terminamos la activity
                finish();
            } else {
                Snackbar snackbar = Snackbar.make(findViewById(android.R.id.content),
                        getString(R.string.activitypantallaregistrocultivo_texto_fallo_guardar_cultivo), Snackbar.LENGTH_LONG);
                Utils.snackbarTextoVerde(PantallaRegistroCultivo.this, snackbar);
                snackbar.show();
            }
        }

        /**
         * Metodo que usaremos para actualizar los datos en el servidor.
         *
         * @return diferente de null si va correctamente
         */
        private String actualizarServidor() {
            List<NameValuePair> parametros = new ArrayList<>();
            // Tipo de función en el servidor
            parametros.add(new BasicNameValuePair("tipo", "1"));
            parametros.add(new BasicNameValuePair("idCultivo", cultivo.getId_cultivo()));
            parametros.add(new BasicNameValuePair("idUsuario", Build.SERIAL));
            parametros.add(new BasicNameValuePair("nombre", cultivo.getNombre()));
            parametros.add(new BasicNameValuePair("descripcion", cultivo.getDescripcion()));
            parametros.add(new BasicNameValuePair("notificar", cultivo.getNotificar()));
            // Temperatura
            parametros.add(new BasicNameValuePair("temperatura", cultivo.getTemperatura()));
            parametros.add(new BasicNameValuePair("temperaturaMax", cultivo.getTemperaturaMaxima()));
            parametros.add(new BasicNameValuePair("temperaturaMin", cultivo.getTemperaturaMinima()));
            // Humedad
            parametros.add(new BasicNameValuePair("humedad", cultivo.getHumedad()));
            parametros.add(new BasicNameValuePair("humedadMax", cultivo.getHumedadMaxima()));
            parametros.add(new BasicNameValuePair("humedadMin", cultivo.getHumedadMinima()));
            // Función luz
            parametros.add(new BasicNameValuePair("funcionLuz", funcionLuz.getEstado()));
            parametros.add(new BasicNameValuePair("horaInicioLuz", funcionLuz.getHoraInicio()));
            parametros.add(new BasicNameValuePair("horaFinLuz", funcionLuz.getHoraFin()));
            // Función riego
            parametros.add(new BasicNameValuePair("funcionRiego", funcionRiego.getEstado()));
            parametros.add(new BasicNameValuePair("horaInicioRiego", funcionRiego.getHoraInicio()));
            parametros.add(new BasicNameValuePair("horaFinRiego", funcionRiego.getHoraFin()));
            // Función cubierta
            parametros.add(new BasicNameValuePair("funcionCubierta", funcionCubierta.getEstado()));
            parametros.add(new BasicNameValuePair("horaInicioCubierta", funcionCubierta.getHoraInicio()));
            parametros.add(new BasicNameValuePair("horaFinCubierta", funcionCubierta.getHoraFin()));
            // Creamos una instancia de ObtenerDatosJSON
            ObtenerDatosJSON datosJSON = new ObtenerDatosJSON();
            // Hacemos la solicitud de obtener el documento JSON, en este caso un POST
            return datosJSON.obtenerDocumentoJSON(Utils.URL_ACTUALIZAR_CULTIVO, ObtenerDatosJSON.POST, parametros);
        }

        /**
         * Metodo que usaremos para programar las funciones automáticas.
         *
         * @return diferente de null si va correctamente
         */
        private String programarAccionCultivo() {
            // Iniciamos las distintas programaciones
            String[] luzRiegoCubierta = {"4", "5", "6"};
            for (int i = 0; i <= 2; i++) {
                // Creamos la lista de parametros
                List<NameValuePair> parametros = new ArrayList<>();
                // Programamos la accion en el servidor
                String programarAccionCultivo = "";
                programarAccionCultivo += "P" + luzRiegoCubierta[i];
                if (luzRiegoCubierta[i].equals("4")) {
                    programarAccionCultivo += programarFuncionAccionCultivo(funcionLuz);
                } else if (luzRiegoCubierta[i].equals("5")) {
                    programarAccionCultivo += programarFuncionAccionCultivo(funcionRiego);
                } else if (luzRiegoCubierta[i].equals("6")) {
                    programarAccionCultivo += programarFuncionAccionCultivo(funcionCubierta);
                }
                // Añadimos el parametro
                parametros.add(new BasicNameValuePair("accionCultivo", programarAccionCultivo));
                // Creamos una instancia de ObtenerDatosJSON
                ObtenerDatosJSON datosJSON = new ObtenerDatosJSON();
                // Hacemos la solicitud de obtener el documento JSON, en este caso un POST
                String resultado = datosJSON.obtenerDocumentoJSON(Utils.URL_ACCION_CULTIVO, ObtenerDatosJSON.POST, parametros);
                if (resultado == null) {
                    // Si alguna ejecucion no se hace bien se retorna null
                    return null;
                }
            }
            return "CORRECTO";
        }

        /**
         * Metodo que usaremos para programar la activacion y duracion de una funcion automática.
         *
         * @param funcion que se desea programar
         * @return la cadena de la funcion programada
         */
        private String programarFuncionAccionCultivo(Funcion funcion) {
            String resultado = "";
            // Comprobamos si ha de estar activada o desactivada
            if (funcion.getEstado().equals("S")) {
                resultado += "1";
            } else {
                resultado += "0";
            }
            // Añadimos las horas tanto de inicio como de fin
            resultado += funcion.getHoraInicio().replace(":", "");
            resultado += funcion.getHoraFin().replace(":", "");
            return resultado;
        }
    }

    private void configurarNumberPicker(NumberPicker numberPicker) {
        numberPicker.setMinValue(0);
        numberPicker.setMaxValue(100);
        numberPicker.setWrapSelectorWheel(true);
        numberPicker.setOnValueChangedListener(this);
    }
}