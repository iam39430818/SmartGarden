package org.escoladeltreball.smartgarden.pojo;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.Arrays;

/**
 * Clase POJO de Registro.
 *
 * @author Carlos DaPalma Corchado <cdapalmac@gmail.com>
 * @author Esdras Valiente Lanaspa <esdras.vl85@gmail.com>
 * @author Andres Jordan Zambrano <andresjordanz@hotmail.com>
 *         <p/>
 *         This is free software, licensed under the GNU General Public License v3.
 *         See http://www.gnu.org/licenses/gpl.html for more information.
 */
public class Registro implements Parcelable {
    // Id de estadisticas
    private String idRegistro;
    // Id del usuario
    private String idUsuario;
    // Id del cultivo
    private String idCultivo;
    // Nombre del cultivo
    private String nombre;
    // mes de las estadísticas
    private String mes;
    // Consumo electrico
    private String consumo;
    // Temperatura del cultivo
    private String temperatura;
    // Humedad del cultivo
    private String humedad;
    // Array de int que guarda los valores de todos los meses
    private float[] arrayMesesConsumo;
    private float[] arrayMesesTemperatura;
    private float[] arrayMesesHumedad;

    /**
     * Constructor de la clase
     */
    public Registro() {
        arrayMesesConsumo = new float[12];
        arrayMesesTemperatura = new float[12];
        arrayMesesHumedad = new float[12];
    }

    /**
     * Constructor de la clase
     *
     * @param idRegistro  idRegistro de estadisticas
     * @param idUsuario   idUsuario  de usuario
     * @param idCultivo   idRegistro del cultivo
     * @param mes         mes de la estadistica
     * @param consumo     consumo electrico
     * @param temperatura temperatura de las estadistcas
     * @param humedad     humedad de las estadistcas
     */
    public Registro(String idRegistro, String idUsuario, String idCultivo, String mes, String consumo, String temperatura, String humedad) {
        this.idRegistro = idRegistro;
        this.idUsuario = idUsuario;
        this.idCultivo = idCultivo;
        this.mes = mes;
        this.consumo = consumo;
        this.temperatura = temperatura;
        this.humedad = humedad;
    }


    /**
     * Constructor de la clase
     *
     * @param idCultivo   identificador del cultivo
     * @param nombre      nombre del cultivo
     * @param mes         mes de la estadistica
     * @param consumo     consumo electrico
     * @param temperatura temperatura de las estadistcas
     * @param humedad     humedad de las estadistcas
     */
    public Registro(String idCultivo, String nombre, String mes, String consumo, String temperatura, String humedad) {
        this.idCultivo = idCultivo;
        this.nombre = nombre;
        this.mes = mes;
        this.consumo = consumo;
        this.temperatura = temperatura;
        this.humedad = humedad;
    }

    /**
     * Getter de idRegistro
     *
     * @return el idRegistro
     */
    public String getIdRegistro() {
        return idRegistro;
    }

    /**
     * Setter de idRegistro
     *
     * @param idRegistro de estadisticas
     */
    public void setIdRegistro(String idRegistro) {
        this.idRegistro = idRegistro;
    }

    /**
     * Getter de idUsuario
     *
     * @return el idUsuario
     */
    public String getIdUsuario() {
        return idUsuario;
    }

    /**
     * Setter de idUsuario
     *
     * @param idUsuario de estadisticas
     */
    public void setIdUsuario(String idUsuario) {
        this.idUsuario = idUsuario;
    }

    /**
     * Getter de idCultivo del cultivo
     *
     * @return el idCultivo del cultivo
     */
    public String getIdCultivo() {
        return idCultivo;
    }

    /**
     * Setter de idCultivo del cultivo
     *
     * @param idCultivo idCultivo del cultivo del mes
     */
    public void setIdCultivo(String idCultivo) {
        this.idCultivo = idCultivo;
    }

    /**
     * Getter del mes
     *
     * @return el mes
     */
    public String getMes() {
        return mes;
    }

    /**
     * Setter del mes
     *
     * @param mes de las estadisticas
     */
    public void setMes(String mes) {
        this.mes = mes;
    }

    /**
     * Getter del consumo
     *
     * @return el consumo
     */
    public String getConsumo() {
        return consumo;
    }

    /**
     * Setter del consumo
     *
     * @param consumo electrico
     */
    public void setConsumo(String consumo) {
        this.consumo = consumo;
    }

    /**
     * Getter de la temperatura
     *
     * @return la temperatura
     */
    public String getTemperatura() {
        return temperatura;
    }

    /**
     * Setter de la temperatura
     *
     * @param temperatura de la estadistica
     */
    public void setTemperatura(String temperatura) {
        this.temperatura = temperatura;
    }

    /**
     * Getter de la humedad
     *
     * @return la humedad de la estadistica
     */
    public String getHumedad() {
        return humedad;
    }

    /**
     * Setter de la humedad
     *
     * @param humedad de la estadistica
     */
    public void setHumedad(String humedad) {
        this.humedad = humedad;
    }

    /**
     * Getter del nombre
     *
     * @return el nombre del cultivo
     */
    public String getNombre() {
        return nombre;
    }

    /**
     * Setter del nombre
     *
     * @param nombre del cultivo
     */
    public void setNombre(String nombre) {
        this.nombre = nombre;
    }


    /**
     * Metodo que usaremos para añadir datos al registro
     *
     * @param nombre      del cultivo
     * @param mes         es la posicion en la array
     * @param consumo     valor de la array
     * @param temperatura valor de la array
     * @param humedad     valor de la array
     */
    public void anadirDatosRegistro(String nombre, int mes, float consumo, float temperatura, float humedad) {
        this.nombre = nombre;
        arrayMesesConsumo[mes] = consumo;
        arrayMesesTemperatura[mes] = temperatura;
        arrayMesesHumedad[mes] = humedad;
    }

    /**
     * Metodo que usaremos para añadir datos al registro
     *
     * @param mes         es la posicion en la array
     * @param consumo     valor de la array
     * @param temperatura valor de la array
     * @param humedad     valor de la array
     */
    public void anadirDatosRegistro(int mes, float consumo, float temperatura, float humedad) {
        arrayMesesConsumo[mes] = consumo;
        arrayMesesTemperatura[mes] = temperatura;
        arrayMesesHumedad[mes] = humedad;
    }

    /**
     * Getter de la array de meses con los consumos
     *
     * @return la array de meses con los consumos
     */
    public float getValorMesesConsumo(int i) {
        return this.arrayMesesConsumo[i];
    }

    /**
     * Setter de la array de meses con el consumo
     *
     * @param arrayMesesConsumo de la array de meses con el consumo
     */
    public void setArrayMesesConsumo(float[] arrayMesesConsumo) {
        this.arrayMesesConsumo = Arrays.copyOf(arrayMesesConsumo, arrayMesesConsumo.length);
    }

    /**
     * Getter de la array de meses con las temperaturas
     *
     * @return la array de meses con las temperaturas
     */
    public float getValorMesesTemperatura(int i) {
        return this.arrayMesesTemperatura[i];
    }

    /**
     * Setter de la array de meses con la temperatura
     *
     * @param arrayMesesTemperatura de la array de meses con la temperatura
     */
    public void setArrayMesesTemperatura(float[] arrayMesesTemperatura) {
        this.arrayMesesTemperatura = Arrays.copyOf(arrayMesesTemperatura, arrayMesesTemperatura.length);
    }

    /**
     * Getter de la array de meses con las humedades
     *
     * @return la array de meses con las humedades
     */
    public float getValorMesesHumedad(int i) {
        return this.arrayMesesHumedad[i];
    }

    /**
     * Setter de la array de meses con la humedad
     *
     * @param arrayMesesHumedad de la array de meses con la humedad
     */
    public void setArrayMesesHumedad(float[] arrayMesesHumedad) {
        this.arrayMesesHumedad = Arrays.copyOf(arrayMesesHumedad, arrayMesesHumedad.length);
    }

    protected Registro(Parcel in) {
        idRegistro = in.readString();
        idUsuario = in.readString();
        idCultivo = in.readString();
        nombre = in.readString();
        mes = in.readString();
        consumo = in.readString();
        temperatura = in.readString();
        humedad = in.readString();

    }

    public static final Parcelable.Creator<Registro> CREATOR = new Parcelable.Creator<Registro>() {
        @Override
        public Registro createFromParcel(Parcel in) {
            return new Registro(in);
        }

        @Override
        public Registro[] newArray(int size) {
            return new Registro[size];
        }
    };


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(idRegistro);
        parcel.writeString(idUsuario);
        parcel.writeString(idCultivo);
        parcel.writeString(nombre);
        parcel.writeString(mes);
        parcel.writeString(consumo);
        parcel.writeString(temperatura);
        parcel.writeString(humedad);


    }
}