package org.escoladeltreball.smartgarden.pojo;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Clase POJO de Funcion.
 *
 * @author Carlos DaPalma Corchado <cdapalmac@gmail.com>
 * @author Esdras Valiente Lanaspa <esdras.vl85@gmail.com>
 * @author Andres Jordan Zambrano <andresjordanz@hotmail.com>
 *         <p/>
 *         This is free software, licensed under the GNU General Public License v3.
 *         See http://www.gnu.org/licenses/gpl.html for more information.
 */
public class Funcion implements Parcelable {
    // Si la función esta activa o no
    private String estado;
    // Hora de inicio de la funcion
    private String horaInicio;
    // Hora de fin de la funcion
    private String horaFin;

    /**
     * Constructor de la lase
     *
     * @param estado     estado de la función
     * @param horaInicio hora de inicio de la funcion
     * @param horaFin    hora final de la funcion
     */
    public Funcion(String estado, String horaInicio, String horaFin) {
        this.estado = estado;
        this.horaInicio = horaInicio;
        this.horaFin = horaFin;
    }

    /**
     * Getter de estado
     *
     * @return el estado de la función
     */
    public String getEstado() {
        return estado;
    }

    /**
     * Setter del estado
     *
     * @param estado de la función
     */
    public void setEstado(String estado) {
        this.estado = estado;
    }

    /**
     * Getter de la hora de inicio
     *
     * @return la hora de inicio
     */
    public String getHoraInicio() {
        return horaInicio;
    }

    /**
     * Setter de la hora de inicio
     *
     * @param horaInicio la hora de inicio de la funcion
     */
    public void setHoraInicio(String horaInicio) {
        this.horaInicio = horaInicio;
    }

    /**
     * Getter de la hora de fin
     *
     * @return la hora de fin de la funcion
     */
    public String getHoraFin() {
        return horaFin;
    }

    /**
     * Setter de la hora de fin
     *
     * @param horaFin hora de fin de la funcion
     */
    public void setHoraFin(String horaFin) {
        this.horaFin = horaFin;
    }

    @Override
    public String toString() {
        return "Funcion{" +
                ", estado='" + estado + '\'' +
                ", horaInicio='" + horaInicio + '\'' +
                ", horaFin='" + horaFin + '\'' +
                '}';
    }

    protected Funcion(Parcel in) {
        estado = in.readString();
        horaInicio = in.readString();
        horaFin = in.readString();
    }

    public static final Creator<Funcion> CREATOR = new Creator<Funcion>() {
        @Override
        public Funcion createFromParcel(Parcel in) {
            return new Funcion(in);
        }

        @Override
        public Funcion[] newArray(int size) {
            return new Funcion[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(estado);
        parcel.writeString(horaInicio);
        parcel.writeString(horaFin);
    }
}