package org.escoladeltreball.smartgarden.pojo;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Clase POJO de cultivo.
 *
 * @author Carlos DaPalma Corchado <cdapalmac@gmail.com>
 * @author Esdras Valiente Lanaspa <esdras.vl85@gmail.com>
 * @author Andres Jordan Zambrano <andresjordanz@hotmail.com>
 *         <p/>
 *         This is free software, licensed under the GNU General Public License v3.
 *         See http://www.gnu.org/licenses/gpl.html for more information.
 */
public class Cultivo implements Parcelable {
    // Id del cultivo
    private String id_cultivo;
    // Usuario del cultivo
    private String id_usuario;
    // Nombre del cultivo
    private String nombre;
    // Descripción del cultivo
    private String descripcion;
    // Si quiere notificaciones
    private String notificar;
    // temperatura idónea del cultivo
    private String temperatura;
    // temperatura máxima permisible
    private String temperaturaMaxima;
    // temperatura mínima permisible
    private String temperaturaMinima;
    // humedad idónea del cultivo
    private String humedad;
    // humedad máxima permisible del cultivo
    private String humedadMaxima;
    // humedad mínima permisible del cultivo
    private String humedadMinima;
    // si esta activada la función de luz
    private String funcionLuz;
    // hora de inicio de la función de luz
    private String horaInicioLuz;
    // hora final de la función de luz
    private String horaFinLuz;
    // si esta activada la función de riego
    private String funcionRiego;
    // hora de inicio de la función de riego
    private String horaInicioRiego;
    // hora final de la función de riego
    private String horaFinRiego;
    // si esta activada la función de cubierta
    private String funcionCubierta;
    // hora de inicio de la función de cubierta
    private String horaInicioCubierta;
    // hora final de la función de cubierta
    private String horaFinCubierta;


    /**
     * Constructor de la clase
     *
     * @param id_cultivo         identificador del cultivo
     * @param id_usuario        ww identificador del usuario
     * @param nombre             nombre del cultivo
     * @param descripcion        descripción del cultivo
     * @param notificar          si las notificaciones estan activas
     * @param temperatura        temperatura idónea de cultivo
     * @param temperaturaMaxima  temperatura máxima permisible del cultivo
     * @param temperaturaMinima  temperatura mínima permisible del cultivo
     * @param humedad            humedad idónea del cultivo
     * @param humedadMaxima      humedad máxima permisible del cultivo
     * @param humedadMinima      humedad mínima permisible del cultivo
     * @param funcionLuz         si la acción de la luz esta activa
     * @param horaInicioLuz      hora de inicio de la función de luz
     * @param horaFinLuz         hora final de la función de luz
     * @param funcionRiego       si la acción del riego esta activa
     * @param horaInicioRiego    hora de inicio de la función de riego
     * @param horaFinRiego       hora final de la función de riego
     * @param funcionCubierta    si la acción de la cubierta esta activa
     * @param horaInicioCubierta hora de inicio de la función de cubierta
     * @param horaFinCubierta    hora final de la función de cubierta
     */
    public Cultivo(String id_cultivo, String id_usuario, String nombre, String descripcion, String notificar, String temperatura,
                   String temperaturaMaxima, String temperaturaMinima, String humedad, String humedadMaxima, String humedadMinima,
                   String funcionLuz, String horaInicioLuz, String horaFinLuz, String funcionRiego, String horaInicioRiego, String horaFinRiego,
                   String funcionCubierta, String horaInicioCubierta, String horaFinCubierta) {
        this.id_cultivo = id_cultivo;
        this.id_usuario = id_usuario;
        this.nombre = nombre;
        this.descripcion = descripcion;
        this.notificar = notificar;
        this.temperatura = temperatura;
        this.temperaturaMaxima = temperaturaMaxima;
        this.temperaturaMinima = temperaturaMinima;
        this.humedad = humedad;
        this.humedadMaxima = humedadMaxima;
        this.humedadMinima = humedadMinima;
        this.funcionLuz = funcionLuz;
        this.horaInicioLuz = horaInicioLuz;
        this.horaFinLuz = horaFinLuz;
        this.funcionRiego = funcionRiego;
        this.horaInicioRiego = horaInicioRiego;
        this.horaFinRiego = horaFinRiego;
        this.funcionCubierta = funcionCubierta;
        this.horaInicioCubierta = horaInicioCubierta;
        this.horaFinCubierta = horaFinCubierta;
    }

    /**
     * Getter de id del cultivo
     *
     * @return el id del cultivo
     */
    public String getId_cultivo() {
        return id_cultivo;
    }

    /**
     * Setter de la id del cultivo
     *
     * @param id_cultivo identificador del cultivo
     */
    public void setId_cultivo(String id_cultivo) {
        this.id_cultivo = id_cultivo;
    }


    /**
     * Getter del id del usuario del cultivo
     *
     * @return el id del usuario
     */
    public String getId_usuario() {
        return id_usuario;
    }

    /**
     * Setter de la id del usuario
     *
     * @param id_usuario nombre del cultivo
     */
    public void setId_usuario(String id_usuario) {
        this.id_usuario = id_usuario;
    }

    /**
     * Getter del nombre del cultivo
     *
     * @return el nombre del cultivo
     */
    public String getNombre() {
        return nombre;
    }

    /**
     * Setter del nombre del cultivo
     *
     * @param nombre nombre del cultivo
     */
    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    /**
     * Getter de la descripción del cultivo
     *
     * @return la descripción del cultivo
     */
    public String getDescripcion() {
        return descripcion;
    }

    /**
     * Setter de la descripción del cultivo
     *
     * @param descripcion descripción del cultivo
     */
    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    /**
     * Getter de notificaciones del cultivo
     *
     * @return si quiere notificaciones del cultivo
     */
    public String getNotificar() {
        return notificar;
    }

    /**
     * Setter de la notificación del cultivo
     *
     * @param notificar si quiere notificaciones del cultivo
     */
    public void setNotificar(String notificar) {
        this.notificar = notificar;
    }

    /**
     * Getter de la temperatura del cultivo
     *
     * @return la temperatura idónea del cultivo
     */
    public String getTemperatura() {
        return temperatura;
    }

    /**
     * Setter de la temperatura idónea del cultivo
     *
     * @param temperatura temperatura idónea del cultivo
     */
    public void setTemperatura(String temperatura) {
        this.temperatura = temperatura;
    }

    /**
     * Getter de la temperatura máxima del cultivo
     *
     * @return la temperatura máxima permisible del cultivo
     */
    public String getTemperaturaMaxima() {
        return temperaturaMaxima;
    }

    /**
     * Setter de la temperatura máxima del cultivo
     *
     * @param temperaturaMaxima temperatura máxima permisible del cultivo
     */
    public void setTemperaturaMaxima(String temperaturaMaxima) {
        this.temperaturaMaxima = temperaturaMaxima;
    }

    /**
     * Getter de la temperatura mínima del cultivo
     *
     * @return la temperatura mínima permisible del cultivo
     */
    public String getTemperaturaMinima() {
        return temperaturaMinima;
    }

    /**
     * Setter de la temperatura mínima del cultivo
     *
     * @param temperaturaMinima temperatura mínima permisible del cultivo
     */
    public void setTemperaturaMinima(String temperaturaMinima) {
        this.temperaturaMinima = temperaturaMinima;
    }

    /**
     * Getter de la humedad del cultivo
     *
     * @return la humedad idónea del cultivo
     */
    public String getHumedad() {
        return humedad;
    }

    /**
     * Setter de la humedad del cultivo
     *
     * @param humedad humedad idónea del cultivo
     */
    public void setHumedad(String humedad) {
        this.humedad = humedad;
    }

    /**
     * Getter de la humedad máxima del cultivo
     *
     * @return la humedad máxima permisible del cultivo
     */
    public String getHumedadMaxima() {
        return humedadMaxima;
    }

    /**
     * Setter de la humedad máxima permisible del cultivo
     *
     * @param humedadMaxima humedad máxima del cultivo
     */
    public void setHumedadMaxima(String humedadMaxima) {
        this.humedadMaxima = humedadMaxima;
    }

    /**
     * Getter de la humedad mínima del cultivo
     *
     * @return la humedad mínima permisible del cultivo
     */
    public String getHumedadMinima() {
        return humedadMinima;
    }

    /**
     * Setter de la humedad mínima del cultivo
     *
     * @param humedadMinima humedad mínima del cultivo
     */
    public void setHumedadMinima(String humedadMinima) {
        this.humedadMinima = humedadMinima;
    }

    /**
     * Getter de la función luz del cultivo
     *
     * @return si la función luz esta activada
     */
    public String getFuncionLuz() {
        return funcionLuz;
    }

    /**
     * Setter de la función de luz del cultivo
     *
     * @param funcionLuz si está activada la función de luz del cultivo
     */
    public void setFuncionLuz(String funcionLuz) {
        this.funcionLuz = funcionLuz;
    }

    /**
     * Getter de la hora de inicio de la función luz del cultivo
     *
     * @return hora de inicio de la función luz
     */
    public String getHoraInicioLuz() {
        return horaInicioLuz;
    }

    /**
     * Setter de la hora de inicio de la función luz del cultivo
     *
     * @param horaInicioLuz hora de inicio de la luz del cultivo
     */
    public void setHoraInicioLuz(String horaInicioLuz) {
        this.horaInicioLuz = horaInicioLuz;
    }

    /**
     * Getter de la hora final de la función luz del cultivo
     *
     * @return hora final de la función luz
     */
    public String getHoraFinLuz() {
        return horaFinLuz;
    }

    /**
     * Setter de la hora final de la función luz del cultivo
     *
     * @param horaFinLuz nombre del cultivo
     */
    public void setHoraFinLuz(String horaFinLuz) {
        this.horaFinLuz = horaFinLuz;
    }

    /**
     * Getter de la función riego del cultivo
     *
     * @return si la función riego esta activada
     */
    public String getFuncionRiego() {
        return funcionRiego;
    }

    /**
     * Setter de la función de riego del cultivo
     *
     * @param funcionRiego si está activada la función de riego del cultivo
     */
    public void setFuncionRiego(String funcionRiego) {
        this.funcionRiego = funcionRiego;
    }

    /**
     * Getter de la hora de inicio de la función riego del cultivo
     *
     * @return hora de inicio de la función riego
     */
    public String getHoraInicioRiego() {
        return horaInicioRiego;
    }

    /**
     * Setter de la hora de inicio de la función de riego del cultivo
     *
     * @param horaInicioRiego hora de inicio de la función riego del cultivo
     */
    public void setHoraInicioRiego(String horaInicioRiego) {
        this.horaInicioRiego = horaInicioRiego;
    }

    /**
     * Getter de la hora final de la función riego del cultivo
     *
     * @return hora de final de la función riego
     */
    public String getHoraFinRiego() {
        return horaFinRiego;
    }

    /**
     * Setter de la hora final de la función riego del cultivo
     *
     * @param horaFinRiego hora final de la función riego del cultivo
     */
    public void setHoraFinRiego(String horaFinRiego) {
        this.horaFinRiego = horaFinRiego;
    }

    /**
     * Getter de la función cubierta del cultivo
     *
     * @return si la función cubierta esta activada
     */
    public String getFuncionCubierta() {
        return funcionCubierta;
    }

    /**
     * Setter de la función de la cubierta del cultivo
     *
     * @param funcionCubierta si está activada la función de cubierta del cultivo
     */
    public void setFuncionCubierta(String funcionCubierta) {
        this.funcionCubierta = funcionCubierta;
    }

    /**
     * Getter de la hora de inicio de la función cubierta del cultivo
     *
     * @return hora de inicio de la función cubierta
     */
    public String getHoraInicioCubierta() {
        return horaInicioCubierta;
    }

    /**
     * Setter de la hora de inicio de la función cubierta del cultivo
     *
     * @param horaInicioCubierta hora de inicio de la función cubierta del cultivo
     */
    public void setHoraInicioCubierta(String horaInicioCubierta) {
        this.horaInicioCubierta = horaInicioCubierta;
    }

    /**
     * Getter de la hora final de la función cubierta del cultivo
     *
     * @return hora final de la función cubierta
     */
    public String getHoraFinCubierta() {
        return horaFinCubierta;
    }

    /**
     * Setter de la hora final de la función cubierta del cultivo
     *
     * @param horaFinCubierta hora final de la función cubierta del cultivo
     */
    public void setHoraFinCubierta(String horaFinCubierta) {
        this.horaFinCubierta = horaFinCubierta;
    }

    protected Cultivo(Parcel in) {
        id_cultivo = in.readString();
        id_usuario = in.readString();
        nombre = in.readString();
        descripcion = in.readString();
        notificar = in.readString();
        temperatura = in.readString();
        temperaturaMaxima = in.readString();
        temperaturaMinima = in.readString();
        humedad = in.readString();
        humedadMaxima = in.readString();
        humedadMinima = in.readString();
        funcionLuz = in.readString();
        horaInicioLuz = in.readString();
        horaFinLuz = in.readString();
        funcionRiego = in.readString();
        horaInicioRiego = in.readString();
        horaFinRiego = in.readString();
        funcionCubierta = in.readString();
        horaInicioCubierta = in.readString();
        horaFinCubierta = in.readString();
    }

    public static final Parcelable.Creator<Cultivo> CREATOR = new Parcelable.Creator<Cultivo>() {
        @Override
        public Cultivo createFromParcel(Parcel in) {
            return new Cultivo(in);
        }

        @Override
        public Cultivo[] newArray(int size) {
            return new Cultivo[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(id_cultivo);
        parcel.writeString(id_usuario);
        parcel.writeString(nombre);
        parcel.writeString(descripcion);
        parcel.writeString(notificar);
        parcel.writeString(temperatura);
        parcel.writeString(temperaturaMaxima);
        parcel.writeString(temperaturaMinima);
        parcel.writeString(humedad);
        parcel.writeString(humedadMaxima);
        parcel.writeString(humedadMinima);
        parcel.writeString(funcionLuz);
        parcel.writeString(horaInicioLuz);
        parcel.writeString(horaFinLuz);
        parcel.writeString(funcionRiego);
        parcel.writeString(horaInicioRiego);
        parcel.writeString(horaFinRiego);
        parcel.writeString(funcionCubierta);
        parcel.writeString(horaInicioCubierta);
        parcel.writeString(horaFinCubierta);
    }
}