package org.escoladeltreball.smartgarden.pojo;

/**
 * Clase POJO del historial de notificaciones.
 *
 *
 * @author Carlos DaPalma Corchado <cdapalmac@gmail.com>
 * @author Esdras Valiente Lanaspa <esdras.vl85@gmail.com>
 * @author Andres Jordan Zambrano <andresjordanz@hotmail.com>
 *
 * This is free software, licensed under the GNU General Public License v3.
 * See http://www.gnu.org/licenses/gpl.html for more information.
 */
public class HistorialNotificacion {
    // Id del registro
    private String id;
    // Id del cultivo
    private String macCultivo;
    // Mensaje de la notificación
    private String mensaje;

    /**
     * Constructor de la clase
     *
     * @param id        de la notificación
     * @param macCultivo id del cultivo
     * @param mensaje   mensaje de la notificación
     */
    public HistorialNotificacion(String id, String macCultivo, String mensaje) {
        this.id = id;
        this.macCultivo = macCultivo;
        this.mensaje = mensaje;
    }


    /**
     * Getter del id
     *
     * @return el id
     */
    public String getId() {
        return id;
    }

    /**
     * Setter del id
     *
     * @param id de la notificación
     */
    public void setId(String id) {
        this.id = id;
    }


    public String getMacCultivo() {
        return macCultivo;
    }


    public void setMacCultivo(String macCultivo) {
        this.macCultivo = macCultivo;
    }

    public String getMensaje() {
        return mensaje;
    }

    public void setMensaje(String mensaje) {
        this.mensaje = mensaje;
    }
}
