package org.escoladeltreball.smartgarden.json;

import android.content.Context;

import org.escoladeltreball.smartgarden.R;
import org.escoladeltreball.smartgarden.pojo.Registro;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.TreeMap;

/**
 * Clase que nos ayudara a parsear los objetos JSON a POJO
 */
public class JSONPullParserRegistros {
    // Nombre de los nodos que recorreremos en el JSON
    private static final String TAG_ID_CULTIVO = "id_cultivo";
    private static final String TAG_NOMBRE = "nombre";
    private static final String TAG_MES = "mes";
    private static final String TAG_CONSUMO = "consumo";
    private static final String TAG_TEMPERATURA = "temperatura";
    private static final String TAG_HUMEDAD = "humedad";
    private Context context;

    // Documento obtenido de la peticion http
    private String documentoJSON;
    // Lista de los items que se mostraran en la listview
    private ArrayList<Registro> arrayListRegistros = new ArrayList<>();
    private TreeMap<Integer, Registro> treeMapRegistros = new TreeMap<>();

    //Constructor de la clase que recibe el documentoJSON
    public JSONPullParserRegistros(Context context, String documentoJSON) {
        this.context = context;
        this.documentoJSON = documentoJSON;
    }

    // Metodo que usaremos para parsear el documentoJSON
    public ArrayList<Registro> parseArrayList() {
        // Comprobamos que el documento no sea nulo
        if (documentoJSON != null) {
            try {
                // Obtenemos la array de nodos del objeto JSON
                JSONArray jsonArrayRegistros = new JSONArray(documentoJSON);
                // Comprobamos el tamaño de la array de JSON
                if (jsonArrayRegistros.length() == 0) {
                    return null;
                }
                // Haremos un bucle que llenara nuestra arraylist de cultivos
                for (int i = 0; i < jsonArrayRegistros.length(); i++) {
                    // Por cada item de la array es un JSONObject
                    JSONObject jsonObjectRegistros = jsonArrayRegistros.getJSONObject(i);
                    // Recuperamos los datos del objeto JSON
                    String idCultivo = jsonObjectRegistros.getString(TAG_ID_CULTIVO);
                    String nombre = jsonObjectRegistros.getString(TAG_NOMBRE);
                    String mes = context.getResources().getStringArray(R.array.arrayMeses)
                            [Integer.parseInt(jsonObjectRegistros.getString(TAG_MES)) - 1];
                    String consumo = jsonObjectRegistros.getString(TAG_CONSUMO);
                    String temperatura = jsonObjectRegistros.getString(TAG_TEMPERATURA);
                    String humedad = jsonObjectRegistros.getString(TAG_HUMEDAD);
                    // Añadimos este nuevo cultivo a nuestra array list
                    arrayListRegistros.add(new Registro(idCultivo, nombre, mes, consumo, temperatura, humedad));
                }
                // Error en el tratamiento del documento JSON
            } catch (JSONException e) {
                e.printStackTrace();
                return null;
            }
            // En caso que el documento sea nulo retornaremos null
        } else {
            return null;
        }

        // Si ha ido bien retornamos la arraylist
        return arrayListRegistros;
    }

    // Metodo que usaremos para parsear el documentoJSON
    public TreeMap<Integer, Registro> parseTreeMap() {
        // Comprobamos que el documento no sea nulo
        if (documentoJSON != null) {
            try {
                // Obtenemos la array de nodos del objeto JSON
                JSONArray jsonArrayRegistros = new JSONArray(documentoJSON);
                // Comprobamos el tamaño de la array de JSON
                if (jsonArrayRegistros.length() == 0) {
                    return null;
                }
                // Haremos un bucle que llenara nuestra arraylist de cultivos
                for (int i = 0; i < jsonArrayRegistros.length(); i++) {
                    // Por cada item de la array es un JSONObject
                    JSONObject jsonObjectRegistros = jsonArrayRegistros.getJSONObject(i);
                    // Recuperamos los datos del objeto JSON
                    int idCultivo = Integer.parseInt(jsonObjectRegistros.getString(TAG_ID_CULTIVO));
                    String nombre = jsonObjectRegistros.getString(TAG_NOMBRE);
                    int mes = Integer.parseInt(jsonObjectRegistros.getString(TAG_MES)) - 1;
                    float consumo = Float.parseFloat(jsonObjectRegistros.getString(TAG_CONSUMO));
                    float temperatura = Float.parseFloat(jsonObjectRegistros.getString(TAG_TEMPERATURA));
                    float humedad = Float.parseFloat(jsonObjectRegistros.getString(TAG_HUMEDAD));
                    // Añadimos este nuevo cultivo a nuestro hash map
                    Registro existeRegistro = treeMapRegistros.get(idCultivo);
                    // En caso que el registro ya exista añadimos los datos a ese registro
                    if (existeRegistro != null) {
                        existeRegistro.anadirDatosRegistro(mes, consumo, temperatura, humedad);
                        treeMapRegistros.put(idCultivo, existeRegistro);
                        // En caso contrario creamos el registro y le añadimos los datos
                    } else {
                        Registro nuevoRegistro = new Registro();
                        nuevoRegistro.anadirDatosRegistro(nombre, mes, consumo, temperatura, humedad);
                        treeMapRegistros.put(idCultivo, nuevoRegistro);
                    }
                }
                // Error en el tratamiento del documento JSON
            } catch (JSONException e) {
                e.printStackTrace();
                return null;
            }
            // En caso que el documento sea nulo retornaremos null
        } else {
            return null;
        }

        // Si ha ido bien retornamos la arraylist
        return treeMapRegistros;
    }
}
