package org.escoladeltreball.smartgarden.json;

import org.escoladeltreball.smartgarden.pojo.Cultivo;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

/**
 * Clase que nos ayudara a parsear los objetos JSON a POJO
 */
public class JSONPullParserCultivos {
    // Nombre de los nodos que recorreremos en el JSON
    private static final String TAG_ID_CULTIVO = "id_cultivo";
    private static final String TAG_ID_USUARIO = "id_usuario";
    private static final String TAG_NOMBRE = "nombre";
    private static final String TAG_DESCRIPCION = "descripcion";
    private static final String TAG_NOTIFICAR = "notificar";
    private static final String TAG_TEMPERATURA = "temperatura";
    private static final String TAG_TEMPERATURA_MAX = "temperatura_max";
    private static final String TAG_TEMPERATURA_MIN = "temperatura_min";
    private static final String TAG_HUMEDAD = "humedad";
    private static final String TAG_HUMEDAD_MAX = "humedad_max";
    private static final String TAG_HUMEDAD_MIN = "humedad_min";
    private static final String TAG_FUNCION_LUZ = "funcion_luz";
    private static final String TAG_HORA_INICIO_LUZ = "hora_inicio_luz";
    private static final String TAG_HORA_FIN_LUZ = "hora_fin_luz";
    private static final String TAG_FUNCION_RIEGO = "funcion_riego";
    private static final String TAG_HORA_INICIO_RIEGO = "hora_inicio_riego";
    private static final String TAG_HORA_FIN_RIEGO = "hora_fin_riego";
    private static final String TAG_FUNCION_CUBIERTA = "funcion_cubierta";
    private static final String TAG_HORA_INICIO_CUBIERTA = "hora_inicio_cubierta";
    private static final String TAG_HORA_FIN_CUBIERTA = "hora_fin_cubierta";
    // Documento obtenido de la peticion http
    private String documentoJSON;
    // Lista de los items que se mostraran en la listview
    private ArrayList<Cultivo> arrayListCultivos = new ArrayList<>();

    //Constructor de la classe que recibe el documentoJSON
    public JSONPullParserCultivos(String documentoJSON) {
        this.documentoJSON = documentoJSON;
    }

    // Metodo que usaremos para parsear el documentoJSON
    public ArrayList<Cultivo> parse() {
        // Comprobamos que el documento no sea nulo
        if (documentoJSON != null) {
            try {
                // Obtenemos la array de nodos del objeto JSON
                JSONArray jsonArrayCultivos = new JSONArray(documentoJSON);
                // Comprobamos el tamaño de la array de JSON
                if (jsonArrayCultivos.length() == 0) {
                    return null;
                }
                // Haremos un bucle que llenara nuestra arraylist de cultivos
                for (int i = 0; i < jsonArrayCultivos.length(); i++) {
                    // Por cada item de la array es un JSONObject
                    JSONObject jsonObjectCultivo = jsonArrayCultivos.getJSONObject(i);
                    // Recuperamos los datos del objeto JSON
                    String id = jsonObjectCultivo.getString(TAG_ID_CULTIVO);
                    String idUsuario = jsonObjectCultivo.getString(TAG_ID_USUARIO);
                    String nombre = jsonObjectCultivo.getString(TAG_NOMBRE);
                    String descripcion = jsonObjectCultivo.getString(TAG_DESCRIPCION);
                    String notificar = jsonObjectCultivo.getString(TAG_NOTIFICAR);
                    String temperatura = jsonObjectCultivo.getString(TAG_TEMPERATURA);
                    String temperaturaMax = jsonObjectCultivo.getString(TAG_TEMPERATURA_MAX);
                    String temperaturaMin = jsonObjectCultivo.getString(TAG_TEMPERATURA_MIN);
                    String humedad = jsonObjectCultivo.getString(TAG_HUMEDAD);
                    String humedadMax = jsonObjectCultivo.getString(TAG_HUMEDAD_MAX);
                    String humedadMin = jsonObjectCultivo.getString(TAG_HUMEDAD_MIN);
                    String funcionLuz = jsonObjectCultivo.getString(TAG_FUNCION_LUZ);
                    String horaInicioLuz = jsonObjectCultivo.getString(TAG_HORA_INICIO_LUZ);
                    String horaFinLuz = jsonObjectCultivo.getString(TAG_HORA_FIN_LUZ);
                    String funcionRiego = jsonObjectCultivo.getString(TAG_FUNCION_RIEGO);
                    String horaInicioRiego = jsonObjectCultivo.getString(TAG_HORA_INICIO_RIEGO);
                    String horaFinRiego = jsonObjectCultivo.getString(TAG_HORA_FIN_RIEGO);
                    String funcionCubierta = jsonObjectCultivo.getString(TAG_FUNCION_CUBIERTA);
                    String horaInicioCubierta = jsonObjectCultivo.getString(TAG_HORA_INICIO_CUBIERTA);
                    String horaFinCubierta = jsonObjectCultivo.getString(TAG_HORA_FIN_CUBIERTA);
                    // Añadimos este nuevo cultivo a nuestra array list
                    arrayListCultivos.add(new Cultivo(id, idUsuario, nombre, descripcion, notificar,
                            temperatura, temperaturaMax, temperaturaMin, humedad, humedadMax,
                            humedadMin, funcionLuz, horaInicioLuz, horaFinLuz, funcionRiego,
                            horaInicioRiego, horaFinRiego, funcionCubierta, horaInicioCubierta,
                            horaFinCubierta));
                }
                // Error en el tratamiento del documento JSON
            } catch (JSONException e) {
                e.printStackTrace();
                return null;
            }
            // En caso que el documento sea nulo retornaremos null
        } else {
            return null;
        }

        // Si ha ido bien retornamos la arraylist
        return arrayListCultivos;
    }
}
