package org.escoladeltreball.smartgarden.gcm;


import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.AsyncTask;

import org.escoladeltreball.smartgarden.R;
import org.escoladeltreball.smartgarden.utils.VariablesGlobales;
import org.escoladeltreball.smartgarden.utils.Utils;

/**
 * AsyncTaskDesregistrarDispositivoGCM para desregistrar el dispositivo de Google Cloud Messaging.
 *
 * @author Carlos DaPalma Corchado <cdapalmac@gmail.com>
 * @author Esdras Valiente Lanaspa <esdras.vl85@gmail.com>
 * @author Andres Jordan Zambrano <andresjordanz@hotmail.com>
 *         <p/>
 *         This is free software, licensed under the GNU General Public License v3.
 *         See http://www.gnu.org/licenses/gpl.html for more information.
 */
public class AsyncTaskDesregistrarDispositivoGCM extends AsyncTask<Void, Void, Void> {
    // Activity que llama al asynctask
    private Activity activity;
    // ProgresDialog para indicarnos el progreso del desregistro
    private ProgressDialog progressDialogDesregistro;

    // Constructor para el desregistro
    public AsyncTaskDesregistrarDispositivoGCM(Activity activity) {
        this.activity = activity;
    }

    /**
     * Metodo que usaremos para mostrar el progress dialog de desregistro.
     */
    @Override
    protected void onPreExecute() {
        // ProgresDialog para indicarnos el progreso del desregistro
        progressDialogDesregistro = new ProgressDialog(activity);
        progressDialogDesregistro.setTitle(activity.getString(R.string.fragmentajustes_asynctaskdesregistrardispositivogcm_progressdialog_titulo));
        progressDialogDesregistro.setMessage(activity.getString(R.string.fragmentajustes_asynctaskdesregistrardispositivogcm_progressdialog_mensaje));
        progressDialogDesregistro.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        progressDialogDesregistro.setIndeterminate(true);
        // Creamos el progress dialog que no pueda ser cancelable
        progressDialogDesregistro.setCancelable(false);
        progressDialogDesregistro.show();
    }

    /**
     * Metodo que usaremos para desregistrar el dispositivo a Google Cloud Messaging.
     *
     * @param params los parametros que le pasamos, en este caso ninguno.
     * @return no devolvera nada.
     */
    @Override
    protected Void doInBackground(Void... params) {
        SharedPreferences prefPetroleum = activity.getSharedPreferences("ajustes_smartgarden", Context.MODE_PRIVATE);
        SharedPreferences.Editor prefEditor = prefPetroleum.edit();
        try {
            // Desregistramos el dispositivo de GCM
            Utils.desregistrarGCM(activity);
            // Desactivamos el checkbox de las preferencias
            modificarCheckBoxPreferencias(false);
            // Si algo va mal dejamos activado el checkbox porque no se habra podido desregistrar
            // el dispostivio de gcm
        } catch (Exception ex) {
            ex.printStackTrace();
            modificarCheckBoxPreferencias(true);
        }
        // Aplicamos los cambios del SharedPreference
        prefEditor.apply();
        return null;
    }

    @Override
    protected void onPostExecute(Void Void) {
        // Ocultamos el progress dialog de desregistro
        progressDialogDesregistro.dismiss();
    }

    /**
     * Metodo que usaremos para activar o desactivar el checkbox de notificaciones y modificar
     * su valor en las preferencias.
     *
     * @param valorCheckBox true o false, dependiendo de si lo queremos activar o desactivar
     */
    public void modificarCheckBoxPreferencias(boolean valorCheckBox) {
        SharedPreferences prefPetroleum = activity.getSharedPreferences("ajustes_smartgarden", Context.MODE_PRIVATE);
        SharedPreferences.Editor prefEditor = prefPetroleum.edit();
        ((VariablesGlobales) activity.getApplication()).getCheckBoxPreference().setChecked(valorCheckBox);
        prefEditor.putBoolean("opcChckNotificaciones", valorCheckBox);
        // Aplicamos los cambios del SharedPreference
        prefEditor.apply();
    }
}