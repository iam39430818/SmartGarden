package org.escoladeltreball.smartgarden.gcm;


import android.app.Activity;

import com.google.android.gms.iid.InstanceIDListenerService;

/**
 * GCMRegistrationID para actualizar el registration id de Google Cloud Messaging.
 *
 *
 * @author Carlos DaPalma Corchado <cdapalmac@gmail.com>
 * @author Esdras Valiente Lanaspa <esdras.vl85@gmail.com>
 * @author Andres Jordan Zambrano <andresjordanz@hotmail.com>
 *
 * This is free software, licensed under the GNU General Public License v3.
 * See http://www.gnu.org/licenses/gpl.html for more information.
 */
public class GCMRegistrationID extends InstanceIDListenerService {

    /**
     * Se llama cuando en el servidor GCM actualizan el registration id, principalemnte por motivos  de seguridad
     */
    @Override
    public void onTokenRefresh() {
        // Obtenemos nuevamente el registration id y lo registramos en el servidor
        AsyncTaskRegistrarDispositivoGCM asyncTaskRegistrarDispositivoGCM = new AsyncTaskRegistrarDispositivoGCM((Activity) getApplicationContext(), false);
        asyncTaskRegistrarDispositivoGCM.execute();
    }
}
