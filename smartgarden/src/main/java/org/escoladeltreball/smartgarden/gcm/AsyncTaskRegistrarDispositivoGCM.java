package org.escoladeltreball.smartgarden.gcm;


import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Build;
import android.util.Log;

import org.escoladeltreball.smartgarden.R;
import org.escoladeltreball.smartgarden.utils.VariablesGlobales;
import org.escoladeltreball.smartgarden.utils.Utils;

/**
 * AsyncTaskRegistrarDispositivoGCM para registrar el dispositivo a Google Cloud Messaging.
 *
 * @author Carlos DaPalma Corchado <cdapalmac@gmail.com>
 * @author Esdras Valiente Lanaspa <esdras.vl85@gmail.com>
 * @author Andres Jordan Zambrano <andresjordanz@hotmail.com>
 *         <p/>
 *         This is free software, licensed under the GNU General Public License v3.
 *         See http://www.gnu.org/licenses/gpl.html for more information.
 */
public class AsyncTaskRegistrarDispositivoGCM extends AsyncTask<Void, Void, Void> {
    // ProgresDialog para indicarnos el progreso del registro
    private ProgressDialog progressDialogRegistro;
    // Activity que llama al asynctask
    private Activity activity;
    // Nos indicara si hay que mostrar la progress dialog
    private boolean mostrarProgressDialog = false;
    // Boolean que nos indicara si el dispositivo dispone de Google Play Services
    private boolean tieneGooglePlayServices;

    // Constructor para el registro
    public AsyncTaskRegistrarDispositivoGCM(Activity activity, boolean mostrarProgressDialog) {
        this.activity = activity;
        this.mostrarProgressDialog = mostrarProgressDialog;
    }

    /**
     * Metodo que usaremos para mostrar el progress dialog de registro en caso necesario.
     */
    @Override
    protected void onPreExecute() {
        if (mostrarProgressDialog) {
            // ProgresDialog para indicarnos el progreso del registro a gcm
            progressDialogRegistro = new ProgressDialog(activity);
            progressDialogRegistro.setTitle(activity.getString(R.string.fragmentajustes_asynctaskregistrardispositivogcm_progressdialog_titulo));
            progressDialogRegistro.setMessage(activity.getString(R.string.fragmentajustes_asynctaskregistrardispositivogcm_progressdialog_mensaje));
            progressDialogRegistro.setProgressStyle(ProgressDialog.STYLE_SPINNER);
            progressDialogRegistro.setIndeterminate(true);
            // Creamos el progress dialog que no pueda ser cancelable
            progressDialogRegistro.setCancelable(false);
            progressDialogRegistro.show();
        }
    }

    /**
     * Metodo que usaremos para registrar el dispositivo a Google Cloud Messaging.
     *
     * @param params los parametros que le pasamos, en este caso ninguno.
     * @return no devolvera nada.
     */
    @Override
    protected Void doInBackground(Void... params) {
        try {
            tieneGooglePlayServices = Utils.comprobarGooglePlayServices(activity);
            //Comprobamos si tiene instalado Google Play Services
            if (tieneGooglePlayServices) {
                // Obtenemos el registration ID
                String registrationID = Utils.obtenerRegistrationIDGCM(activity);
                Log.w("REGISTRATION ID->", registrationID);
                // Se registra en el servidor
                Utils.registroServidor(Build.SERIAL, registrationID);
                // Activamos el checkbox de las preferencias
                modificarCheckBoxPreferencias(true);
                // Si no tiene GooglePlayServices se le indicara en Ajustes quen no podra recibir
                // notificaciones con el checkbox de notificaciones desactivado
            } else {
                modificarCheckBoxPreferencias(false);
            }
            // Si algo va mal dejamos desactivado el checkbox porque no se habra podido registrar
        } catch (Exception ex) {
            ex.printStackTrace();
            modificarCheckBoxPreferencias(false);
        }

        return null;
    }

    @Override
    protected void onPostExecute(Void Void) {
        if (mostrarProgressDialog) {
            // Ocultamos el progress dialog de registro a gcm
            progressDialogRegistro.dismiss();
            // Tambien mostramos el mensaje para que los instale
            if (!tieneGooglePlayServices) {
                Utils.mostrarMensajeInstalarServiciosGoogle(activity);
                // Desactivamos el checkbox de las preferencias
                ((VariablesGlobales) activity.getApplication()).getCheckBoxPreference().setChecked(false);
            }
        }
    }

    /**
     * Metodo que usaremos para activar o desactivar el checkbox de notificaciones y modificar
     * su valor en las preferencias.
     *
     * @param valorCheckBox true o false, dependiendo de si lo queremos activar o desactivar
     */
    public void modificarCheckBoxPreferencias(boolean valorCheckBox) {
        SharedPreferences prefPetroleum = activity.getSharedPreferences("ajustes_smartgarden", Context.MODE_PRIVATE);
        SharedPreferences.Editor prefEditor = prefPetroleum.edit();
        if (mostrarProgressDialog) {
            ((VariablesGlobales) activity.getApplication()).getCheckBoxPreference().setChecked(valorCheckBox);
        }
        prefEditor.putBoolean("opcChckNotificaciones", valorCheckBox);
        // Aplicamos los cambios del SharedPreference
        prefEditor.apply();
    }
}