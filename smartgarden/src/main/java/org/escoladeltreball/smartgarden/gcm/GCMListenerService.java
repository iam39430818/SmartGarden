package org.escoladeltreball.smartgarden.gcm;


import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.NotificationCompat;
import android.support.v4.content.ContextCompat;
import android.text.Spannable;
import android.text.SpannableStringBuilder;
import android.text.style.ForegroundColorSpan;

import com.google.android.gms.gcm.GcmListenerService;

import org.escoladeltreball.smartgarden.PantallaPrincipal;
import org.escoladeltreball.smartgarden.R;
import org.escoladeltreball.smartgarden.database.HistorialNotificacionDAO;
import org.escoladeltreball.smartgarden.utils.Utils;

import java.util.Collections;
import java.util.Comparator;
import java.util.List;

/**
 * GCMListenerService para recibir las notificaciones de Google Cloud Messaging.
 *
 *
 * @author Carlos DaPalma Corchado <cdapalmac@gmail.com>
 * @author Esdras Valiente Lanaspa <esdras.vl85@gmail.com>
 * @author Andres Jordan Zambrano <andresjordanz@hotmail.com>
 *
 * This is free software, licensed under the GNU General Public License v3.
 * See http://www.gnu.org/licenses/gpl.html for more information.
 */
public class GCMListenerService extends GcmListenerService {
    // Gestor del historial de notificaciones
    private HistorialNotificacionDAO historialNotificacionDAO;

    /**
     * Metodo que se llama cuando el mensaje es recibido.
     *
     * @param from es el senderID del que nos envia el mensaje
     * @param data es el bundle que contiene el mensaje en formato key->valor.
     */
    @Override
    public void onMessageReceived(String from, Bundle data) {
        String mensajeCultivo = null, tipoAviso = null, idMensaje = null;
        // Recuperamos el mensaje enviado por el servidor
        for (String mensajeServidor : data.keySet()) {
            // Obtenemos el mensaje, el tipo y la mac de la notificacion
            switch (mensajeServidor) {
                case "mensaje":
                    mensajeCultivo = data.getString("mensaje");
                    break;
                case "tipo":
                    tipoAviso = data.getString("tipo");
                    break;
                default:
                    idMensaje = data.getString("idMensaje");
                    break;
            }
        }
        // Creamos y abrimos el gestor del historial de notificaciones
        historialNotificacionDAO = new HistorialNotificacionDAO(this);
        historialNotificacionDAO.abrirHistorialNotificacionDAO();
        // si la mac ya esta en la base de datos, actualiza el mensaje a aviso o a peligro
        boolean existeMac = historialNotificacionDAO.existeNotificacionPorMac(idMensaje);
        if (existeMac) {
            historialNotificacionDAO.actualizarHistorialNotificacion(new String[]{"", idMensaje, tipoAviso + " " + mensajeCultivo});
        } else {
            // si no existe, añadimos el mensaje a la base de datos
            historialNotificacionDAO.insertarHistorialNotificacion(new String[]{"", idMensaje, tipoAviso + " " + mensajeCultivo});
        }

        // Mostramos la notificacion
        this.MostrarNotificacion();
    }


    private void MostrarNotificacion() {
        // Creamos el intent que se mostrara al pulsar encima de la notificacion
        Intent notificationIntent = new Intent(this, PantallaPrincipal.class);
        notificationIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        PendingIntent pendingIntent = PendingIntent.getActivity(this, 0, notificationIntent, PendingIntent.FLAG_ONE_SHOT);

        // Ordenamos los valores de la lista con los peligros por encima de los avisos
        List<String> listOfEntries = historialNotificacionDAO.obtenerListaHistorialNotificaciones();
        historialNotificacionDAO.cerrarHistorialNotificacionDAO();
        Collections.sort(listOfEntries, comparatorOrdenarPeligrosAvisos);

        // Recuperamos el icono de notificaciones large
        Bitmap iconoNotificacionLarge = BitmapFactory.decodeResource(getResources(), R.drawable.ic_mis_cultivos);
        // Lo ajustamos al tamaño correcto
        iconoNotificacionLarge = Utils.resizeIconLarge(this, iconoNotificacionLarge);
        // En caso que sea la versión mayor o igual a la 21 se usará el icono blanco
        boolean usarIconoBlanco = (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.LOLLIPOP);
        // Escogemos el sonido de notificacion
        Uri sonidoNotificacion = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
        // Preparamos el mensaje a enviar
        NotificationCompat.Builder builder = new NotificationCompat.Builder(
                this)
                // Depende de la version de android asignamos un icono u otro
                .setSmallIcon(usarIconoBlanco ? R.drawable.ic_notificacion_blanco : R.drawable.ic_mis_cultivos)
                        // Asignamos un color de fondo al smallicon
                .setColor(ContextCompat.getColor(this, R.color.color_actionbar))
                .setLargeIcon(iconoNotificacionLarge)
                        // Mostramos el titulo cuando no se muestra el inbox
                .setContentTitle("Notificaciones SmartGarden")
                        // Mostramos el texto con el numero de notificaciones pendientes cuando no se muestra el inbox
                .setContentText(listOfEntries.size() > 1 ?
                        listOfEntries.size() + " notificaciones pendientes." :
                        listOfEntries.size() + " notificacion pendiente.")
                        // Asignamos que se borre cuando pulse
                .setAutoCancel(true)
                        // Asignamos el sonido de notificacion
                .setSound(sonidoNotificacion)
                        // Asignamos el intent que se mostrar
                .setContentIntent(pendingIntent);
        //Creamos el estilo inbox para las notificaciones
        NotificationCompat.InboxStyle inboxStyle = new NotificationCompat.InboxStyle();
        // Añadimos el titulo al inbox
        inboxStyle.setBigContentTitle("Notificaciones SmartGarden");
        // Añadimos las cadenas de peligro y aviso
        for (String mapping : listOfEntries) {
            if (mapping.toLowerCase().startsWith("peligro")) {
                inboxStyle.addLine(stringTituloColor("Peligro: ", mapping.substring(8, mapping.length()), Color.RED));
            } else if (mapping.toLowerCase().startsWith("aviso")) {
                inboxStyle.addLine(stringTituloColor("Aviso: ", mapping.substring(6, mapping.length()), Color.BLUE));
            }
        }
        // Creamos el mensaje que aparece en el pie del inbox
        if (listOfEntries.size() > 7) {
            inboxStyle.setSummaryText(listOfEntries.size() > 8 ?
                    "+" + (listOfEntries.size() - 7) + " notificaciones" :
                    "+" + (listOfEntries.size() - 7) + " notificacion");
        } else {
            inboxStyle.setSummaryText("SmartGarden");
        }

        // Asignamos el estilo
        builder.setStyle(inboxStyle);
        // Mostramos la notificacion
        NotificationManager mNotificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        mNotificationManager.notify(1, builder.build());
    }

    /**
     * Metodo que usaremos para modificar el texto del mensaje del inbox.
     *
     * @param titulo el titulo del mensaje que cambiara de color
     * @param texto  el texto que ira a continuacion del titulo
     * @param color  el color que deseamos pintar el titulo
     * @return la string con los colores indicados
     */
    public static SpannableStringBuilder stringTituloColor(String titulo, String texto, int color) {
        // Creamos el SpannableStringBuilder
        SpannableStringBuilder constructorString = new SpannableStringBuilder();
        // Le añadimos el titulo
        constructorString.append(titulo);
        // Guardamos cuanto ocupara el titulo
        int finTexto = constructorString.length();
        // Añadimos el texto al titulo
        constructorString.append(texto);
        // Modificamos el color del titulo
        constructorString.setSpan(new ForegroundColorSpan(color), 0, finTexto,
                Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
        // Retornamos la string modificada
        return constructorString;
    }


    /**
     * Comparador que usaremos para ordenar los valores del hashmap (peligros > avisos)
     */
    Comparator<String> comparatorOrdenarPeligrosAvisos = new Comparator<String>() {
        @Override
        public int compare(String str1, String str2) {
            return str2.compareTo(str1);
        }
    };
}
