package org.escoladeltreball.smartgarden;

import android.app.Activity;
import android.app.NotificationManager;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.support.design.widget.Snackbar;
import android.view.View;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.escoladeltreball.smartgarden.activities.PantallaBienvenida;
import org.escoladeltreball.smartgarden.activities.PantallaMenuLateral;
import org.escoladeltreball.smartgarden.utils.ObtenerDatosJSON;
import org.escoladeltreball.smartgarden.utils.Utils;

import java.util.ArrayList;
import java.util.List;

/**
 * Activity que se abre al iniciar la aplicacion.
 *
 * @author Carlos DaPalma Corchado <cdapalmac@gmail.com>
 * @author Esdras Valiente Lanaspa <esdras.vl85@gmail.com>
 * @author Andres Jordan Zambrano <andresjordanz@hotmail.com>
 *         <p/>
 *         This is free software, licensed under the GNU General Public License v3.
 *         See http://www.gnu.org/licenses/gpl.html for more information.
 */
public class PantallaPrincipal extends Activity {
    // Variable en milisegundos del tiempo de espera antes de iniciar la aplicacion
    private static final long TIEMPO_ESPERA = 3000;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pantalla_principal);
        // Eliminamos las notificaciones de la status bar en caso que hayan
        NotificationManager notifManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        notifManager.cancelAll();
        new TieneCultivo().execute();
    }

    /**
     * Hilo que usaremos para conectar al servidor y actualizar el cultivo
     */
    private class TieneCultivo extends AsyncTask<Void, Void, String> {

        @Override
        protected String doInBackground(Void... arg0) {
            // Declaramos una variable que devolveremos null en caso de que no hubiese internet
            String resultado = null;
            // Comprobamos si hay internet
            if (Utils.tieneConexion(PantallaPrincipal.this)) {
                List<NameValuePair> parametros = new ArrayList<>();
                // Le pasamos al servidor el parametro usuario
                parametros.add(new BasicNameValuePair("usuario", Build.SERIAL));
                // Creamos una instancia de ObtenerDatosJSON
                ObtenerDatosJSON datosJSON = new ObtenerDatosJSON();
                // Comprobamos si el usuario tiene algun cultivo registrado
                resultado = datosJSON.obtenerDocumentoJSON(Utils.URL_TIENE_CULTIVOS_REGISTRADOS, ObtenerDatosJSON.POST, parametros);
            } else {
                // Si no tiene conexion a internet mostramos un mensaje
                final Snackbar snackbar = Snackbar.make(findViewById(android.R.id.content),
                        getString(R.string.fragmentcultivo_snackbar_nohayconexion), Snackbar.LENGTH_INDEFINITE);
                // Le daremos la opcion de reintentarlo
                snackbar.setAction(getString(R.string.fragmentcultivo_snackbar_reintentar), new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        snackbar.dismiss();
                        // Intentamos otra vez la conexion con el servidor
                        new TieneCultivo().execute();
                    }
                });
                Utils.snackbarTextoVerde(PantallaPrincipal.this, snackbar);
                snackbar.show();
                resultado = "Sin conexion";
            }
            // Retormnamos el resultado
            return resultado;
        }

        @Override
        protected void onPostExecute(String resultado) {
            super.onPostExecute(resultado);
            // Si el resultado es diferente de null y la ha retornado una 's' el usuario ya tiene un cultivo registrado
            if (resultado != null && resultado.equals("S")) {
                // Setteamos a false el sharedpreferences del primer cultivo registrado
                Utils.registroPrimerCultivo(PantallaPrincipal.this);
            }
            // Si el resultado es diferente de null
            if (resultado != null && !resultado.equals("Sin conexion")) {
                // Ejecutamos un puente que ejecutara un hilo despues de un tiempo de espera
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        Intent intent;
                        // Comprobamos si ya tiene registrado un cultivo para mostrar un camino o otro
                        if (Utils.comprobarPrimerRegistroDeCultivo(PantallaPrincipal.this)) {
                            // Si es la primera vez que entramos mostraremos la PantallaBienvenida
                            intent = new Intent(PantallaPrincipal.this, PantallaBienvenida.class);
                        } else {
                            // Si no es la primera vez que entramos mostramos la PantallaMenuLateral
                            intent = new Intent(PantallaPrincipal.this, PantallaMenuLateral.class);
                        }
                        // Iniciamos el intent
                        startActivity(intent);
                        // Finalizamos la activity
                        finish();
                    }
                }, TIEMPO_ESPERA);
            } else if (resultado != null && resultado.equals("Sin conexion")) {
                // En caso de no haber conexion cancelamos el hilo
                this.cancel(true);
            } else if (resultado == null) {
                // Si no tiene conexion a internet mostramos un mensaje
                final Snackbar snackbar = Snackbar.make(findViewById(android.R.id.content),
                        "No se ha podido conectar con el servidor", Snackbar.LENGTH_INDEFINITE);
                Utils.snackbarTextoVerde(PantallaPrincipal.this, snackbar);
                snackbar.show();
            }
        }
    }
}
