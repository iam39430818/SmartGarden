package org.escoladeltreball.smartgarden.utils;

import android.app.Application;
import android.preference.CheckBoxPreference;
import android.view.View;
import android.widget.ImageButton;

public class VariablesGlobales extends Application {

    private CheckBoxPreference checkBoxPreference;
    private ImageButton imgBtnSiguiente;
    private boolean mostrarImgBtnSiguiente;
    public int codigoResultadoComprobarGooglePlayServices;
    private View view;

    public CheckBoxPreference getCheckBoxPreference() {
        return checkBoxPreference;
    }

    public void setCheckBoxPreference(CheckBoxPreference checkBoxPreference) {
        this.checkBoxPreference = checkBoxPreference;
    }

    public ImageButton getImgBtnSiguiente() {
        return imgBtnSiguiente;
    }

    public void setImgBtnSiguiente(ImageButton imgBtnSiguiente) {
        this.imgBtnSiguiente = imgBtnSiguiente;
    }

    public boolean isMostrarImgBtnSiguiente() {
        return mostrarImgBtnSiguiente;
    }

    public void setMostrarImgBtnSiguiente(boolean mostrarImgBtnSiguiente) {
        this.mostrarImgBtnSiguiente = mostrarImgBtnSiguiente;
    }

    public int getCodigoResultadoComprobarGooglePlayServices() {
        return codigoResultadoComprobarGooglePlayServices;
    }

    public void setCodigoResultadoComprobarGooglePlayServices(int codigoResultadoComprobarGooglePlayServices) {
        this.codigoResultadoComprobarGooglePlayServices = codigoResultadoComprobarGooglePlayServices;
    }

    public View getView() {
        return view;
    }

    public void setView(View view) {
        this.view = view;
    }
}