package org.escoladeltreball.smartgarden.utils;

import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.protocol.HTTP;
import org.apache.http.util.EntityUtils;

import java.io.IOException;
import java.util.List;

/**
 * La clase que a traves de la URL hace un GET o POST y devuelve el texto JSON.
 *
 * @author Carlos DaPalma Corchado <cdapalmac@gmail.com>
 * @author Esdras Valiente Lanaspa <esdras.vl85@gmail.com>
 * @author Andres Jordan Zambrano <andresjordanz@hotmail.com>
 *         <p/>
 *         This is free software, licensed under the GNU General Public License v3.
 *         See http://www.gnu.org/licenses/gpl.html for more information.
 */
public class ObtenerDatosJSON {
    // En caso que sea tipo GET el tipo es 1
    public final static int GET = 1;
    // En caso que sea tipo POST el tipo es 2
    public final static int POST = 2;

    public ObtenerDatosJSON() {
    }

    /**
     * Metodo que usaremos para hacer la peticion al servidor.
     *
     * @param url    url a la que haremos la peticion
     * @param method la solicitud http que queremos
     */
    public String obtenerDocumentoJSON(String url, int method) {
        return this.obtenerDocumentoJSON(url, method, null);
    }

    /**
     * Metodo que usaremos para recibir la peticion y respuesta del servidor.
     *
     * @param url        url a la que haremos la peticion
     * @param tipoMetodo la solicitud http que queremos hacer
     * @param parametros los parametros que enviamos (en caso que haya)
     */
    public String obtenerDocumentoJSON(String url, int tipoMetodo,
                                       List<NameValuePair> parametros) {
        // La respuesta que recibiremos de la conexion
        String respuestaServidor;

        try {
            // Se crea el cliente http
            HttpClient httpClient = new DefaultHttpClient();
            // Se crear la respuesta del servidor
            HttpResponse httpResponse;
            // Codigo que usaremos para comprobar que se haya ejecutado correctamente
            int codigoEstado;
            // Ejecutamos el tipo de peticion solicitada
            switch (tipoMetodo) {
                // Enviaremos unos parametros y recibiremos respuesta del servidor
                case POST:
                    HttpPost httpPost = new HttpPost(url);
                    // Añadimos los parametros a la solicitud del servidor
                    if (parametros != null) {
                        httpPost.setEntity(new UrlEncodedFormEntity(parametros, HTTP.UTF_8));
                    }
                    // Ejecutamos la solicud y obtenemos la respuesta
                    httpResponse = httpClient.execute(httpPost);
                    codigoEstado = httpResponse.getStatusLine().getStatusCode();
                    break;
                // No enviaremos ningun parametro y solo recibimos respuesta del servidor
                default:
                    HttpGet httpGet = new HttpGet(url);
                    // Ejecutamos la solicud y obtenemos la respuesta
                    httpResponse = httpClient.execute(httpGet);
                    codigoEstado = httpResponse.getStatusLine().getStatusCode();
                    break;
            }

            // Si el codigo de error es diferente de 200 continuara la ejecucion
            if (codigoEstado != 200) {
                return null;
            }

            // Obtenemos el contenido del JSON que vamos a retornar
            respuestaServidor = EntityUtils.toString(httpResponse.getEntity());

            // Si algo falla saltara la excepcion
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }

        // Retornamos el contenido del JSON
        return respuestaServidor;

    }

}
