package org.escoladeltreball.smartgarden.utils;

import android.text.Editable;
import android.text.TextWatcher;
import android.widget.EditText;

/**
 * Clase que nos limitará cualquier editText a las lineas que queramos.
 *
 *
 * @author Carlos DaPalma Corchado <cdapalmac@gmail.com>
 * @author Esdras Valiente Lanaspa <esdras.vl85@gmail.com>
 * @author Andres Jordan Zambrano <andresjordanz@hotmail.com>
 *
 * This is free software, licensed under the GNU General Public License v3.
 * See http://www.gnu.org/licenses/gpl.html for more information.
 */
public class LimitarLineasEditText implements TextWatcher {
    // EditText de la view
    private EditText editText;
    // Maximo de lineas del edittext
    private int maximoLineas;
    // Anterior valor del edittext
    private String lastValue = "";

    // Constructor del cual obtendremos el edittext y el maximo de lineas
    public LimitarLineasEditText(EditText editText, int maximoLineas) {
        this.editText = editText;
        this.maximoLineas = maximoLineas;
    }

    @Override
    public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
        lastValue = charSequence.toString();
    }

    @Override
    public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

    }

    @Override
    public void afterTextChanged(Editable editable) {
        if (editText.getLineCount() > maximoLineas) {
            int inicioSeleccion = editText.getSelectionStart() - 1;
            editText.setText(lastValue);
            if (inicioSeleccion >= editText.length()) {
                inicioSeleccion = editText.length();
            }
            editText.setSelection(inicioSeleccion);
        }
    }
}
