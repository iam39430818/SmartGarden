package org.escoladeltreball.smartgarden.utils;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.ColorMatrix;
import android.graphics.ColorMatrixColorFilter;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffColorFilter;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.support.design.widget.Snackbar;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.github.amlcurran.showcaseview.ShowcaseView;
import com.github.amlcurran.showcaseview.targets.ViewTarget;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GoogleApiAvailability;
import com.google.android.gms.gcm.GoogleCloudMessaging;
import com.google.android.gms.iid.InstanceID;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.escoladeltreball.smartgarden.R;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * Clase de utilidades.
 *
 * @author Carlos DaPalma Corchado <cdapalmac@gmail.com>
 * @author Esdras Valiente Lanaspa <esdras.vl85@gmail.com>
 * @author Andres Jordan Zambrano <andresjordanz@hotmail.com>
 *         <p/>
 *         This is free software, licensed under the GNU General Public License v3.
 *         See http://www.gnu.org/licenses/gpl.html for more information.
 */
public class Utils {

    // URLs //

    // Url donde atacaremos al servidor
    public final static String URL = "http://smartgardencea.ddns.net:41000/";
    //        public final static String URL = "http://192.168.104.85/";
    // Url donde atacaremos al servidor para que retorne todos los cultivos libres o los registrados
    public final static String URL_CULTIVOS = URL + "cultivos.php";
    // Url donde atacaremos al servidor para llamar a la función actualizarCultivo
    public final static String URL_ACTUALIZAR_CULTIVO = URL + "actualizarCultivo.php";
    // Url donde atacaremos al servidor para llamar a la función obtenerRegistros
    public final static String URL_OBTENER_REGISTROS = URL + "obtenerRegistros.php";
    // Url donde atacaremos al servidor para llamar a la función activarDesactivarFunciones
    public final static String URL_ACCION_CULTIVO = URL + "accionCultivo.php";
    // Url donde atacaremos al servidor para llamar a la función actualizarRegId
    public final static String URL_ACTUALIZAR_REG_ID = URL + "actualizarRegId.php";
    // Url para comprobar si un usuario tiene cultivos registrados
    public final static String URL_TIENE_CULTIVOS_REGISTRADOS = URL + "tieneCultivosRegistrados.php";
    // Numero de solicitud para comprobar si tiene Google Play Services
    public static final int PLAY_SERVICES_RESOLUTION_REQUEST = 1972;
    // El nombre de la shared preferences de nuestros ajustes
    public static final String NOMBRE_SHARED_PREFERENCES_AJUSTES = "ajustes_smartgarden";
    // Nombre del SharedPreferences donde guardamos los registros de entrada en las activitys
    private static final String REGISTRO_PRIMER_CULTIVO = "REGISTRO_PRIMER_CULTIVO";

    /**
     * Metodo que se usará para ver hay algun cultivo guardado
     *
     * @param context de la activity
     * @return un boolean que indica si se ha registrado un cultivo
     */
    public static boolean comprobarPrimerRegistroDeCultivo(Context context) {
        // Obtenemos el SharedPreferences del primer registro de un cultivo
        SharedPreferences preferences = context.getSharedPreferences(REGISTRO_PRIMER_CULTIVO, Context.MODE_PRIVATE);
        // Obtenemos el boolean que nos indica si se ha registrado al menos un cultivo
        return preferences.getBoolean("primerCultivoRegistrado", true);
    }

    /**
     * Método para guardar si se ha registrado un cultivo
     *
     * @param context de la activity
     */
    public static boolean registroPrimerCultivo(Context context) {
        // Obtenemos el SharedPreferences del primer registro de un cultivo
        SharedPreferences preferences = context.getSharedPreferences(REGISTRO_PRIMER_CULTIVO, Context.MODE_PRIVATE);
        // Obtenemos el boolean de inicio especifico de la activity
        boolean primerCultivoRegistrado = preferences.getBoolean("primerCultivoRegistrado", true);
        // Si es el primer cultivo registrado
        if (primerCultivoRegistrado) {
            // Obtenemos el editor del SharedPreferences
            SharedPreferences.Editor editor = preferences.edit();
            // Ponemos false en el SharedPreferences
            editor.putBoolean("primerCultivoRegistrado", false);
            // Aplicamos cambios
            editor.apply();
        }
        return primerCultivoRegistrado;
    }

    /**
     * Metodo que se usará para ver si es la primera vez que abre la activity/fragment.
     *
     * @param context     de la activity
     * @param nombreClase nombre de la clase
     * @return true si es la primera vez que se abre y false si no es la primera vez
     */
    public static boolean comprobarPrimerInicioClase(Context context, String nombreClase) {
        // Obtenemos con sharedpreferences si ya ha se ha conectado anteriormente
        SharedPreferences preferences = context.getSharedPreferences(NOMBRE_SHARED_PREFERENCES_AJUSTES, Context.MODE_PRIVATE);
        boolean primerInicio = preferences.getBoolean(nombreClase, true);
        // Si no es la primera vez que carga
        if (primerInicio) {
            SharedPreferences.Editor editor = preferences.edit();
            editor.putBoolean(nombreClase, false);
            editor.apply();
            return true;
        }
        return false;
    }

    /**
     * Método que nos servira para poder recuperar la action bar.
     *
     * @param context de la activity
     * @return la action bar
     */
    public static ActionBar obtenerActionBar(Context context) {
        return ((AppCompatActivity) context).getSupportActionBar();
    }

    /**
     * Método que nos servirá para mostrar AlertDialog.
     *
     * @param context              el contexto de la activity
     * @param titulo               el titulo del alertDialog
     * @param mensaje              el mensaje a mostrar en el alertDialog
     * @param botonPositivo        el texto a mostrar en el botón positivo
     * @param botonNegativo        el texto a mostrar en el botón negativo
     * @param mostrarBotonNegativo para decir si queremos bostrar el boton de cancelar o no
     * @return respuesta del usuario
     */
    public static boolean mostrarAlertDialog(Context context, String titulo, String mensaje, String botonPositivo,
                                             String botonNegativo, boolean mostrarBotonNegativo) {
        final boolean[] respuestaUsuario = new boolean[1];
        AlertDialog.Builder alertDialog = new AlertDialog.Builder(context).setTitle(
                titulo).setMessage(mensaje).setPositiveButton(botonPositivo, new AlertDialog.OnClickListener() {
            public void onClick(DialogInterface dlg, int i) {
                dlg.cancel();
                respuestaUsuario[0] = true;
            }
        });
        if (mostrarBotonNegativo) {
            alertDialog.setNegativeButton(botonNegativo, new AlertDialog.OnClickListener() {
                public void onClick(DialogInterface dlg, int i) {
                    dlg.cancel();
                    respuestaUsuario[0] = false;
                }
            });
        }
        alertDialog.show();
        return respuestaUsuario[0];
    }

    /**
     * Metodo que nos servira para poder convertir una imageview en escala de grises.
     *
     * @param imgView que se convertira en escala de grises
     */
    public static void imgViewEscalaGrises(ImageView imgView) {
        ColorMatrix matrix = new ColorMatrix();
        matrix.setSaturation(0);
        ColorMatrixColorFilter filter = new ColorMatrixColorFilter(matrix);
        imgView.setColorFilter(filter);
    }

    /**
     * Metodo que nos servira para poder pintar una imageview al que color que le pasemos.
     *
     * @param color   el color que se ha de pintar
     * @param imgView que se pintara de ese color
     */
    public static void imgViewPintarImagen(int color, ImageView imgView) {
        imgView.setColorFilter(new
                PorterDuffColorFilter(color, PorterDuff.Mode.MULTIPLY));
    }

    /**
     * Metodo que nos servira para ajustar el icono large de notificacion a la resolucion
     * del dispositivo.
     *
     * @param context de la activity
     * @param imagen  el bitmap que se ajustara
     * @return el bitmap ajustado a la resolucion
     */
    public static Bitmap resizeIconLarge(Context context, Bitmap imagen) {
        Resources res = context.getResources();
        int height = (int) res.getDimension(android.R.dimen.notification_large_icon_height);
        int width = (int) res.getDimension(android.R.dimen.notification_large_icon_width);
        return Bitmap.createScaledBitmap(imagen, width, height, false);
    }

    /**
     * Metodo que usaremos para comprobar si tiene instalado los servicios de Google.
     *
     * @param activity de la activity
     * @return si tiene instalado los servicios de Google un true sino false
     */
    public static boolean comprobarGooglePlayServices(Activity activity) {
        ((VariablesGlobales) activity.getApplication()).setCodigoResultadoComprobarGooglePlayServices(GoogleApiAvailability.getInstance().isGooglePlayServicesAvailable(activity));
        return ((VariablesGlobales) activity.getApplication()).getCodigoResultadoComprobarGooglePlayServices() == ConnectionResult.SUCCESS;
    }

    /**
     * Metodo que nos servira para mostrar el mensaje de instalacion de los Servicios de Google.
     *
     * @param activity de la activity
     */
    public static void mostrarMensajeInstalarServiciosGoogle(Activity activity) {
        // Mensaje automático de Google si no tiene instalado los servicios
        if (GoogleApiAvailability.getInstance().isUserResolvableError(((VariablesGlobales) activity.getApplication()).getCodigoResultadoComprobarGooglePlayServices())) {
            GoogleApiAvailability.getInstance().getErrorDialog((Activity) activity, ((VariablesGlobales) activity.getApplication()).getCodigoResultadoComprobarGooglePlayServices(),
                    PLAY_SERVICES_RESOLUTION_REQUEST).show();
            // Mensaje que se mostrará en caso que no se soporte el dispositivo
        } else {
            AlertDialog.Builder errorDialog = new AlertDialog.Builder(activity);
            errorDialog.setTitle(activity.getString(R.string.utils_mostrarMensajeInstalarServiciosGoogle_alertdialog_titulo));
            errorDialog.setMessage(activity.getString(R.string.utils_mostrarMensajeInstalarServiciosGoogle_alertdialog_mensaje));
            errorDialog.setCancelable(false);
            errorDialog.setNegativeButton("Aceptar", new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int id) {
                    // Cerramos el dialog
                    dialog.cancel();
                }
            });
            // Creamos el dialog y lo mostramos
            AlertDialog alertDialog = errorDialog.create();
            alertDialog.show();
        }
    }

    /**
     * Metodo que usaremos para obtener el registration id de GCM.
     *
     * @param context de la activity
     * @return el registration id
     * @throws Exception
     */
    public static String obtenerRegistrationIDGCM(Context context) throws Exception {
        InstanceID instanceID = InstanceID.getInstance(context);
        return instanceID.getToken(context.getString(R.string.gcm_senderid),
                GoogleCloudMessaging.INSTANCE_ID_SCOPE, null);
    }

    /**
     * Metodo que usaremos para desregistrar el dispositivo de GCM.
     *
     * @param context de la activity.
     * @throws IOException
     */
    public static void desregistrarGCM(Context context) throws IOException {
        InstanceID instanceID = InstanceID.getInstance(context);
        instanceID.deleteToken(context.getString(R.string.gcm_senderid),
                GoogleCloudMessaging.INSTANCE_ID_SCOPE);
    }

    /**
     * Metodo que usaremos para registrar en el servidor.
     *
     * @param numeroSerieDispositivo el numero de serie del dispositivo
     * @param regid                  el registration id de GCM
     * @return true si no esta vacio y false si lo esta
     */
    public static boolean registroServidor(String numeroSerieDispositivo, String regid) {
        // Parametros que se pasaran a la url
        List<NameValuePair> parametros = new ArrayList<>();
        parametros.add(new BasicNameValuePair("usuario", numeroSerieDispositivo));
        parametros.add(new BasicNameValuePair("regId", regid));
        // Creamos una instancia de ObtenerDatosJSON
        ObtenerDatosJSON datosJSON = new ObtenerDatosJSON();
        // Hacemos la solicitud de obtener el documento JSON, en este caso un POST
        String documentoJSON = datosJSON.obtenerDocumentoJSON(Utils.URL_ACTUALIZAR_REG_ID, ObtenerDatosJSON.POST, parametros);
        // Retorna el contenido del documento
        return !documentoJSON.isEmpty();

    }

    /**
     * Metodo que usaremos para comprobar si el dispositivo tiene conexion via Wi-Fi o red movil.
     *
     * @param context de la activity
     * @return si tiene disponible algun tipo de conexion
     */
    public static boolean tieneConexion(Context context) {
        ConnectivityManager connectivityManager = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo informacionRed = connectivityManager.getActiveNetworkInfo();
        return (informacionRed != null && informacionRed.isConnected());
    }

    // Metodo que usaremos para poner el texto de la Snackbar de color blanco
    public static void snackbarTextoVerde(Context context, Snackbar snackbar) {
        View snackbarView = snackbar.getView();
        // Por si tiene la función  setAction le ponemos también el texto en color rojo
        snackbar.setActionTextColor(ContextCompat.getColor(context, R.color.color_rojo));
        TextView textViewSnackBar = (TextView) snackbarView.findViewById(android.support.design.R.id.snackbar_text);
        // El texto de la snackbar lo ponemos en verde
        textViewSnackBar.setTextColor(ContextCompat.getColor(context, R.color.color_actionbar));
    }

    /**
     * Metodo que nos mostrara una ayuda para la primera vez que mostremos la activity o fragment.
     * O se mostrara en caso que este activado en preferencias las ayudas.
     *
     * @param activity    donde se mostrara el showcaseview
     * @param nombreClase el nombre de la clase que se comprobara si hay que mostrar la ayuda
     * @param viewTarget  el item que hay que indicar
     * @param titulo      del showcaseview
     * @param texto       el mensaje del showcaseview
     */
    public static ShowcaseView.Builder crearShowCaseView(Activity activity, String nombreClase, ViewTarget viewTarget, String titulo, String texto) {
        ShowcaseView.Builder showcaseViewBuilder = null;
        // Comprobamos si tiene activado en Ajustes el chech box de ayudas
        SharedPreferences prefChckBxAyuda = activity.getSharedPreferences(Utils.NOMBRE_SHARED_PREFERENCES_AJUSTES, Context.MODE_PRIVATE);
        boolean pref_chck_ayuda = prefChckBxAyuda.getBoolean("opcChckAyuda", false);
        // Comprobamos si es la primera vez que abre la clase o si tiene activadas las ayudas
        if (Utils.comprobarPrimerInicioClase(activity, nombreClase) || pref_chck_ayuda) {
            showcaseViewBuilder = new ShowcaseView.Builder(activity)
                    // Le asigamos el estilo
                    .setStyle(R.style.CustomShowcaseTheme)
                            // Le asignamos un titulo
                    .setContentTitle(titulo)
                            // Le asignamos el mensaje de contenido
                    .setContentText(texto);

            if (viewTarget != null) {
                // Asignamos la target del item
                showcaseViewBuilder.setTarget(viewTarget);
            }

        }
        // Retornamos el showcaseview
        return showcaseViewBuilder;
    }
}





