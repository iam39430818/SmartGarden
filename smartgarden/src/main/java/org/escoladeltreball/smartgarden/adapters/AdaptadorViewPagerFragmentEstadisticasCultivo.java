package org.escoladeltreball.smartgarden.adapters;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.app.FragmentStatePagerAdapter;

import org.escoladeltreball.smartgarden.R;
import org.escoladeltreball.smartgarden.fragments.FragmentTabHostRegistrosCultivoElectricidad;
import org.escoladeltreball.smartgarden.fragments.FragmentTabHostRegistrosCultivoHumedad;
import org.escoladeltreball.smartgarden.fragments.FragmentTabHostRegistrosCultivoTemperatura;

/**
 * Adaptador que se usa en el FragmentEstadisticasCultivo.
 *
 *
 * @author Carlos DaPalma Corchado <cdapalmac@gmail.com>
 * @author Esdras Valiente Lanaspa <esdras.vl85@gmail.com>
 * @author Andres Jordan Zambrano <andresjordanz@hotmail.com>
 *
 * This is free software, licensed under the GNU General Public License v3.
 * See http://www.gnu.org/licenses/gpl.html for more information.
 */
public class AdaptadorViewPagerFragmentEstadisticasCultivo extends FragmentStatePagerAdapter {
    // Context de la activity
    private Context context;
    // Número de paginas total que tendra el view pager
    public int totalPaginas = 3;

    private Bundle bundle;

    /**
     * Constructor del cual obtendremos el fragmentmanager y el context de la activity.
     *  @param context de la activity
     * @param fragmentManager de la activity
     * @param bundle
     */
    public AdaptadorViewPagerFragmentEstadisticasCultivo(Context context, FragmentManager fragmentManager, Bundle bundle) {
        super(fragmentManager);
        this.context = context;
        this.bundle = bundle;
    }


    /**
     * Metodo que usaremos para mostrar el fragment correspondiente del view pager.
     *
     * @param position del item seleccionado del view pager.
     * @return el fragment que se tiene que cargar en el layout.
     */
    @Override
    public Fragment getItem(int position) {
        // Comprobamos la posicion que se ha seleccionado y retornamos el fragment correspondiente
        switch (position) {
            case 0:
                FragmentTabHostRegistrosCultivoElectricidad fragmentTabHostRegistrosCultivoElectricidad = new FragmentTabHostRegistrosCultivoElectricidad();
                fragmentTabHostRegistrosCultivoElectricidad.setArguments(bundle);
                return fragmentTabHostRegistrosCultivoElectricidad;
            case 1:
                FragmentTabHostRegistrosCultivoTemperatura fragmentTabHostRegistrosCultivoTemperatura = new FragmentTabHostRegistrosCultivoTemperatura();
                fragmentTabHostRegistrosCultivoTemperatura.setArguments(bundle);
                return fragmentTabHostRegistrosCultivoTemperatura;
            case 2:
                FragmentTabHostRegistrosCultivoHumedad fragmentTabHostRegistrosCultivoHumedad = new FragmentTabHostRegistrosCultivoHumedad();
                fragmentTabHostRegistrosCultivoHumedad.setArguments(bundle);
                return fragmentTabHostRegistrosCultivoHumedad;

        }
        return null;
    }

    /**
     * Metodo que usaremos para retornar el titulo del tab de acuerdo a la posicion.
     *
     * @param position del item seleccionado del view pager
     * @return el titulo del tab
     */

    @Override
    public CharSequence getPageTitle(int position) {

        switch (position) {
            case 0:
                return context.getString(R.string.fragmentestadisticascultivo_tabhost_electricidad);
            case 1:
                return context.getString(R.string.fragmentestadisticascultivo_tabhost_temperatura);
            case 2:
                return context.getString(R.string.fragmentestadisticascultivo_tabhost_humedad);
        }
        return null;
    }

    /**
     * Metodo que nos servira para saber el numero de paginas en total del view pager.
     *
     * @return el total de paginas del view pager
     */
    @Override
    public int getCount() {
        return totalPaginas;
    }
}
