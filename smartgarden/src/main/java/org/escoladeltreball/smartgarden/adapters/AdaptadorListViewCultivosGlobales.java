package org.escoladeltreball.smartgarden.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import org.escoladeltreball.smartgarden.R;
import org.escoladeltreball.smartgarden.pojo.Registro;

import java.util.ArrayList;

/**
 * Adaptador que se usa en el FragmentTabHostEstadisticasGlobalesElectricidad,
 * FragmentTabHostEstadisticasGlobalesHumedad y FragmentTabHostEstadisticasGlobalesTemperatura.
 *
 * @author Carlos DaPalma Corchado <cdapalmac@gmail.com>
 * @author Esdras Valiente Lanaspa <esdras.vl85@gmail.com>
 * @author Andres Jordan Zambrano <andresjordanz@hotmail.com>
 *         <p/>
 *         This is free software, licensed under the GNU General Public License v3.
 *         See http://www.gnu.org/licenses/gpl.html for more information.
 */
public class AdaptadorListViewCultivosGlobales extends BaseAdapter {
    // Context de la activity
    private Context context;
    // Lista que tiene el nombre y el valor de este
    private ArrayList<Registro> listaTotalesRegistros;
    // Variable que nos indica el tipo de campo a mostrar (E -> elecricidad, T -> Temperatura y H -> Humedad)
    private String campoValor;

    /**
     * Constructor del cual obtendremos la lista de los cultivos con su valor y el context de la activity.
     *
     * @param context               de la activity
     * @param listaTotalesRegistros lista de los cultivos con su valor
     */
    public AdaptadorListViewCultivosGlobales(Context context, ArrayList<Registro> listaTotalesRegistros, String campoValor) {
        this.listaTotalesRegistros = listaTotalesRegistros;
        this.context = context;
        this.campoValor = campoValor;
    }

    /**
     * Clase contenedor para nuestro item en la listView.
     */
    static class ViewHolder {
        TextView nombre;
        TextView valor;
    }

    @Override
    public View getView(int position, View item, ViewGroup parent) {
        ViewHolder viewHolder;
        if (item == null) {
            item = LayoutInflater.from(context).inflate(R.layout.item_list_view_estadisticas_globales, parent, false);
            viewHolder = new ViewHolder();
            // Recogemos los texview del item
            viewHolder.nombre = (TextView) item.findViewById(R.id.mesEstadisticasGlobales);
            viewHolder.valor = (TextView) item.findViewById(R.id.valorEstadisticasGlobales);
            //Guardamos el viewHolder
            item.setTag(viewHolder);
        } else {
            // Recuperamos el viewHolder
            viewHolder = (ViewHolder) item.getTag();
        }
        // Rellenamos los datos en cada objeto
        viewHolder.nombre.setText(listaTotalesRegistros.get(position).getNombre());
        if (campoValor.equals("E")) {
            viewHolder.valor.setText(listaTotalesRegistros.get(position).getConsumo());
        } else if (campoValor.equals("T")) {
            viewHolder.valor.setText(listaTotalesRegistros.get(position).getTemperatura());
        } else if (campoValor.equals("H")) {
            viewHolder.valor.setText(listaTotalesRegistros.get(position).getHumedad());
        }
        return item;
    }

    /**
     * Metodo que nos servira para saber el numero de la lista en total.
     *
     * @return el total de items de la lista
     */
    @Override
    public int getCount() {
        return listaTotalesRegistros.size();
    }

    /**
     * Metodo que nos servira para obtener un item de la lista.
     *
     * @param position es la posicion del item de la lista
     * @return el item en esa posicion
     */
    @Override
    public Object getItem(int position) {
        return listaTotalesRegistros.get(position);
    }

    /**
     * Metodo que usaremos para obtener el id del item de la lista,
     *
     * @param position es la posicion del item de la lista
     * @return el id del item de esa posicion
     */
    @Override
    public long getItemId(int position) {
        return position;
    }
}
