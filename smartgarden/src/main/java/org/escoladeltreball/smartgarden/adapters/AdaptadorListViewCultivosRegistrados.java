package org.escoladeltreball.smartgarden.adapters;

import android.content.Context;
import android.support.v4.content.ContextCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.TextView;

import org.escoladeltreball.smartgarden.R;
import org.escoladeltreball.smartgarden.pojo.Cultivo;

import java.util.ArrayList;
import java.util.List;

/**
 * Adaptador que se usa en el FragmentCultivosRegistrados.
 *
 *
 * @author Carlos DaPalma Corchado <cdapalmac@gmail.com>
 * @author Esdras Valiente Lanaspa <esdras.vl85@gmail.com>
 * @author Andres Jordan Zambrano <andresjordanz@hotmail.com>
 *
 * This is free software, licensed under the GNU General Public License v3.
 * See http://www.gnu.org/licenses/gpl.html for more information.
 */
public class AdaptadorListViewCultivosRegistrados extends BaseAdapter implements Filterable {
    // Context de la activity
    public Context context;
    // Lista de los cultivos registrados
    public ArrayList<Cultivo> listaCultivosRegistrados;
    // Copia de la lista de los cultivos registrados para filtrar la busqueda
    public ArrayList<Cultivo> listaCultivosRegistradosOrig;


    /**
     * View con cache de busqueda.
     */
    static class CultivoHolder {
        ImageView imageViewCultivo;
        TextView nombreCultivo;
        TextView descripcionCultivo;
    }

    /**
     * Constructor del cual obtendremos la lista de los cultivos registrados y el context de la activity.
     *
     * @param context de la activity
     * @param cultivos lista de los cultivos
     */
    public AdaptadorListViewCultivosRegistrados(Context context, ArrayList<Cultivo> cultivos) {
        this.context = context;
        this.listaCultivosRegistrados = cultivos;
    }


    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        // Obtenemos la informacion del item en esa posicion
        CultivoHolder holder;
        // Chequeamos si la view esta siendo reutilzada la vista existente, de lo contrario inflo la view
        if (convertView == null) {
            convertView = LayoutInflater.from(context).inflate(R.layout.listview_item_cultivos_registrados, parent, false);
            holder = new CultivoHolder();
            holder.imageViewCultivo = (ImageView) convertView.findViewById(R.id.imgViewCultivo);
            holder.nombreCultivo = (TextView) convertView.findViewById(R.id.tvNombreCultivo);
            holder.descripcionCultivo = (TextView) convertView.findViewById(R.id.tvdescripcionCultivo);
            convertView.setTag(holder);
        } else {
            holder = (CultivoHolder) convertView.getTag();
        }
        // Rellenamos los datos en cada objeto
        holder.imageViewCultivo.setImageDrawable(ContextCompat.getDrawable(context, R.drawable.ic_mis_cultivos));
        holder.nombreCultivo.setText(listaCultivosRegistrados.get(position).getNombre());
        holder.descripcionCultivo.setText(listaCultivosRegistrados.get(position).getDescripcion());

        // Devolvemos la vista
        return convertView;
    }

    /**
     * Metodo que usaremos para filtrar la lista.
     *
     * @return la lista de los items filtrada
     */
    @Override
    public Filter getFilter() {
        return new Filter() {

            @Override
            protected FilterResults performFiltering(CharSequence letraFiltrar) {
                final FilterResults resultadosFiltro = new FilterResults();
                final ArrayList<Cultivo> results = new ArrayList<>();
                if (listaCultivosRegistradosOrig == null)
                    listaCultivosRegistradosOrig = listaCultivosRegistrados;
                // Si se filtra una letra modificamos la lista
                if (letraFiltrar != null) {
                    if (listaCultivosRegistradosOrig != null && listaCultivosRegistradosOrig.size() > 0) {
                        for (final Cultivo cultivo : listaCultivosRegistradosOrig) {
                            // Filtraremos por el nombre del nombre y la descripción este escrito en mayusculas o minusculas
                            if (cultivo.getNombre().toLowerCase()
                                    .contains(letraFiltrar.toString().toLowerCase())) {
                                results.add(cultivo);
                            } else if (cultivo.getId_cultivo().toLowerCase()
                                    .contains(letraFiltrar.toString().toLowerCase())) {
                                results.add(cultivo);
                            }
                        }
                    }
                    resultadosFiltro.values = results;
                    // Si se presiona la X o se borra el campo se muestra la lista original
                } else {
                    resultadosFiltro.values = listaCultivosRegistradosOrig;
                }

                return resultadosFiltro;
            }

            @Override
            protected void publishResults(CharSequence constraint,
                                          FilterResults results) {
                // Creamos una lista de resultados temporales
                ArrayList<Cultivo> listaTemporal = new ArrayList<>();
                // Añadimos los valores encontrados a la lista
                List<?> result = (List<?>) results.values;
                for (Object object : result) {
                    if (object instanceof Cultivo) {
                        listaTemporal.add((Cultivo) object);
                    }
                }
                // Asignamos la nueva lista y notificamos a la list view
                listaCultivosRegistrados = listaTemporal;
                notifyDataSetChanged();
            }
        };
    }

    /**
     * Metodo que usaremos para notificar los cambios de la listview.
     */
    @Override
    public void notifyDataSetChanged() {
        super.notifyDataSetChanged();
    }

    /**
     * Metodo que nos servira para saber el numero de la lista en total.
     *
     * @return el total de items de la lista
     */
    @Override
    public int getCount() {
        return listaCultivosRegistrados.size();
    }

    /**
     * Metodo que nos servira para obtener un item de la lista.
     *
     * @param position es la posicion del item de la lista
     * @return el item en esa posicion
     */
    @Override
    public Object getItem(int position) {
        return listaCultivosRegistrados.get(position);
    }

    /**
     * Metodo que usaremos para obtener el id del item de la lista,
     *
     * @param position es la posicion del item de la lista
     * @return el id del item de esa posicion
     */
    @Override
    public long getItemId(int position) {
        return position;
    }

}
