package org.escoladeltreball.smartgarden.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import org.escoladeltreball.smartgarden.R;
import org.escoladeltreball.smartgarden.pojo.Registro;

import java.util.ArrayList;

/**
 * Adaptador que se usa en el FragmentTabHostRegistrosCultivo.
 *
 * @author Carlos DaPalma Corchado <cdapalmac@gmail.com>
 * @author Esdras Valiente Lanaspa <esdras.vl85@gmail.com>
 * @author Andres Jordan Zambrano <andresjordanz@hotmail.com>
 *         <p/>
 *         This is free software, licensed under the GNU General Public License v3.
 *         See http://www.gnu.org/licenses/gpl.html for more information.
 */
public class AdaptadorListviewRegistrosCultivo extends BaseAdapter {
    // Context de la activity
    private Context context;
    // Lista de los cultivos con el consumo de electricidad
    private ArrayList<Registro> listaEstadisticas;
    private String tipo;

    /**
     * View con cache de busqueda.
     */
    static class ViewHolder {
        TextView mes;
        TextView consumo;
    }

    /**
     * Constructor del cual obtendremos la lista de las estadisticas y el context de la activity.
     *
     * @param context      de la activity
     * @param estadisticas la lista de las estadisticas
     */
    public AdaptadorListviewRegistrosCultivo(Context context, ArrayList<Registro> estadisticas, String tipo) {
        this.context = context;
        this.listaEstadisticas = estadisticas;
        this.tipo = tipo;
    }

    // Metodo que usaremos para convertir el contenido en una view
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        // Obtenemos la informacion del item en esa posicion
        ViewHolder holder;
        // Chequeamos si la view esta siendo reutilzada la vista existente, de lo contrario inflo la view
        if (convertView == null) {
            convertView = LayoutInflater.from(context).inflate(R.layout.listview_item_estadisticas_cultivo, parent, false);
            holder = new ViewHolder();
            holder.mes = (TextView) convertView.findViewById(R.id.tvMesItemCultivo);
            holder.consumo = (TextView) convertView.findViewById(R.id.tvConsumoItemCultivo);
            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }
        // Rellenamos los datos en cada objeto
        holder.mes.setText(listaEstadisticas.get(position).getMes());
        if (tipo.equals("E")) {
            holder.consumo.setText(listaEstadisticas.get(position).getConsumo());
        } else if (tipo.equals("T")) {
            holder.consumo.setText(listaEstadisticas.get(position).getTemperatura());
        } else if (tipo.equals("H")) {
            holder.consumo.setText(listaEstadisticas.get(position).getHumedad());
        }

        // Devolvemos la vista
        return convertView;
    }

    /**
     * Metodo que nos servira para saber el numero de la lista en total.
     *
     * @return el total de items de la lista
     */
    @Override
    public int getCount() {
        return listaEstadisticas.size();
    }

    /**
     * Metodo que nos servira para obtener un item de la lista.
     *
     * @param position es la posicion del item de la lista
     * @return el item en esa posicion
     */
    @Override
    public Object getItem(int position) {
        return listaEstadisticas.get(position);
    }

    /**
     * Metodo que usaremos para obtener el id del item de la lista,
     *
     * @param position es la posicion del item de la lista
     * @return el id del item de esa posicion
     */
    @Override
    public long getItemId(int position) {
        return position;
    }
}