package org.escoladeltreball.smartgarden.adapters;


import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import org.escoladeltreball.smartgarden.fragments.FragmentBienvenida;
import org.escoladeltreball.smartgarden.fragments.FragmentBusquedaCultivos;

/**
 * Adaptador que se usa en el FragmentPantallaBienvenida.
 *
 *
 * @author Carlos DaPalma Corchado <cdapalmac@gmail.com>
 * @author Esdras Valiente Lanaspa <esdras.vl85@gmail.com>
 * @author Andres Jordan Zambrano <andresjordanz@hotmail.com>
 *
 * This is free software, licensed under the GNU General Public License v3.
 * See http://www.gnu.org/licenses/gpl.html for more information.
 */
public class AdaptadorViewPagerFragmentPantallaBienvenida extends FragmentPagerAdapter {
    // Número de paginas total que tendra el view pager
    public int totalPaginas = 2;

    /**
     * Constructor del cual obtendremos el fragmentmanager y el context de la activity.
     *
     * @param fragmentManager de la activity
     */
    public AdaptadorViewPagerFragmentPantallaBienvenida(FragmentManager fragmentManager) {
        super(fragmentManager);
    }


    /**
     * Metodo que usaremos para mostrar el fragment correspondiente del view pager.
     *
     * @param position del item seleccionado del view pager.
     * @return el fragment que se tiene que cargar en el layout.
     */
    @Override
    public Fragment getItem(int position) {
        // Comprobamos la posicion que se ha seleccionado y retornamos el fragment correspondiente
        switch (position) {
            case 0:
                return new FragmentBienvenida();
            case 1:
                return new FragmentBusquedaCultivos();
        }
        // Retornamos el fragment que se tiene que cargar en el layout
        return null;
    }

    /**
     * Metodo que nos servira para saber el numero de paginas en total del view pager.
     *
     * @return el total de paginas del view pager
     */
    @Override
    public int getCount() {
        return totalPaginas;
    }

}
