package org.escoladeltreball.smartgarden.adapters;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.app.FragmentStatePagerAdapter;

import org.escoladeltreball.smartgarden.R;
import org.escoladeltreball.smartgarden.fragments.FragmentTabHostEstadisticasGlobalesElectricidad;
import org.escoladeltreball.smartgarden.fragments.FragmentTabHostEstadisticasGlobalesHumedad;
import org.escoladeltreball.smartgarden.fragments.FragmentTabHostEstadisticasGlobalesTemperatura;
import org.escoladeltreball.smartgarden.fragments.FragmentTabHostRegistrosCultivoElectricidad;

/**
 * Adaptador que se usa en el FragmentEstadisticasGlobales.
 *
 *
 * @author Carlos DaPalma Corchado <cdapalmac@gmail.com>
 * @author Esdras Valiente Lanaspa <esdras.vl85@gmail.com>
 * @author Andres Jordan Zambrano <andresjordanz@hotmail.com>
 *
 * This is free software, licensed under the GNU General Public License v3.
 * See http://www.gnu.org/licenses/gpl.html for more information.
 */
public class AdaptadorViewPagerFragmentEstadisticasGlobales extends FragmentStatePagerAdapter {
    // Context de la activity
    private Context context;
    // Bundle que obtiene la lista de los registros
    private Bundle bundle;
    // Número de paginas total que tendra el view pager
    public int totalPaginas = 3;

    /**
     * Constructor del cual obtendremos el fragmentmanager y el context de la activity.
     *  @param context de la activity
     * @param fragmentManager de la activity
     * @param bundle de la activity
     */
    public AdaptadorViewPagerFragmentEstadisticasGlobales(Context context, FragmentManager fragmentManager, Bundle bundle) {
        super(fragmentManager);
        this.context = context;
        this.bundle = bundle;
    }


    /**
     * Metodo que usaremos para mostrar el fragment correspondiente del view pager.
     *
     * @param position del item seleccionado del view pager.
     * @return el fragment que se tiene que cargar en el layout.
     */
    @Override
    public Fragment getItem(int position) {
        // Comprobamos la posicion que se ha seleccionado y retornamos el fragment correspondiente
        switch (position) {
            case 0:
                FragmentTabHostEstadisticasGlobalesElectricidad fragmentTabHostEstadisticasGlobalesElectricidad = new FragmentTabHostEstadisticasGlobalesElectricidad();
                fragmentTabHostEstadisticasGlobalesElectricidad.setArguments(bundle);
                return fragmentTabHostEstadisticasGlobalesElectricidad;
            case 1:
                FragmentTabHostEstadisticasGlobalesTemperatura fragmentTabHostEstadisticasGlobalesTemperatura = new FragmentTabHostEstadisticasGlobalesTemperatura();
                fragmentTabHostEstadisticasGlobalesTemperatura.setArguments(bundle);
                return fragmentTabHostEstadisticasGlobalesTemperatura;
            case 2:
                FragmentTabHostEstadisticasGlobalesHumedad fragmentTabHostEstadisticasGlobalesHumedad = new FragmentTabHostEstadisticasGlobalesHumedad();
                fragmentTabHostEstadisticasGlobalesHumedad.setArguments(bundle);
                return fragmentTabHostEstadisticasGlobalesHumedad;
        }
        return null;
    }

    /**
     * Metodo que usaremos para retornar el titulo del tab de acuerdo a la posicion.
     *
     * @param position del item seleccionado del view pager
     * @return el titulo del tab
     */

    @Override
    public CharSequence getPageTitle(int position) {

        switch (position) {
            case 0:
                return context.getString(R.string.fragmentestadisticasglobales_tabhost_electricidad);
            case 1:
                return context.getString(R.string.fragmentestadisticasglobales_tabhost_temperatura);
            case 2:
                return context.getString(R.string.fragmentestadisticasglobales_tabhost_humedad);
        }
        return null;
    }

    /**
     * Metodo que nos servira para saber el numero de paginas en total del view pager.
     *
     * @return el total de paginas del view pager
     */
    @Override
    public int getCount() {
        return totalPaginas;
    }
}
