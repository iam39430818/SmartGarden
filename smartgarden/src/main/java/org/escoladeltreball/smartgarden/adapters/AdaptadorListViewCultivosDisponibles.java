package org.escoladeltreball.smartgarden.adapters;


import android.content.Context;
import android.support.v4.content.ContextCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import org.escoladeltreball.smartgarden.R;
import org.escoladeltreball.smartgarden.pojo.Cultivo;

import java.util.ArrayList;

/**
 * Adaptador que se usa en el FragmentBusquedaCultivos.
 *
 *
 * @author Carlos DaPalma Corchado <cdapalmac@gmail.com>
 * @author Esdras Valiente Lanaspa <esdras.vl85@gmail.com>
 * @author Andres Jordan Zambrano <andresjordanz@hotmail.com>
 *
 * This is free software, licensed under the GNU General Public License v3.
 * See http://www.gnu.org/licenses/gpl.html for more information.
 */
public class AdaptadorListViewCultivosDisponibles extends BaseAdapter {
    // Context de la activity
    public Context context;
    // Lista de los dispositivos bluetooh
    public ArrayList<Cultivo> listaDispositivos;

    /**
     * Holder de las view con cache de busqueda.
     */
    static class DispositivoHolder {
        ImageView imageViewCultivo;
        TextView nombreDispositivo;
        TextView macDispositivo;

    }

    /**
     * Constructor del cual obtendremos la lista de los dispositivos bluetooth y el context de la activity.
     *
     * @param context de la activity
     * @param dispositivos lista de los dispositivos bluetooth
     */
    public AdaptadorListViewCultivosDisponibles(Context context, ArrayList<Cultivo> dispositivos) {
        this.context = context;
        this.listaDispositivos = dispositivos;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        // Obtenemos la informacion del item en esa posicion
        DispositivoHolder holder;
        // Chequeamos si la view esta siendo reutilzada la vista existente, de lo contrario inflo la view
        if (convertView == null) {
            convertView = LayoutInflater.from(context).inflate(R.layout.listview_item_cultivos_disponibles, parent, false);
            holder = new DispositivoHolder();
            holder.imageViewCultivo = (ImageView) convertView.findViewById(R.id.imgViewCultivoBluetooth);
            holder.nombreDispositivo = (TextView) convertView.findViewById(R.id.tvNombreDispositivo);
            holder.macDispositivo = (TextView) convertView.findViewById(R.id.tvMacDispositivo);
            convertView.setTag(holder);
        } else {
            holder = (DispositivoHolder) convertView.getTag();
        }
        // Rellenamos los datos en cada objeto
        holder.imageViewCultivo.setImageDrawable(ContextCompat.getDrawable(context, R.drawable.ic_mis_cultivos));
        holder.nombreDispositivo.setText(listaDispositivos.get(position).getNombre());
        holder.macDispositivo.setText(listaDispositivos.get(position).getId_cultivo());

        // Devolvemos la vista
        return convertView;
    }

    /**
     * Metodo que usaremos para notificar los cambios de la listview.
     */
    @Override
    public void notifyDataSetChanged() {
        super.notifyDataSetChanged();
    }

    /**
     * Metodo que nos servira para saber el numero de la lista en total.
     *
     * @return el total de items de la lista
     */
    @Override
    public int getCount() {
        return listaDispositivos.size();
    }

    /**
     * Metodo que nos servira para obtener un item de la lista.
     *
     * @param position es la posicion del item de la lista
     * @return el item en esa posicion
     */
    @Override
    public Object getItem(int position) {
        return listaDispositivos.get(position);
    }

    /**
     * Metodo que usaremos para obtener el id del item de la lista,
     *
     * @param position es la posicion del item de la lista
     * @return el id del item de esa posicion
     */
    @Override
    public long getItemId(int position) {
        return position;
    }
}
